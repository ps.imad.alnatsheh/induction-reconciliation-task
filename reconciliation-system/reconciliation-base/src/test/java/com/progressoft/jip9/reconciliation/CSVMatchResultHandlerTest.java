package com.progressoft.jip9.reconciliation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;

public class CSVMatchResultHandlerTest {
    @Test
    public void givenNullResultStreams_whenConstruct_thenThrowException() {
        ResultWriters streams = null;
        NullPointerException ex = Assertions.assertThrows(NullPointerException.class,
                () -> new CSVMatchResultHandler(streams));
        Assertions.assertEquals("given ResultWriters was null", ex.getMessage());
    }

    @Test
    public void whenIOExceptionOccurs_throwResultWritingException() {
        RecordStore store = getMatched();
        IOException toThrow = new IOException("this is a testing exception");
        ResultWriters throwingResultWriters = new ThrowingResultWriters(toThrow);
        CSVMatchResultHandler handler = new CSVMatchResultHandler(throwingResultWriters);

        ResultWritingException thrown = Assertions.assertThrows(ResultWritingException.class,
                () -> handler.handleMatched(store));
        Assertions.assertEquals("an error occurred while writing matched records CSV: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());

        thrown = Assertions.assertThrows(ResultWritingException.class,
                () -> handler.handleMismatched(store,store));
        Assertions.assertEquals("an error occurred while writing mismatched records CSV: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());

        thrown = Assertions.assertThrows(ResultWritingException.class,
                () -> handler.handleMissing(store,store));
        Assertions.assertEquals("an error occurred while writing missing records CSV: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());
    }

    @Test
    public void givenRecord_whenHandle_thenResultIsWritten() {
        RecordStore matched = getMatched();
        RecordStore mismatchedSource = getMismatchedSource();
        RecordStore mismatchedTarget = getMismatchedTarget();
        RecordStore inSourceOnly = getInSourceOnly();
        RecordStore inTargetOnly = getInTargetOnly();

        StringResultWriters resultWriters = new StringResultWriters();
        CSVMatchResultHandler handler = new CSVMatchResultHandler(resultWriters);

        handler.handleMatched(matched);
        handler.handleMismatched(mismatchedSource, mismatchedTarget);
        handler.handleMissing(inSourceOnly, inTargetOnly);

        String expectedMatched = "transaction id,amount,currency code,value date\n"
                + "TR-47884222202,10.250,JOD,2019-05-19\n"
                + "TR-47884222206,19.350,OMR,2020-01-05\n";

        String expectedMatched2 = "transaction id,amount,currency code,value date\n"
                + "TR-47884222206,19.350,OMR,2020-01-05\n"
                + "TR-47884222202,10.250,JOD,2019-05-19\n";

        String expectedMismatched = "found in file,transaction id,amount,currency code,value date\n"
                + "SOURCE,TR-47884222205,2.60,USD,2019-09-23\n"
                + "TARGET,TR-47884222205,2.600,JOD,2019-10-12\n"
                + "SOURCE,TR-47884222288,9.000,OMR,2020-08-09\n"
                + "TARGET,TR-47884222288,8.000,OMR,2020-08-09\n";

        String expectedMissing = "found in file,transaction id,amount,currency code,value date\n"
                + "SOURCE,TR-47884222201,15.00,EUR,2019-04-04\n"
                + "TARGET,TR-47884222199,25.90,EUR,2019-04-04\n";

        String matchedString = resultWriters.matched.toString();
        Assertions.assertTrue(expectedMatched.equals(matchedString) ||
                        expectedMatched2.equals(matchedString),
                "matched csv result was not as expected");
        Assertions.assertEquals(expectedMismatched, resultWriters.mismatched.toString(),
                "mismatched csv result was not as expected");
        Assertions.assertEquals(expectedMissing, resultWriters.missing.toString(),
                "missing csv result was not as expected");

    }


    private RecordStore getInTargetOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222199")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("25.90"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getInSourceOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.0"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getMismatchedTarget() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("2.6"))
                .setDate(LocalDate.of(2019, Month.OCTOBER, 12))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222288")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("8"))
                .setDate(LocalDate.of(2020, Month.AUGUST, 9))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        store.add(record2);
        return store;
    }

    private RecordStore getMismatchedSource() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222288")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("9"))
                .setDate(LocalDate.of(2020, Month.AUGUST, 9))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        store.add(record2);
        return store;
    }

    private RecordStore getMatched() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.25"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.35"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        return store;
    }
}
