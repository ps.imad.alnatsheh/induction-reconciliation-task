package com.progressoft.jip9.reconciliation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;

public class RecordMatcherTest {
    @Test
    public void givenNullMatchedResultHandler_whenConstructRecordMatcher_thenThrowException() {
        MatchResultHandler nullHandler = null;
        Exception thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new RecordMatcher(nullHandler));
        Assertions.assertEquals("null matchResultHandler was given", thrown.getMessage());
    }

    @Test
    public void givenNullRecordMap_whenMatch_thenHandleAsMissing() {
        SimpleMatchResultHandler handler = new SimpleMatchResultHandler();
        RecordMatcher matcher = new RecordMatcher(handler);

        RecordStore store = getSourceRecordStore();
        RecordStore empty = new RecordStore();
        RecordStore nullStore = null;
        Assertions.assertDoesNotThrow(() -> matcher.match(nullStore, store));
        Assertions.assertEquals(store, handler.inTargetOnly, "result was not as expected");
        Assertions.assertEquals(empty, handler.inSourceOnly, "result was not as expected");
        Assertions.assertEquals(empty, handler.mismatchedSource, "result was not as expected");
        Assertions.assertEquals(empty, handler.mismatchedTarget, "result was not as expected");
        Assertions.assertEquals(empty, handler.matched, "result was not as expected");

        handler = new SimpleMatchResultHandler();
        RecordMatcher matcher2 = new RecordMatcher(handler);
        Assertions.assertDoesNotThrow(() -> matcher2.match(store, nullStore));
        Assertions.assertEquals(store, handler.inSourceOnly, "result was not as expected");
        Assertions.assertEquals(empty, handler.inTargetOnly, "result was not as expected");
        Assertions.assertEquals(empty, handler.mismatchedSource, "result was not as expected");
        Assertions.assertEquals(empty, handler.mismatchedTarget, "result was not as expected");
        Assertions.assertEquals(empty, handler.matched, "result was not as expected");

        handler = new SimpleMatchResultHandler();
        RecordMatcher matcher3 = new RecordMatcher(handler);
        Assertions.assertDoesNotThrow(() -> matcher3.match(nullStore, nullStore));
        Assertions.assertEquals(empty, handler.inTargetOnly, "result was not as expected");
        Assertions.assertEquals(empty, handler.inSourceOnly, "result was not as expected");
        Assertions.assertEquals(empty, handler.mismatchedSource, "result was not as expected");
        Assertions.assertEquals(empty, handler.mismatchedTarget, "result was not as expected");
        Assertions.assertEquals(empty, handler.matched, "result was not as expected");
    }

    @Test
    public void givenRecordMaps_whenMatch_thenReturnExpectedResult() {
        RecordStore source = getSourceRecordStore();
        RecordStore target = getTargetRecordStore();
        RecordStore expectedMatched = getExpectedMatched();
        RecordStore expectedMismatchedSource = getExpectedMismatchedSource();
        RecordStore expectedMismatchedTarget = getExpectedMismatchedTarget();
        RecordStore expectedInSourceOnly = getExpectedInSourceOnly();
        RecordStore expectedInTargetOnly = getExpectedInTargetOnly();

        SimpleMatchResultHandler handler = new SimpleMatchResultHandler();
        RecordMatcher matcher = new RecordMatcher(handler);

        matcher.match(source, target);

        Assertions.assertEquals(expectedMatched, handler.matched, "result was not as expected");
        Assertions.assertEquals(expectedMismatchedSource, handler.mismatchedSource, "result was not as expected");
        Assertions.assertEquals(expectedMismatchedTarget, handler.mismatchedTarget, "result was not as expected");
        Assertions.assertEquals(expectedInSourceOnly, handler.inSourceOnly, "result was not as expected");
        Assertions.assertEquals(expectedInTargetOnly, handler.inTargetOnly, "result was not as expected");
    }

    private RecordStore getExpectedInTargetOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222199")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("25.90"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedInSourceOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.00"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedMismatchedTarget() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("2.600"))
                .setDate(LocalDate.of(2019, Month.OCTOBER, 12))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedMismatchedSource() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedMatched() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.350"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        return store;
    }

    private RecordStore getSourceRecordStore() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        Record record3 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.350"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        Record record4 = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.00"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        store.add(record3);
        store.add(record4);
        return store;
    }

    private RecordStore getTargetRecordStore() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("2.600"))
                .setDate(LocalDate.of(2019, Month.OCTOBER, 12))
                .build();
        Record record3 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.350"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        Record record4 = new Record.Builder()
                .setID("TR-47884222199")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("25.90"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        store.add(record3);
        store.add(record4);
        return store;
    }

    @Test
    public void whenSerialize_thenDeserializeCorrectly() {
        RecordMatcher matcher = new RecordMatcher(new SimpleMatchResultHandler());
        matcher.match(getSourceRecordStore(), getTargetRecordStore());

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(matcher);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            RecordMatcher deserialized = (RecordMatcher) ois.readObject();
        });
    }

    private static class SimpleMatchResultHandler implements MatchResultHandler, Serializable {
        private static final long serialVersionUID = 7350340416725111017L;
        RecordStore matched;
        RecordStore mismatchedSource;
        RecordStore mismatchedTarget;
        RecordStore inSourceOnly;
        RecordStore inTargetOnly;

        @Override
        public void handleMatched(RecordStore recordStore) {
            matched = recordStore;
        }

        @Override
        public void handleMismatched(RecordStore source, RecordStore target) {
            mismatchedSource = source;
            mismatchedTarget = target;
        }

        @Override
        public void handleMissing(RecordStore inSourceOnly, RecordStore inTargetOnly) {
            this.inTargetOnly = inTargetOnly;
            this.inSourceOnly = inSourceOnly;
        }
    }
}
