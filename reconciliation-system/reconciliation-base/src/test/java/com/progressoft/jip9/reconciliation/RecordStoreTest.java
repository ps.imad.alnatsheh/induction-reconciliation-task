package com.progressoft.jip9.reconciliation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;
import java.util.Iterator;

public class RecordStoreTest {

    @Test
    public void whenConstructRecordStore_thenExpectedResultIsReturned() {
        String id = "TR-47884222202";
        RecordStore store = new RecordStore();

        Assertions.assertTrue(store.isEmpty(), "isEmpty result was not as expected");
        Assertions.assertEquals(0, store.size(), "size result was not as expected");
        Assertions.assertNull(store.get(id), "get result was not as expected");
    }

    @Test
    public void givenInvalidInput_whenAddRecord_thenThrowException() {
        Record nullRecord = null;
        RecordStore store = new RecordStore();
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> store.add(nullRecord));
        Assertions.assertEquals("record to add was null", ex.getMessage());
    }

    @Test
    public void givenRecord_whenAdd_thenReturnExpectedResult() {
        Record record = getRecord();
        RecordStore store = new RecordStore();
        store.add(record);

        Assertions.assertFalse(store.isEmpty(), "isEmpty result was not as expected");
        Assertions.assertEquals(1, store.size(), "size result was not as expected");
        Assertions.assertSame(record, store.get(record.getID()), "get result was not as expected");
    }

    @Test
    public void givenRecordStores_whenIterator_thenReturnExpectedResult() {
        String id1 = "TR-47884222202";
        String id2 = "TR-47884222205";
        RecordStore store = getRecordStore();

        Iterator<Record> iterator = store.iterator();
        Assertions.assertNotNull(iterator, "returned iterator was null");
        Assertions.assertTrue(iterator.hasNext(), "iterator is empty");
        Record next = iterator.next();
        Assertions.assertNotNull(next, "iterator returned null record");
        String nextid = null;
        if (store.get(id1).equals(next))
            nextid = id2;
        if (store.get(id2).equals(next))
            nextid = id1;
        Assertions.assertNotNull(nextid, "first item did not match any known");

        Assertions.assertThrows(UnsupportedOperationException.class, iterator::remove,
                "returned iterator was modifiable");

        Assertions.assertTrue(iterator.hasNext(), "iterator did not have all items");
        next = iterator.next();
        Assertions.assertNotNull(next, "iterator returned null record");
        Assertions.assertEquals(store.get(nextid), next, "second item was not as expected");

        Assertions.assertFalse(iterator.hasNext(), "iterator has extra items");
    }

    @Test
    public void givenRecordStores_whenEquals_thenFollowSpecifications() {
        RecordStore store1 = getRecordStore();
        RecordStore store2 = getRecordStore();
        RecordStore store3 = getDifferentRecordStore();

        Assertions.assertFalse(store1.equals(null), "a RecordStore was equal to null");
        Assertions.assertTrue(store1.equals(store1), "a RecordStore was not equal to itself");
        Assertions.assertTrue(store1.equals(store2), "false returned on equal RecordStores");
        Assertions.assertFalse(store1.equals(store3), "true returned on unequal RecordStores");
        Assertions.assertFalse(store2.equals(store3), "true returned on unequal RecordStores");
        Assertions.assertEquals(store1.hashCode(), store2.hashCode(), "equal RecordStores did not have equal hashCodes");
    }

    private RecordStore getRecordStore() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        return store;
    }

    private RecordStore getDifferentRecordStore() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.00"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        return store;
    }

    private Record getRecord() {
        String id = "TR-47884222202";
        Currency jod = Currency.getInstance("JOD");
        BigDecimal amount = new BigDecimal("10.250");
        LocalDate date = LocalDate.of(2019, Month.MAY, 19);
        return new Record.Builder()
                .setID(id)
                .setCurrency(jod)
                .setAmount(amount)
                .setDate(date)
                .build();
    }

    @Test
    public void whenSerialize_thenDeserializeCorrectly() {
        RecordStore store = getRecordStore();
        RecordStore[] deserialized = new RecordStore[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(store);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (RecordStore) ois.readObject();
        });

        Assertions.assertEquals(store, deserialized[0], "deserialized store was not as expected");
    }
}
