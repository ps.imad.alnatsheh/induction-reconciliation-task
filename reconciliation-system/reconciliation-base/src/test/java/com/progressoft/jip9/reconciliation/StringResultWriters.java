package com.progressoft.jip9.reconciliation;

import java.io.StringWriter;
import java.io.Writer;

public class StringResultWriters implements ResultWriters {
    StringWriter matched = new StringWriter();
    StringWriter mismatched = new StringWriter();
    StringWriter missing = new StringWriter();

    @Override
    public Writer getMatchedStream() {
        return matched;
    }

    @Override
    public Writer getMismatchedStream() {
        return mismatched;
    }

    @Override
    public Writer getMissingStream() {
        return missing;
    }
}
