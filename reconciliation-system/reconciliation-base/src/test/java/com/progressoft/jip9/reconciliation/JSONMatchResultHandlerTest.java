package com.progressoft.jip9.reconciliation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;

public class JSONMatchResultHandlerTest {
    @Test
    public void givenNullResultStreams_whenConstruct_thenThrowException() {
        ResultWriters streams = null;
        NullPointerException ex = Assertions.assertThrows(NullPointerException.class,
                () -> new JSONMatchResultHandler(streams));
        Assertions.assertEquals("given ResultWriters was null", ex.getMessage());
    }

    @Test
    public void whenIOExceptionOccurs_throwResultWritingException() {
        RecordStore store = getMatched();
        IOException toThrow = new IOException("this is a testing exception");
        ResultWriters throwingResultWriters = new ThrowingResultWriters(toThrow);
        JSONMatchResultHandler handler = new JSONMatchResultHandler(throwingResultWriters);

        ResultWritingException thrown = Assertions.assertThrows(ResultWritingException.class,
                () -> handler.handleMatched(store));
        Assertions.assertEquals("an error occurred while writing matched records JSON: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());

        thrown = Assertions.assertThrows(ResultWritingException.class,
                () -> handler.handleMismatched(store,store));
        Assertions.assertEquals("an error occurred while writing mismatched records JSON: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());

        thrown = Assertions.assertThrows(ResultWritingException.class,
                () -> handler.handleMissing(store,store));
        Assertions.assertEquals("an error occurred while writing missing records JSON: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());
    }

    @Test
    public void givenRecord_whenHandle_thenResultIsWritten() {
        RecordStore matched = getMatched();
        RecordStore mismatchedSource = getMismatchedSource();
        RecordStore mismatchedTarget = getMismatchedTarget();
        RecordStore inSourceOnly = getInSourceOnly();
        RecordStore inTargetOnly = getInTargetOnly();

        StringResultWriters resultWriters = new StringResultWriters();
        JSONMatchResultHandler handler = new JSONMatchResultHandler(resultWriters);

        handler.handleMatched(matched);
        handler.handleMismatched(mismatchedSource, mismatchedTarget);
        handler.handleMissing(inSourceOnly, inTargetOnly);

        String expectedMatched = getExpectedMatched1();

        String expectedMatched2 = getExpectedMatched2();

        String expectedMismatched = getExpectedMismatched();

        String expectedMissing = getExpectedMissing();

        String matchedResult = resultWriters.matched.toString();
        Assertions.assertTrue(expectedMatched.equals(matchedResult)
                || expectedMatched2.equals(matchedResult),
                "matched JSON result was not as expected");
        Assertions.assertEquals(expectedMismatched, resultWriters.mismatched.toString(),
                "mismatched JSON result was not as expected");
        Assertions.assertEquals(expectedMissing, resultWriters.missing.toString(),
                "missing JSON result was not as expected");

    }

    private String getExpectedMatched1() {
        return "[\n" +
                "  {\n" +
                "    \"transaction id\": \"TR-47884222202\",\n" +
                "    \"amount\": \"10.250\",\n" +
                "    \"currency code\": \"JOD\",\n" +
                "    \"value date\": \"2019-05-19\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"transaction id\": \"TR-47884222206\",\n" +
                "    \"amount\": \"19.350\",\n" +
                "    \"currency code\": \"OMR\",\n" +
                "    \"value date\": \"2020-01-05\"\n" +
                "  }\n" +
                "]";
    }

    private String getExpectedMatched2() {
        return "[\n" +
                "  {\n" +
                "    \"transaction id\": \"TR-47884222206\",\n" +
                "    \"amount\": \"19.350\",\n" +
                "    \"currency code\": \"OMR\",\n" +
                "    \"value date\": \"2020-01-05\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"transaction id\": \"TR-47884222202\",\n" +
                "    \"amount\": \"10.250\",\n" +
                "    \"currency code\": \"JOD\",\n" +
                "    \"value date\": \"2019-05-19\"\n" +
                "  }\n" +
                "]";
    }

    private String getExpectedMismatched() {
        return "[\n" +
                "  {\n" +
                "    \"found in file\": \"SOURCE\",\n" +
                "    \"transaction id\": \"TR-47884222205\",\n" +
                "    \"amount\": \"2.60\",\n" +
                "    \"currency code\": \"USD\",\n" +
                "    \"value date\": \"2019-09-23\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"found in file\": \"TARGET\",\n" +
                "    \"transaction id\": \"TR-47884222205\",\n" +
                "    \"amount\": \"2.600\",\n" +
                "    \"currency code\": \"JOD\",\n" +
                "    \"value date\": \"2019-10-12\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"found in file\": \"SOURCE\",\n" +
                "    \"transaction id\": \"TR-47884222288\",\n" +
                "    \"amount\": \"9.000\",\n" +
                "    \"currency code\": \"OMR\",\n" +
                "    \"value date\": \"2020-08-09\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"found in file\": \"TARGET\",\n" +
                "    \"transaction id\": \"TR-47884222288\",\n" +
                "    \"amount\": \"8.000\",\n" +
                "    \"currency code\": \"OMR\",\n" +
                "    \"value date\": \"2020-08-09\"\n" +
                "  }\n" +
                "]";
    }

    private String getExpectedMissing() {
        return "[\n" +
                "  {\n" +
                "    \"found in file\": \"SOURCE\",\n" +
                "    \"transaction id\": \"TR-47884222201\",\n" +
                "    \"amount\": \"15.00\",\n" +
                "    \"currency code\": \"EUR\",\n" +
                "    \"value date\": \"2019-04-04\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"found in file\": \"TARGET\",\n" +
                "    \"transaction id\": \"TR-47884222199\",\n" +
                "    \"amount\": \"25.90\",\n" +
                "    \"currency code\": \"EUR\",\n" +
                "    \"value date\": \"2019-04-04\"\n" +
                "  }\n" +
                "]";
    }

    private RecordStore getInTargetOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222199")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("25.90"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getInSourceOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.0"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getMismatchedTarget() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("2.6"))
                .setDate(LocalDate.of(2019, Month.OCTOBER, 12))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222288")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("8"))
                .setDate(LocalDate.of(2020, Month.AUGUST, 9))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        store.add(record2);
        return store;
    }

    private RecordStore getMismatchedSource() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222288")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("9"))
                .setDate(LocalDate.of(2020, Month.AUGUST, 9))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        store.add(record2);
        return store;
    }

    private RecordStore getMatched() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.25"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.35"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        return store;
    }

}
