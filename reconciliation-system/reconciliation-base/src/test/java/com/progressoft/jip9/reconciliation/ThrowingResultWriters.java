package com.progressoft.jip9.reconciliation;

import java.io.IOException;
import java.io.Writer;

class ThrowingResultWriters implements ResultWriters {
    private final IOException toThrow;

    public ThrowingResultWriters(IOException toThrow) {this.toThrow = toThrow;}

    @Override
    public Writer getMatchedStream() throws IOException {
        return getThrowingWriter();
    }

    @Override
    public Writer getMismatchedStream() throws IOException {
        return getThrowingWriter();
    }

    @Override
    public Writer getMissingStream() throws IOException {
        return getThrowingWriter();
    }

    private Writer getThrowingWriter() {
        return new Writer() {
            @Override
            public void write(char[] cbuf, int off, int len) throws IOException {
                throw toThrow;
            }

            @Override
            public void flush() throws IOException {
                throw toThrow;
            }

            @Override
            public void close() throws IOException {

            }
        };
    }
}
