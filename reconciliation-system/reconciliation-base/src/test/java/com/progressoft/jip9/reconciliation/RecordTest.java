package com.progressoft.jip9.reconciliation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;

public class RecordTest {

    @Test
    public void givenInvalidValues_whenCreateRecord_thenThrowException() {
        String id = "TR-47884222202";
        String nullID = null;
        String emptyID = "";
        Currency jod = Currency.getInstance("JOD");
        Currency nullCurrency = null;
        BigDecimal amount = new BigDecimal("10.250");
        BigDecimal nullAmount = null;
        BigDecimal negativeAmount = new BigDecimal("-10.250");
        LocalDate date = LocalDate.of(2019, Month.MAY, 19);
        LocalDate nullDate = null;

        Record.Builder builder = new Record.Builder()
                .setID(id)
                .setCurrency(jod)
                .setAmount(amount)
                .setDate(date);

        builder.setID(nullID);
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                builder::build);
        Assertions.assertEquals("id must not be null", ex.getMessage());
        builder.setID(id);

        builder.setID(emptyID);
        ex = Assertions.assertThrows(IllegalArgumentException.class,
                builder::build);
        Assertions.assertEquals("id must not be empty", ex.getMessage());
        builder.setID(id);

        builder.setCurrency(nullCurrency);
        ex = Assertions.assertThrows(NullPointerException.class,
                builder::build);
        Assertions.assertEquals("currency must not be null", ex.getMessage());
        builder.setCurrency(jod);

        builder.setAmount(nullAmount);
        ex = Assertions.assertThrows(NullPointerException.class,
                builder::build);
        Assertions.assertEquals("amount must not be null", ex.getMessage());
        builder.setAmount(amount);

        builder.setAmount(negativeAmount);
        ex = Assertions.assertThrows(IllegalArgumentException.class,
                builder::build);
        Assertions.assertEquals("amount must not be negative", ex.getMessage());
        builder.setAmount(amount);

        builder.setDate(nullDate);
        ex = Assertions.assertThrows(NullPointerException.class,
                builder::build);
        Assertions.assertEquals("date must not be null", ex.getMessage());
        builder.setDate(date);
    }

    @Test
    public void givenValues_whenCreateRecord_thenSuccess() {
        String id = "TR-47884222202";
        Currency jod = Currency.getInstance("JOD");
        BigDecimal amount = new BigDecimal("10.2585");
        LocalDate date = LocalDate.of(2019, Month.MAY, 19);
        Record record = new Record.Builder()
                .setID(id)
                .setCurrency(jod)
                .setAmount(amount)
                .setDate(date)
                .build();

        BigDecimal expectedAmount = amount.setScale(jod.getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
        Assertions.assertEquals(id, record.getID(), "stored value was not as given");
        Assertions.assertEquals(jod, record.getCurrency(), "stored value was not as given");
        Assertions.assertEquals(expectedAmount, record.getAmount(), "stored value was not as given");
        Assertions.assertEquals(date, record.getDate(), "stored value was not as given");
    }

    @Test
    public void givenRecords_whenEqualsAndHashcode_thenFollowSpecifications() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record3 = new Record.Builder()
                .setID("TR-47884666202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record4 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.JUNE, 19))
                .build();

        Assertions.assertFalse(record1.equals(null), "a record was equal to null");
        Assertions.assertTrue(record1.equals(record1), "a record was not equal to itself");
        Assertions.assertTrue(record1.equals(record2), "false returned on equal records");
        Assertions.assertFalse(record1.equals(record3), "true returned on unequal records");
        Assertions.assertFalse(record2.equals(record4), "true returned on unequal records");
        Assertions.assertEquals(record1.hashCode(), record2.hashCode(), "equal records did not have equal hashCodes");
    }

    @Test
    public void whenSerialize_thenDeserializeCorrectly() {
        Record record = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record[] deserialized = new Record[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(record);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (Record) ois.readObject();
        });

        Assertions.assertEquals(record, deserialized[0], "deserialized record was not as expected");
    }
}
