package com.progressoft.jip9.reconciliation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;

public class DelegatingMatchResultHandlerTest {
    @Test
    public void givenRecordStores_whenHandle_thenRunAsExpected() {
        RecordStore matched = getMatched();
        RecordStore mismatchedSource = getMismatchedSource();
        RecordStore mismatchedTarget = getMismatchedTarget();
        RecordStore inSourceOnly = getInSourceOnly();
        RecordStore inTargetOnly = getInTargetOnly();
        DelegatingMatchResultHandler handler = new DelegatingMatchResultHandler();

        handler.handleMatched(matched);
        handler.handleMismatched(mismatchedSource, mismatchedTarget);
        handler.handleMissing(inSourceOnly, inTargetOnly);

        Assertions.assertEquals(matched, handler.getMatched());
        Assertions.assertEquals(mismatchedSource, handler.getMismatchedSource());
        Assertions.assertEquals(mismatchedTarget, handler.getMismatchedTarget());
        Assertions.assertEquals(inSourceOnly, handler.getInSourceOnly());
        Assertions.assertEquals(inTargetOnly, handler.getInTargetOnly());

        handler.handleMatched(mismatchedSource);
        Assertions.assertEquals(mismatchedSource, handler.getMatched());
    }

    @Test
    public void givenDelegatingHandlerWithUncalledMethod_whenDelegate_thenThrowException() {
        DelegatingMatchResultHandler handler = new DelegatingMatchResultHandler();
        MatchResultHandler acceptor = getDoNothingAcceptor();
        IllegalStateException ex = Assertions.assertThrows(IllegalStateException.class,
                () -> handler.delegate(acceptor));
        Assertions.assertEquals("not all handle methods were called", ex.getMessage());
    }

    @Test
    public void givenMatchResultHandler_whenDelegate_thenRecordStoresAreDelegated() {
        RecordStore matched = getMatched();
        RecordStore mismatchedSource = getMismatchedSource();
        RecordStore mismatchedTarget = getMismatchedTarget();
        RecordStore inSourceOnly = getInSourceOnly();
        RecordStore inTargetOnly = getInTargetOnly();
        DelegatingMatchResultHandler handler = new DelegatingMatchResultHandler();

        handler.handleMatched(matched);
        handler.handleMismatched(mismatchedSource, mismatchedTarget);
        handler.handleMissing(inSourceOnly, inTargetOnly);

        boolean[] called = new boolean[3];
        MatchResultHandler acceptor = new MatchResultHandler() {
            @Override
            public void handleMatched(RecordStore recordStore) {
                Assertions.assertEquals(matched, recordStore);
                Assertions.assertFalse(called[0], "handleMatched was called more than once");
                called[0] = true;
            }

            @Override
            public void handleMismatched(RecordStore source, RecordStore target) {
                Assertions.assertEquals(source, mismatchedSource);
                Assertions.assertEquals(target, mismatchedTarget);
                Assertions.assertFalse(called[1], "handleMismatched was called more than once");
                called[1] = true;
            }

            @Override
            public void handleMissing(RecordStore _inSourceOnly, RecordStore _inTargetOnly) {
                Assertions.assertEquals(inSourceOnly, _inSourceOnly);
                Assertions.assertEquals(inTargetOnly, _inTargetOnly);
                Assertions.assertFalse(called[2], "handleMissing was called more than once");
                called[2] = true;
            }
        };

        handler.delegate(acceptor);
        boolean[] expected = {true, true, true};
        Assertions.assertArrayEquals(expected, called, "not all handle methods were called");
    }

    private MatchResultHandler getDoNothingAcceptor() {
        return new MatchResultHandler() {
            @Override
            public void handleMatched(RecordStore recordStore) {

            }

            @Override
            public void handleMismatched(RecordStore source, RecordStore target) {

            }

            @Override
            public void handleMissing(RecordStore inSourceOnly, RecordStore inTargetOnly) {

            }
        };
    }


    private RecordStore getInTargetOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222199")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("25.90"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getInSourceOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.0"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getMismatchedTarget() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("2.6"))
                .setDate(LocalDate.of(2019, Month.OCTOBER, 12))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222288")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("8"))
                .setDate(LocalDate.of(2020, Month.AUGUST, 9))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        store.add(record2);
        return store;
    }

    private RecordStore getMismatchedSource() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222288")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("9"))
                .setDate(LocalDate.of(2020, Month.AUGUST, 9))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        store.add(record2);
        return store;
    }

    private RecordStore getMatched() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.25"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.35"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        return store;
    }

    @Test
    public void whenSerialize_thenDeserializeCorrectly() {
        DelegatingMatchResultHandler handler = new DelegatingMatchResultHandler();
        handler.handleMatched(getMatched());
        handler.handleMismatched(getMismatchedSource(), getMismatchedTarget());
        handler.handleMissing(getInSourceOnly(), getInTargetOnly());
        DelegatingMatchResultHandler[] deserialized = new DelegatingMatchResultHandler[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(handler);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (DelegatingMatchResultHandler) ois.readObject();
        });

        Assertions.assertEquals(handler.getMatched(), deserialized[0].getMatched(), "deserialized handler was not as expected");
        Assertions.assertEquals(handler.getMismatchedSource(), deserialized[0].getMismatchedSource(), "deserialized handler was not as expected");
        Assertions.assertEquals(handler.getMismatchedTarget(), deserialized[0].getMismatchedTarget(), "deserialized handler was not as expected");
        Assertions.assertEquals(handler.getInSourceOnly(), deserialized[0].getInSourceOnly(), "deserialized handler was not as expected");
        Assertions.assertEquals(handler.getInTargetOnly(), deserialized[0].getInTargetOnly(), "deserialized handler was not as expected");
    }
}
