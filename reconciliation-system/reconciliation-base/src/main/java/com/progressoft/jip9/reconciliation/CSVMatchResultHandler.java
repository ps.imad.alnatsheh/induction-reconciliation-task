package com.progressoft.jip9.reconciliation;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class CSVMatchResultHandler implements MatchResultHandler {
    private static final CSVFormat MATCHED_FORMAT = CSVFormat.DEFAULT.withQuote(null).withRecordSeparator('\n')
            .withHeader("transaction id","amount","currency code","value date");
    private static final CSVFormat FOUND_IN_FILE_FORMAT = CSVFormat.DEFAULT.withQuote(null).withRecordSeparator('\n')
            .withHeader("found in file", "transaction id","amount","currency code","value date");

    private final ResultWriters resultWriters;

    public CSVMatchResultHandler(ResultWriters resultWriters) {
        this.resultWriters = Objects.requireNonNull(resultWriters, "given ResultWriters was null");
    }

    @Override
    public void handleMatched(RecordStore recordStore) {
        try (CSVPrinter printer = MATCHED_FORMAT.print(resultWriters.getMatchedStream())) {
            for (Record record : recordStore) {
                printer.printRecord( getMatchedFields(record));
            }
        } catch (IOException e) {
            throw new ResultWritingException("an error occurred while writing matched records CSV: " + e.getMessage(), e);
        }
    }

    private Object[] getMatchedFields(Record record) {
        return new Object[]{record.getID(), getAmountAndFixScale(record),
                record.getCurrency().getCurrencyCode(), record.getDate()};
    }

    private Object[] getFieldsWithFoundIn(Record record, String foundIn) {
        return new Object[]{foundIn, record.getID(), getAmountAndFixScale(record),
                record.getCurrency().getCurrencyCode(), record.getDate()};
    }
    @Override
    public void handleMismatched(RecordStore source, RecordStore target) {
        try (CSVPrinter printer = FOUND_IN_FILE_FORMAT.print(resultWriters.getMismatchedStream())) {
            for (Record sourceRecord : source) {
                Record targetRecord = target.get(sourceRecord.getID());
                printer.printRecord(getFieldsWithFoundIn(sourceRecord, "SOURCE"));
                printer.printRecord(getFieldsWithFoundIn(targetRecord, "TARGET"));
            }
        } catch (IOException e) {
            throw new ResultWritingException("an error occurred while writing mismatched records CSV: " + e.getMessage(), e);
        }
    }

    @Override
    public void handleMissing(RecordStore inSourceOnly, RecordStore inTargetOnly) {
        try (CSVPrinter printer = FOUND_IN_FILE_FORMAT.print(resultWriters.getMissingStream())) {
            for (Record record : inSourceOnly) {
                printer.printRecord(getFieldsWithFoundIn(record, "SOURCE"));
            }
            for (Record record : inTargetOnly) {
                printer.printRecord(getFieldsWithFoundIn(record, "TARGET"));
            }
        } catch (IOException e) {
            throw new ResultWritingException("an error occurred while writing missing records CSV: " + e.getMessage(), e);
        }
    }

    private BigDecimal getAmountAndFixScale(Record r) {
        return r.getAmount().setScale(r.getCurrency().getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
    }
}
