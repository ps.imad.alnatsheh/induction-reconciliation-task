package com.progressoft.jip9.reconciliation;

public interface MatchResultHandler {

    void handleMatched(RecordStore recordStore);

    void handleMismatched(RecordStore source, RecordStore target);

    void handleMissing(RecordStore inSourceOnly, RecordStore inTargetOnly);
}
