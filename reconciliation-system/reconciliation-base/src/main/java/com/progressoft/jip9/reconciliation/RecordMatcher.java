package com.progressoft.jip9.reconciliation;

import java.io.Serializable;
import java.util.Objects;

public class RecordMatcher implements Serializable {
    private static final long serialVersionUID = -5209991794591243439L;
    private final MatchResultHandler matchResultHandler;

    public RecordMatcher(MatchResultHandler matchResultHandler) {
        this.matchResultHandler = Objects.requireNonNull(matchResultHandler, "null matchResultHandler was given");
    }

    public void match(RecordStore source, RecordStore target) {
        source = getOrEmpty(source);
        target = getOrEmpty(target);

        MatchingProcessor processor = new MatchingProcessor();
        processor.process(source, target);

        matchResultHandler.handleMatched(processor.getMatched());
        matchResultHandler.handleMismatched(processor.getMismatchedSource(), processor.getMismatchedTarget());
        matchResultHandler.handleMissing(processor.getInSourceOnly(), processor.getInTargetOnly());
    }

    private RecordStore getOrEmpty(RecordStore store) {
        return store == null ? RecordStore.EMPTY : store;
    }

    private static class MatchingProcessor {
        private final RecordStore matched;
        private final RecordStore mismatchedSource;
        private final RecordStore mismatchedTarget;
        private final RecordStore inSourceOnly;
        private final RecordStore inTargetOnly;

        private MatchingProcessor() {
            this.matched = new RecordStore();
            this.mismatchedSource = new RecordStore();
            this.mismatchedTarget = new RecordStore();
            this.inSourceOnly = new RecordStore();
            this.inTargetOnly = new RecordStore();
        }

        public RecordStore getMatched() {
            return matched;
        }

        public RecordStore getMismatchedSource() {
            return mismatchedSource;
        }

        public RecordStore getMismatchedTarget() {
            return mismatchedTarget;
        }

        public RecordStore getInSourceOnly() {
            return inSourceOnly;
        }

        public RecordStore getInTargetOnly() {
            return inTargetOnly;
        }

        public void process(RecordStore source, RecordStore target) {
            processSource(source, target);
            processTarget(source,target);
        }

        private void processSource(RecordStore source, RecordStore target) {
            for (Record sourceRecord : source) {
                String sourceID = sourceRecord.getID();
                Record targetRecord = target.get(sourceID);
                if(addIfMissing(targetRecord, sourceRecord, inSourceOnly)) {
                    continue;
                }
                if (addIfMatched(sourceRecord, targetRecord)) {
                    continue;
                }
                addToMismatched(sourceRecord, targetRecord);
            }
        }

        private void processTarget(RecordStore source, RecordStore target) {
            for (Record targetRecord : target) {
                String targetID = targetRecord.getID();
                Record sourceRecord = source.get(targetID);
                addIfMissing(sourceRecord, targetRecord, inTargetOnly);
            }
        }

        private void addToMismatched(Record sourceRecord, Record targetRecord) {
            mismatchedSource.add(sourceRecord);
            mismatchedTarget.add(targetRecord);
        }

        private boolean addIfMatched(Record sourceRecord, Record targetRecord) {
            if(targetRecord.equals(sourceRecord)) {
                matched.add(sourceRecord);
                return true;
            }
            return false;
        }

        private boolean addIfMissing(Record missing, Record exists, RecordStore toAddTo) {
            if(missing == null) {
                toAddTo.add(exists);
                return true;
            }
            return false;
        }
    }
}
