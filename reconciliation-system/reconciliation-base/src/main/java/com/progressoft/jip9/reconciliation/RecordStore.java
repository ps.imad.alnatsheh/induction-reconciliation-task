package com.progressoft.jip9.reconciliation;

import java.io.Serializable;
import java.util.*;

public class RecordStore implements Iterable<Record>, Serializable {
    public static final RecordStore EMPTY = new RecordStore() {
        private static final long serialVersionUID = -3721253369393403140L;
        @Override
        public Record add(Record record) {
            return null;
        }

        @Override
        public Record get(String transactionID) {
            return null;
        }
    };

    private static final long serialVersionUID = -705861060980196379L;

    Map<String, Record> recordMap;

    public RecordStore() {
        recordMap = new TreeMap<>();
    }

    public Record add(Record record) {
        Objects.requireNonNull(record, "record to add was null");
        return recordMap.put(record.getID(), record);
    }

    public boolean isEmpty() {
        return recordMap.isEmpty();
    }

    public int size() {
        return recordMap.size();
    }

    public Record get(String transactionID) {
        return recordMap.get(transactionID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecordStore store = (RecordStore) o;
        return recordMap.equals(store.recordMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recordMap);
    }

    @Override
    public Iterator<Record> iterator() {
        return Collections.unmodifiableCollection(recordMap.values()).iterator();
    }

    @Override
    public String toString() {
        return recordMap.values().toString();
    }
}
