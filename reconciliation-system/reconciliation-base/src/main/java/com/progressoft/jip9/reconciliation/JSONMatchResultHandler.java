package com.progressoft.jip9.reconciliation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class JSONMatchResultHandler implements MatchResultHandler {
    public static final String FOUND_IN_FILE_FIELD = "found in file";
    public static final String TRANSACTION_ID_FIELD = "transaction id";
    public static final String AMOUNT_FIELD = "amount";
    public static final String CURRENCY_CODE_FIELD = "currency code";
    public static final String VALUE_DATE_FIELD = "value date";
    public static final String IN_FILE_SOURCE = "SOURCE";
    public static final String IN_FILE_TARGET = "TARGET";
    private final ResultWriters resultWriters;
    private ObjectMapper mapper;

    public JSONMatchResultHandler(ResultWriters resultWriters) {
        this.resultWriters = Objects.requireNonNull(resultWriters, "given ResultWriters was null");
        initMapper();
    }

    @Override
    public void handleMatched(RecordStore recordStore) {
        try (JsonGenerator generator = mapper.createGenerator(resultWriters.getMatchedStream()).useDefaultPrettyPrinter()) {
            generator.writeStartArray();
            for (Record record : recordStore) {
                writeMatchedRecord(record, generator);
            }
            generator.writeEndArray();
        } catch (IOException e) {
            throw new ResultWritingException("an error occurred while writing matched records JSON: " + e.getMessage(), e);
        }
    }

    @Override
    public void handleMismatched(RecordStore source, RecordStore target) {
        try (JsonGenerator generator = mapper.createGenerator(resultWriters.getMismatchedStream()).useDefaultPrettyPrinter()) {
            generator.writeStartArray();
            for (Record sourceRecord : source) {
                Record targetRecord = target.get(sourceRecord.getID());
                writeInFileRecord(sourceRecord, IN_FILE_SOURCE, generator);
                writeInFileRecord(targetRecord, IN_FILE_TARGET, generator);
            }
            generator.writeEndArray();
        } catch (IOException e) {
            throw new ResultWritingException("an error occurred while writing mismatched records JSON: " + e.getMessage(), e);
        }
    }

    @Override
    public void handleMissing(RecordStore inSourceOnly, RecordStore inTargetOnly) {
        try (JsonGenerator generator = mapper.createGenerator(resultWriters.getMissingStream()).useDefaultPrettyPrinter()) {
            generator.writeStartArray();
            for (Record record : inSourceOnly) {
                writeInFileRecord(record, IN_FILE_SOURCE, generator);
            }
            for (Record record : inTargetOnly) {
                writeInFileRecord(record, IN_FILE_TARGET, generator);
            }
            generator.writeEndArray();
        } catch (IOException e) {
            throw new ResultWritingException("an error occurred while writing missing records JSON: " + e.getMessage(), e);
        }
    }

    private void writeInFileRecord(Record record, String inFile, JsonGenerator generator) throws IOException {
        generator.writeStartObject();
        generator.writeStringField(FOUND_IN_FILE_FIELD, inFile);
        writeRecordFields(record, generator);
        generator.writeEndObject();
    }

    private void writeMatchedRecord(Record record, JsonGenerator generator) throws IOException {
        generator.writeStartObject();
        writeRecordFields(record, generator);
        generator.writeEndObject();
    }

    private void writeRecordFields(Record record, JsonGenerator generator) throws IOException {
        generator.writeStringField(TRANSACTION_ID_FIELD, record.getID());
        generator.writeStringField(AMOUNT_FIELD, getAmountAndFixScale(record).toString());
        generator.writeStringField(CURRENCY_CODE_FIELD, record.getCurrency().getCurrencyCode());
        generator.writeStringField(VALUE_DATE_FIELD, record.getDate().toString());
    }

    private BigDecimal getAmountAndFixScale(Record r) {
        return r.getAmount().setScale(r.getCurrency().getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
    }

    private void initMapper() {
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setDefaultPrettyPrinter(new CustomPrettyPrinter());
    }

    private static class CustomPrettyPrinter extends DefaultPrettyPrinter {
        public CustomPrettyPrinter() {
            indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
        }

        @Override
        public DefaultPrettyPrinter createInstance() {
            return new CustomPrettyPrinter();
        }

        @Override
        public void writeObjectFieldValueSeparator(JsonGenerator g) throws IOException {
            g.writeRaw(": ");
        }
    }
}
