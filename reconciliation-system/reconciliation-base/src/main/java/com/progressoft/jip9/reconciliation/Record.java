package com.progressoft.jip9.reconciliation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Objects;

public class Record implements Serializable {
    private static final long serialVersionUID = -8254681383484406720L;
    private final String id;
    private final Currency currency;
    private final BigDecimal amount;
    private final LocalDate date;

    private Record(Builder builder) {
        validate(builder);
        this.id = builder.id;
        this.currency = builder.currency;
        this.amount = fixScale(builder.amount, builder.currency);
        this.date = builder.date;
    }

    private BigDecimal fixScale(BigDecimal amount, Currency currency) {
        return amount.setScale(currency.getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
    }

    private static void validate(Builder builder) {
        validateID(builder.id);
        validateCurrency(builder.currency);
        validateAmount(builder.amount);
        validateDate(builder.date);
    }

    private static void validateDate(LocalDate date) {
        Objects.requireNonNull(date, "date must not be null");
    }

    private static void validateAmount(BigDecimal amount) {
        Objects.requireNonNull(amount, "amount must not be null");
        if (amount.compareTo(BigDecimal.ZERO) < 0)
            throw new IllegalArgumentException("amount must not be negative");
    }

    private static void validateCurrency(Currency currency) {
        Objects.requireNonNull(currency, "currency must not be null");
    }

    private static void validateID(String id) {
        Objects.requireNonNull(id, "id must not be null");
        if (id.trim().equals(""))
            throw new IllegalArgumentException("id must not be empty");
    }

    public String getID() {
        return id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return Objects.equals(id, record.id) &&
                Objects.equals(currency, record.currency) &&
                Objects.equals(amount, record.amount) &&
                Objects.equals(date, record.date);
    }

    int hashCache = 0;

    @Override
    public int hashCode() {
        return hashCache == 0 ? (hashCache = Objects.hash(id, currency, amount, date)) : hashCache;
    }

    @Override
    public String toString() {
        return id + ": " + amount + ' ' + currency + " in " + date;
    }

    public static class Builder {
        private String id;
        private Currency currency;
        private BigDecimal amount;
        private LocalDate date;

        public Builder setID(String id) {
            this.id = id;
            return this;
        }

        public String getID() {
            return id;
        }

        public Builder setCurrency(Currency currency) {
            this.currency = currency;
            return this;
        }

        public Currency getCurrency() {
            return currency;
        }

        public Builder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public Builder setDate(LocalDate date) {
            this.date = date;
            return this;
        }

        public LocalDate getDate() {
            return date;
        }

        public Record build() {
            return new Record(this);
        }
    }
}
