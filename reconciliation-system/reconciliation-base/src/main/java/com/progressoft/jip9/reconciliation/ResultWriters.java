package com.progressoft.jip9.reconciliation;

import java.io.IOException;
import java.io.Writer;

public interface ResultWriters {
    Writer getMatchedStream() throws IOException;

    Writer getMismatchedStream() throws IOException;

    Writer getMissingStream() throws IOException;
}
