package com.progressoft.jip9.reconciliation;

import java.io.IOException;

public class ResultWritingException extends RuntimeException {
    public ResultWritingException(String message, IOException cause) {
        super(message, cause);
    }

    public ResultWritingException(IOException cause) {
        super(cause);
    }
}
