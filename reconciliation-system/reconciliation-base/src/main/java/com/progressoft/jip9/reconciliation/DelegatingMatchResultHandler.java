package com.progressoft.jip9.reconciliation;

import java.io.Serializable;

public class DelegatingMatchResultHandler implements MatchResultHandler, Serializable {
    private static final long serialVersionUID = 7650186869201125090L;

    private RecordStore matched;
    private RecordStore mismatchedSource;
    private RecordStore mismatchedTarget;
    private RecordStore inSourceOnly;
    private RecordStore inTargetOnly;

    private boolean matchCalled = false;
    private boolean mismatchCalled = false;
    private boolean missingCalled = false;


    @Override
    public void handleMatched(RecordStore matched) {
        this.matched = matched;
        matchCalled = true;
    }

    @Override
    public void handleMismatched(RecordStore source, RecordStore target) {
        mismatchedSource = source;
        mismatchedTarget = target;
        mismatchCalled = true;
    }

    @Override
    public void handleMissing(RecordStore inSourceOnly, RecordStore inTargetOnly) {
        this.inSourceOnly = inSourceOnly;
        this.inTargetOnly = inTargetOnly;
        missingCalled = true;
    }

    public RecordStore getMatched() {
        return matched;
    }

    public RecordStore getMismatchedSource() {
        return mismatchedSource;
    }

    public RecordStore getMismatchedTarget() {
        return mismatchedTarget;
    }

    public RecordStore getInSourceOnly() {
        return inSourceOnly;
    }

    public RecordStore getInTargetOnly() {
        return inTargetOnly;
    }

    public void delegate(MatchResultHandler acceptor) {
        throwIfNotAllHandleMethodsCalled();
        acceptor.handleMatched(matched);
        acceptor.handleMismatched(mismatchedSource, mismatchedTarget);
        acceptor.handleMissing(inSourceOnly, inTargetOnly);
    }

    private void throwIfNotAllHandleMethodsCalled() {
        if(!areAllHandleMethodsCalled())
            throw new IllegalStateException("not all handle methods were called");
    }

    private boolean areAllHandleMethodsCalled() {
        return matchCalled && mismatchCalled && missingCalled;
    }
}
