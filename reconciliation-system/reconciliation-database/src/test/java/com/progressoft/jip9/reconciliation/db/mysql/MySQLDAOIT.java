package com.progressoft.jip9.reconciliation.db.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.progressoft.jip9.reconciliation.account.cred.UsernameAlreadyExistsException;
import com.progressoft.jip9.reconciliation.db.AccountDatabaseInitializer;
import com.progressoft.jip9.reconciliation.db.TestingUtils;
import com.progressoft.jip9.reconciliation.account.HistoryEntry;
import com.progressoft.jip9.reconciliation.db.dto.UserDTO;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class MySQLDAOIT {
    public static final String URL = "jdbc:mysql://localhost:3306/RECON_ACCOUNT_TEST";
    public static DataSource limitingDS;
    public static MysqlDataSource ds;

    @BeforeAll
    public static void reinitialize() throws SQLException {
        ds = new MysqlDataSource();
        ds.setURL(URL);
        ds.setUser("root");
        ds.setPassword("root");
        ds.setServerTimezone("Asia/Amman");
        ds.setCreateDatabaseIfNotExist(true);
        AccountDatabaseInitializer dbinit = new AccountDatabaseInitializer();
        dbinit.initialize(ds);
        limitingDS = TestingUtils.getLimitingDataSource(ds);
    }

    @BeforeEach
    public void truncateTablesBeforeEach() {
        truncateTables();
    }

    @AfterAll
    public static void truncateTables() {
        try (Connection conn = ds.getConnection()){
            try(Statement statement = conn.createStatement()) {
                statement.execute("SET FOREIGN_KEY_CHECKS = 0");
                statement.execute("TRUNCATE TABLE RECON_HISTORY");
                statement.execute("TRUNCATE TABLE RECON_USER");
                statement.execute("SET FOREIGN_KEY_CHECKS = 1");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void whenOperateUserDAO_thenSuccess() {
        MySQLUserDAO userDAO = new MySQLUserDAO(limitingDS);

        String username = "ahmad123";
        String passhash = "xdERfg";
        Assertions.assertEquals(1, userDAO.addUser(username, passhash), "returned ID was not a expected");
        UserDTO expected = new UserDTO().setUserID(1).setUsername(username).setPasswordHash(passhash);
        Assertions.assertEquals(expected, userDAO.getByUsername(username), "returned user was not as expected");

        String newUsername = "ahmad12";
        String newPasshash = "GrREws";

        Assertions.assertTrue(userDAO.updatePasswordHash(1, newPasshash), "update did not happen");
        Assertions.assertEquals(expected.setPasswordHash(newPasshash), userDAO.getByUsername(username), "update was not reflected");

        Assertions.assertTrue(userDAO.updateUsername(1, newUsername), "update did not happen");
        Assertions.assertEquals(expected.setUsername(newUsername), userDAO.getByUsername(newUsername), "update was not reflected");

        Assertions.assertEquals(2, userDAO.addUser(username, passhash), "new user did not have expected id");

        Exception ex = Assertions.assertThrows(UsernameAlreadyExistsException.class,
                () -> userDAO.addUser(username, newPasshash), "identical username should throw");
        Assertions.assertEquals("given username already exists in the database", ex.getMessage());

        ex = Assertions.assertThrows(UsernameAlreadyExistsException.class,
                () -> userDAO.updateUsername(1, username), "identical username should throw");
        Assertions.assertEquals("given username already exists in the database", ex.getMessage());
    }

    @Test
    public void whenOperateHistory_thenSuccess() {
        MySQLUserDAO userDAO = new MySQLUserDAO(limitingDS);
        MySQLHistoryDAO historyDAO = new MySQLHistoryDAO(limitingDS);
        String username1 = "ahmad123";
        String passhash1 = "xdERfg";
        String username2 = "ahmad12";
        String passhash2 = "GrREws";

        long userID1 = userDAO.addUser(username1, passhash1);
        long userID2 = userDAO.addUser(username2, passhash2);

        long session1time = Instant.now().toEpochMilli();
        long session2time = session1time + 10000;
        long session3time = session1time + 20000;

        HistoryEntry entry1DTO = getEntry1(session1time);
        HistoryEntry entry2DTO = getEntry2(session1time);
        HistoryEntry entry3DTO = getEntry3(session3time);
        HistoryEntry entry4DTO = getEntry3(session2time);

        Assertions.assertTrue(historyDAO.insertHistoryEntry(userID1, entry1DTO), "insert operation did not happen");
        Assertions.assertTrue(historyDAO.insertHistoryEntry(userID1, entry2DTO), "insert operation did not happen");
        Assertions.assertTrue(historyDAO.insertHistoryEntry(userID2, entry3DTO), "insert operation did not happen");
        Assertions.assertTrue(historyDAO.insertHistoryEntry(userID1, entry4DTO), "insert operation did not happen");

        List<HistoryEntry> user1history = Arrays.asList(entry1DTO,entry2DTO,entry4DTO);
        List<HistoryEntry> user2history = Collections.singletonList(entry3DTO);
        Map<Long, List<HistoryEntry>> user1map = getHistoryMap(user1history);
        Map<Long, List<HistoryEntry>> user2map = getHistoryMap(user2history);

        Assertions.assertEquals(user1history, historyDAO.getAllForUser(userID1), "user 1 history was not as expected");
        Assertions.assertEquals(user2history, historyDAO.getAllForUser(userID2), "user 2 history was not as expected");
        Assertions.assertEquals(user1map, historyDAO.getAllForUserGroupedBySession(userID1), "user 1 history map was not as expected");
        Assertions.assertEquals(user2map, historyDAO.getAllForUserGroupedBySession(userID2), "user 2 history map was not as expected");
    }

    private Map<Long, List<HistoryEntry>> getHistoryMap(List<HistoryEntry> user1history) {
        return user1history.stream().collect(Collectors.groupingBy(HistoryEntry::getSessionCreationTime));
    }

    private HistoryEntry getEntry1(long session) {
        return new HistoryEntry()
                .setSessionCreationTime(session)
                .setMatchingTime(session + 100)
                .setSourceFileName("source.csv")
                .setTargetFileName("target.json")
                .setMatched(10)
                .setMismatched(2)
                .setMissing(1);
    }

    private HistoryEntry getEntry2(long session) {
        return new HistoryEntry()
                .setSessionCreationTime(session)
                .setMatchingTime(session + 150)
                .setSourceFileName("source.json")
                .setTargetFileName("target.json")
                .setMatched(11)
                .setMismatched(2)
                .setMissing(1);
    }

    private HistoryEntry getEntry3(long session) {
        return new HistoryEntry()
                .setSessionCreationTime(session)
                .setMatchingTime(session + 1000)
                .setSourceFileName("source.csv")
                .setTargetFileName("target.csv")
                .setMatched(19)
                .setMismatched(0)
                .setMissing(10);
    }
}
