package com.progressoft.jip9.reconciliation.db;

import com.progressoft.jip9.reconciliation.account.User;
import com.progressoft.jip9.reconciliation.account.cred.*;
import com.progressoft.jip9.reconciliation.db.dao.DAOFactory;
import com.progressoft.jip9.reconciliation.db.dao.HistoryDAO;
import com.progressoft.jip9.reconciliation.db.dao.UserDAO;
import com.progressoft.jip9.reconciliation.account.HistoryEntry;
import com.progressoft.jip9.reconciliation.db.dto.UserDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.time.Instant;
import java.util.*;


public class DBUserSourceTest {
    @Test
    public void givenNullInputs_whenCreate_thenThrowException(){
        PasswordManager nullManager = null;
        DAOFactory nullDAOFactory = null;
        PasswordManager passwordManager = getPasswordManager();
        DAOFactory daoFactory = new SimpleDAOFactory(passwordManager, null);
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new DBUserSource(nullDAOFactory, passwordManager));
        Assertions.assertEquals("null daoFactory was given", ex.getMessage());

        ex = Assertions.assertThrows(NullPointerException.class,
                () -> new DBUserSource(daoFactory, nullManager));
        Assertions.assertEquals("null passwordManager was given", ex.getMessage());
    }

    @Test
    public void givenInvalidCredentials_whenLoginOrAddUser_thenThrowException() {
        PasswordManager passwordManager = getPasswordManager();
        DAOFactory daoFactory = new SimpleDAOFactory(passwordManager, null);
        DBUserSource userSource = new DBUserSource(daoFactory, passwordManager);

        Credentials nullCreds = null;
        Credentials unsupported = new Credentials() {};
        Credentials usernameUnknown = new UsernamePasswordCredentials("asdf", "fdsa".toCharArray());
        Credentials wrongPassword = new UsernamePasswordCredentials("imad1234", "1171998".toCharArray());
        Credentials duplicateUser = new UsernamePasswordCredentials("imad1234", "1171998".toCharArray());

        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> userSource.login(nullCreds));
        Assertions.assertEquals("null credentials were given", ex.getMessage());

        ex = Assertions.assertThrows(NullPointerException.class,
                () -> userSource.addUser(nullCreds));
        Assertions.assertEquals("null credentials were given", ex.getMessage());

        UnsupportedCredentialsException unsupportedEx = Assertions.assertThrows(UnsupportedCredentialsException.class,
                () -> userSource.login(unsupported));
        Assertions.assertEquals(unsupported.getClass(), unsupportedEx.getCausingClass());

        unsupportedEx = Assertions.assertThrows(UnsupportedCredentialsException.class,
                () -> userSource.addUser(unsupported));
        Assertions.assertEquals(unsupported.getClass(), unsupportedEx.getCausingClass());

        ex = Assertions.assertThrows(UnknownUsernameException.class,
                () -> userSource.login(usernameUnknown));
        Assertions.assertEquals("unknown username", ex.getMessage());

        ex = Assertions.assertThrows(PasswordDoesNotMatchException.class,
                () -> userSource.login(wrongPassword));
        Assertions.assertEquals("password does not match the given username", ex.getMessage());

        Assertions.assertThrows(UsernameAlreadyExistsException.class,
                () -> userSource.addUser(duplicateUser));
    }

    @Test
    public void givenNotLoggedInUser_whenAddOrGetHistory_thenThrowException() {
        User unauthenticatedUser = getUnauthenticatedUser();
        PasswordManager passwordManager = getPasswordManager();
        DAOFactory daoFactory = new SimpleDAOFactory(passwordManager, null);
        DBUserSource userSource = new DBUserSource(daoFactory, passwordManager);

        Exception ex = Assertions.assertThrows(UnauthenticatedUserException.class,
                () -> userSource.insertHistoryEntry(unauthenticatedUser, new HistoryEntry()));
        Assertions.assertEquals("given user is not authenticated", ex.getMessage());

        ex = Assertions.assertThrows(UnauthenticatedUserException.class,
                () -> userSource.getEntriesByUser(unauthenticatedUser));
        Assertions.assertEquals("given user is not authenticated", ex.getMessage());
    }

    @Test
    public void givenValidCredentials_whenOperate_thenSuccess() {
        PasswordManager passwordManager = getPasswordManager();
        HistoryEntry toInsert = getEntry1(123456789);
        SimpleDAOFactory daoFactory = new SimpleDAOFactory(passwordManager, toInsert);
        DBUserSource userSource = new DBUserSource(daoFactory, passwordManager);
        UsernamePasswordCredentials loginCreds = new UsernamePasswordCredentials("imad1234", "1161998".toCharArray());
        UsernamePasswordCredentials newUserCreds = new UsernamePasswordCredentials("imad4321", "1171998".toCharArray());

        long expectedLoginID = 34, expectedNewID = 35;

        Assertions.assertEquals(expectedLoginID, userSource.login(loginCreds), "returned ID was not as expected");
        Assertions.assertEquals(expectedNewID, userSource.addUser(newUserCreds), "returned ID was not as expected");

        User user = getAuthenticatedUser(expectedLoginID);

        Assertions.assertEquals(getEntriesMap(), userSource.getEntriesByUser(user), "entries were not was expected");
        userSource.insertHistoryEntry(user, toInsert);
        Assertions.assertTrue(daoFactory.insertHistoryEntryCalled, "insertHistoryEntry was not called on the HistoryDAO");
    }

    @Test
    public void whenSerialize_thenDeserializeCorrectly() {
        PasswordManager passwordManager = getPasswordManager();
        DAOFactory daoFactory = new SimpleDAOFactory(passwordManager, null);
        DBUserSource userSource = new DBUserSource(daoFactory, passwordManager);

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(userSource);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            ois.readObject();
        });
    }

    private User getUnauthenticatedUser() {
        return new User() {
            @Override
            public boolean isLoggedIn() {
                return false;
            }

            @Override
            public long getUserID() {
                Assertions.fail("unnecessary call");
                return 0;
            }

            @Override
            public void login(Credentials creds) {
                Assertions.fail("unnecessary call");
            }

            @Override
            public void logout() {
                Assertions.fail("unnecessary call");
            }
        };
    }

    private User getAuthenticatedUser(long userID) {
        return new User() {
            @Override
            public boolean isLoggedIn() {
                return true;
            }

            @Override
            public long getUserID() {
                return userID;
            }

            @Override
            public void login(Credentials creds) {
            }

            @Override
            public void logout() {
            }
        };
    }

    private PasswordManager getPasswordManager() {
        PBKDF2PasswordHasher passwordHasher = new PBKDF2PasswordHasher(16, 32);
        return new PasswordManager(Collections.singleton(passwordHasher), passwordHasher.getID());
    }

    private static Map<Long, List<HistoryEntry>> getEntriesMap() {
        long session1time = 1600061406210L;
        long session2time = session1time + 10000;
        long session3time = session1time + 20000;

        HistoryEntry entry1DTO = getEntry1(session1time);
        HistoryEntry entry2DTO = getEntry2(session1time);
        HistoryEntry entry3DTO = getEntry3(session2time);
        HistoryEntry entry4DTO = getEntry3(session3time);
        Map<Long, List<HistoryEntry>> result = new HashMap<>();
        result.put(session1time, Arrays.asList(entry1DTO, entry2DTO));
        result.put(session2time, Collections.singletonList(entry3DTO));
        result.put(session3time, Collections.singletonList(entry4DTO));
        return result;
    }

    private static HistoryEntry getEntry1(long session) {
        return new HistoryEntry()
                .setSessionCreationTime(session)
                .setMatchingTime(session + 100)
                .setSourceFileName("source.csv")
                .setTargetFileName("target.json")
                .setMatched(10)
                .setMismatched(2)
                .setMissing(1);
    }

    private static HistoryEntry getEntry2(long session) {
        return new HistoryEntry()
                .setSessionCreationTime(session)
                .setMatchingTime(session + 150)
                .setSourceFileName("source.json")
                .setTargetFileName("target.json")
                .setMatched(11)
                .setMismatched(2)
                .setMissing(1);
    }

    private static HistoryEntry getEntry3(long session) {
        return new HistoryEntry()
                .setSessionCreationTime(session)
                .setMatchingTime(session + 1000)
                .setSourceFileName("source.csv")
                .setTargetFileName("target.csv")
                .setMatched(19)
                .setMismatched(0)
                .setMissing(10);
    }

    private static class SimpleDAOFactory implements DAOFactory, Serializable {
        private static final long serialVersionUID = -3034783953853651680L;

        private final String storedUsername;
        private final String storedPasshash;
        private final PasswordManager passwordManager;
        private final HistoryEntry expectedToInsert;

        public SimpleDAOFactory(PasswordManager passwordManager, HistoryEntry expectedToInsert) {
            this.passwordManager = passwordManager;
            this.expectedToInsert = expectedToInsert;
            storedUsername = "imad1234";
            storedPasshash = passwordManager.hash("1161998".toCharArray());
        }

        @Override
        public UserDAO getUserDAO() {
            return new UserDAO() {
                @Override
                public UserDTO getByUsername(String username) {
                    if(storedUsername.equals(username))
                        return new UserDTO().setUsername(storedUsername).setPasswordHash(storedPasshash).setUserID(34);
                    return null;
                }

                @Override
                public boolean updateUsername(long userID, String newUsername) {
                    Assertions.fail("unnecessary call");
                    return false;
                }

                @Override
                public boolean updatePasswordHash(long userID, String newPasswordHash) {
                    Assertions.fail("unnecessary call");
                    return false;
                }

                @Override
                public long addUser(String username, String passwordHash) {
                    if(storedUsername.equals(username))
                        throw new UsernameAlreadyExistsException();
                    Assertions.assertEquals("imad4321", username, "username was not stored as expected");
                    Assertions.assertTrue(passwordManager.match(passwordHash, "1171998".toCharArray()));
                    return 35;
                }
            };
        }

        public boolean insertHistoryEntryCalled = false;
        @Override
        public HistoryDAO getHistoryDAO() {
            return new HistoryDAO() {
                @Override
                public List<HistoryEntry> getAllForUser(long userID) {
                    Assertions.fail("unnecessary call");
                    return null;
                }

                @Override
                public Map<Long, List<HistoryEntry>> getAllForUserGroupedBySession(long userID) {
                    Assertions.assertEquals(34, userID, "userID was not as expected");
                    return getEntriesMap();
                }

                @Override
                public boolean insertHistoryEntry(long userID, HistoryEntry toInsert) {
                    Assertions.assertEquals(34, userID, "userID was not as expected");
                    Assertions.assertEquals(expectedToInsert, toInsert, "historyEntry was nto as expected");
                    Assertions.assertFalse(insertHistoryEntryCalled);
                    insertHistoryEntryCalled = true;
                    return true;
                }
            };
        }
    }
}
