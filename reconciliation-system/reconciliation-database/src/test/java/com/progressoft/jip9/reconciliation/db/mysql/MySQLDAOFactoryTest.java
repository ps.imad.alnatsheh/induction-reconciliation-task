package com.progressoft.jip9.reconciliation.db.mysql;

import com.progressoft.jip9.reconciliation.account.cred.PBKDF2PasswordHasher;
import com.progressoft.jip9.reconciliation.account.cred.PasswordHasher;
import com.progressoft.jip9.reconciliation.account.cred.PasswordManager;
import com.progressoft.jip9.reconciliation.db.DBUserSource;
import com.progressoft.jip9.reconciliation.db.SerializableSupplier;
import com.progressoft.jip9.reconciliation.db.TestingUtils;
import com.progressoft.jip9.reconciliation.db.dao.DAOFactory;
import com.progressoft.jip9.reconciliation.db.dao.HistoryDAO;
import com.progressoft.jip9.reconciliation.db.dao.UserDAO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.Set;

public class MySQLDAOFactoryTest {
    @Test
    public void givenNullDataSourceOrNullDataSourceSupplier_whenCreate_thenThrowException(){
        SerializableSupplier<DataSource> nullDataSourceSupplier = this::getNullDataSource;
        SerializableSupplier<DataSource> nullSupplier = null;

        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new MySQLDAOFactory(nullSupplier));
        Assertions.assertEquals("null dataSourceSupplier was given", ex.getMessage());

        ex = Assertions.assertThrows(NullPointerException.class,
                () -> new MySQLDAOFactory(nullDataSourceSupplier));
        Assertions.assertEquals("null dataSource was given", ex.getMessage());
    }

    @Test
    public void givenValidDatasource_whenCreate_thenSuccess() {
        DAOFactory factory = new MySQLDAOFactory(TestingUtils::getThrowingDataSource);

        UserDAO userDAO = factory.getUserDAO();
        HistoryDAO historyDAO = factory.getHistoryDAO();

        Assertions.assertNotNull(userDAO, " given DAO was null");
        Assertions.assertTrue(userDAO instanceof MySQLUserDAO, " given DAO was not a MySQLUserDAO");
        Assertions.assertNotNull(historyDAO, " given DAO was null");
        Assertions.assertTrue(historyDAO instanceof MySQLHistoryDAO, " given DAO was not a MySQLHistoryDAO");
    }

    @Test
    public void whenSerialized_thenSerializeCorrectly() {
        DAOFactory factory = new MySQLDAOFactory(TestingUtils::getThrowingDataSource);
        DAOFactory[] deserialized = new DAOFactory[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(factory);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (DAOFactory) ois.readObject();
            deserialized[0].getUserDAO();
            deserialized[0].getHistoryDAO();
        });
    }

    private DataSource getNullDataSource() {
        return null;
    }
}
