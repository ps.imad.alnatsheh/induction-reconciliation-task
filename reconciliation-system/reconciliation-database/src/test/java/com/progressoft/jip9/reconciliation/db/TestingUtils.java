package com.progressoft.jip9.reconciliation.db;

import org.junit.jupiter.api.Assertions;

import javax.sql.DataSource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestingUtils {

    public static DataSource getThrowingDataSource() {
        return (DataSource) Proxy.newProxyInstance(TestingUtils.class.getClassLoader(),
                new Class[]{DataSource.class}, (p, m, a) -> {
                    if (m.getName().equals("getConnection")) {
                        return getThrowingProxyConnection();
                    }
                    return null;
                });
    }

    private static Object getThrowingProxyConnection() {
        return Proxy.newProxyInstance(TestingUtils.class.getClassLoader(),
                new Class[]{Connection.class}, (p, m, a) ->
                {
                    if (m.getName().equals("prepareStatement"))
                        return getThrowingProxyStatement();
                    return null;
                });
    }

    private static Object getThrowingProxyStatement() {
        return Proxy.newProxyInstance(TestingUtils.class.getClassLoader(),
                new Class[]{PreparedStatement.class}, (p, m, a) ->
                {
                    if (m.getName().startsWith("execute"))
                        throw new SQLException("testing exception");
                    return null;
                });
    }

    public static DataSource getLimitingDataSource(DataSource wrapped) {
        Connection[] storedConnection = new Connection[1];
        return (DataSource) Proxy.newProxyInstance(TestingUtils.class.getClassLoader(),
                new Class[]{DataSource.class}, (p, m, a) -> {
                    if (m.getName().equals("getConnection") && a == null) {
                        if (storedConnection[0] != null && !storedConnection[0].isClosed())
                            Assertions.fail("more than one Connection was requested at the same time");
                        storedConnection[0] = wrapped.getConnection();
                        return getLimitingConnection(storedConnection[0]);
                    }
                    Assertions.fail("unnecessary DataSource method call: " + m.toGenericString());
                    return null;
                });

    }

    private static Connection getLimitingConnection(Connection connection) {
        PreparedStatement[] storedStatement = new PreparedStatement[1];
        return (Connection) Proxy.newProxyInstance(TestingUtils.class.getClassLoader(),
                new Class[]{Connection.class}, (p, m, a) ->
                {
                    try {
                        String mName = m.getName();
                        if (mName.equals("prepareStatement")) {
                            if (storedStatement[0] != null && !storedStatement[0].isClosed())
                                Assertions.fail("more than one PreparedStatement was requested at the same time");
                            PreparedStatement wrapped = null;
                            if (a.length == 1 && a[0] instanceof String)
                                wrapped = connection.prepareStatement((String) a[0]);
                            if (a.length == 2 && a[0] instanceof String && a[1] instanceof String[])
                                wrapped = connection.prepareStatement((String) a[0], (String[]) a[1]);
                            if (wrapped != null) {
                                storedStatement[0] = wrapped;
                                return getLimitingProxyStatement(wrapped);
                            }
                        }
                        if (m.getName().equals("close"))
                            return m.invoke(connection, a);
                        if (mName.equals("createStatement"))
                            Assertions.fail("creating unsafe Statement, must instead call prepareStatement");
                        Assertions.fail("unnecessary Connection method call: " + m.toGenericString());
                        return null;
                    }
                    catch (InvocationTargetException e) {
                        throw e.getCause();
                    }
                });
    }

    private static PreparedStatement getLimitingProxyStatement(PreparedStatement wrapped) {
        return (PreparedStatement) Proxy.newProxyInstance(TestingUtils.class.getClassLoader(),
                new Class[]{PreparedStatement.class}, (p, m, a) ->
                {
                    try {
                        if (nameStartsWithAny(m, "set", "execute")
                                || nameEqualsAny(m, "getGeneratedKeys", "close", "getResultSet")) {
                            return m.invoke(wrapped, a);
                        }
                        Assertions.fail("unnecessary PreparedStatement method call: " + m.toGenericString());
                        return null;
                    }
                    catch (InvocationTargetException e) {
                        throw e.getCause();
                    }
                });
    }

    private static boolean nameStartsWithAny(Method m, String... prefixes) {
        String name = m.getName();
        for (String prefix : prefixes) {
            if(name.startsWith(prefix))
                return true;
        }
        return false;
    }

    private static boolean nameEqualsAny(Method m, String... names) {
        String methodName = m.getName();
        for (String name:names) {
            if(methodName.equals(name))
                return true;
        }
        return false;
    }
}
