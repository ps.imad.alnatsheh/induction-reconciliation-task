package com.progressoft.jip9.reconciliation.db.mysql;

import com.progressoft.jip9.reconciliation.db.DAOExecutionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQLUserDAOTest {
    @Test
    public void givenNullDataSource_whenCreate_thenThrowException() {
        DataSource nullDataSource = null;
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new MySQLUserDAO(nullDataSource));
        Assertions.assertEquals("null dataSource was given", ex.getMessage());
    }

    @Test
    public void whenSQLExceptionHappens_thenThrowDAOExecutionException() {
        DataSource ds = getThrowingDataSource();
        MySQLUserDAO userDAO = new MySQLUserDAO(ds);

        Exception ex = Assertions.assertThrows(DAOExecutionException.class,
                () -> userDAO.addUser("abc", "def"));
        Assertions.assertEquals("an exception was thrown while inserting new user", ex.getMessage());
        Assertions.assertTrue(ex.getCause() instanceof SQLException);

        ex = Assertions.assertThrows(DAOExecutionException.class,
                () -> userDAO.updatePasswordHash(1,"asdf"));
        Assertions.assertEquals("an exception was thrown while updating user value", ex.getMessage());
        Assertions.assertTrue(ex.getCause() instanceof SQLException);

        ex = Assertions.assertThrows(DAOExecutionException.class,
                () -> userDAO.updateUsername(1, "asdf"));
        Assertions.assertEquals("an exception was thrown while updating user value", ex.getMessage());
        Assertions.assertTrue(ex.getCause() instanceof SQLException);

        ex = Assertions.assertThrows(DAOExecutionException.class,
                () -> userDAO.getByUsername("Asdf"));
        Assertions.assertEquals("an exception was thrown while fetching user with username", ex.getMessage());
        Assertions.assertTrue(ex.getCause() instanceof SQLException);
    }

    private DataSource getThrowingDataSource() {
        return (DataSource) Proxy.newProxyInstance(getClass().getClassLoader(),
                new Class[]{DataSource.class}, (p, m, a) -> {
                    if (m.getName().equals("getConnection")) {
                        return getThrowingProxyConnection();
                    }
                    return null;
                });
    }

    private Object getThrowingProxyConnection() {
        return Proxy.newProxyInstance(getClass().getClassLoader(),
                new Class[]{Connection.class}, (p,m,a)->
                {
                    if(m.getName().equals("prepareStatement"))
                        return getThrowingProxyStatement();
                    return null;
                });
    }

    private Object getThrowingProxyStatement() {
        return Proxy.newProxyInstance(getClass().getClassLoader(),
                new Class[]{PreparedStatement.class}, (p, m, a)->
                {
                    if(m.getName().startsWith("execute"))
                        throw new SQLException("testing exception");
                    return null;
                });
    }
}
