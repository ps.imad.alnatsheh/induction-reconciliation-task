package com.progressoft.jip9.reconciliation.db;

import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;

public class AccountDatabaseInitializerIT {
    @Test
    public void givenNullDataSource_whenInitialize_thenFail() {
        DataSource nullDataSource = null;
        AccountDatabaseInitializer initializer = new AccountDatabaseInitializer();

        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> initializer.initialize(nullDataSource));
        Assertions.assertEquals("null DataSource was given", ex.getMessage());
    }

    @Test
    public void givenValidDataSource_whenInitialize_thisSuccess() throws IOException {
        Path dbPath = Files.createTempFile("account-db", ".db");
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:file:" + dbPath);
        ds.setUser("sa");
        ds.setPassword("sa");
        AccountDatabaseInitializer initializer = new AccountDatabaseInitializer();
        initializer.initialize(ds);

        Map<String, TableDesc> expectedTableDescs = getExpectedTableDescription();
        Map<String, TableDesc> tableDescs = new HashMap<>();
        tableDescs.put("RECON_USER", new TableDesc());
        tableDescs.put("RECON_HISTORY", new TableDesc());

        try(Connection connection = ds.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            try (ResultSet tables = metaData.getTables(null, null, "%", new String[]{"TABLE"})) {
                addTableDescs(tableDescs, metaData, tables);
            }
        }
        catch (SQLException ex) {
            Assertions.fail(ex);
        }
        excludeGeneratedIndices(expectedTableDescs, tableDescs);

        Assertions.assertEquals(expectedTableDescs, tableDescs);
    }

    private void excludeGeneratedIndices(Map<String, TableDesc> expectedTableDescs, Map<String, TableDesc> tableDescs) {
        tableDescs.forEach((tdn, td) -> td.getIndices().retainAll(expectedTableDescs.get(tdn).getIndices()));
    }

    private void addTableDescs(Map<String, TableDesc> tableDescs, DatabaseMetaData metaData, ResultSet tables) throws SQLException {
        while (tables.next()) {
            String tableName = tables.getString("TABLE_NAME");
            String catalog = tables.getString("TABLE_CAT");
            String schema = tables.getString("TABLE_SCHEM");
            TableDesc tableDesc = tableDescs.get(tableName);
            if(tableDesc == null)
                continue;
            try (ResultSet columns = metaData.getColumns(catalog, schema, tableName, "%")) {
                addColumnDescs(tableDesc, columns);
            }
            try (ResultSet primaryKeys = metaData.getPrimaryKeys(catalog, schema, tableName)) {
                addPKs(tableDescs, tableName, primaryKeys);
            }
            try (ResultSet indices = metaData.getIndexInfo(catalog, schema, tableName, false,false)) {
                addIndices(tableDescs, tableName, indices);
            }
        }
    }

    private void addIndices(Map<String, TableDesc> tableDescs, String tableName, ResultSet indices) throws SQLException {
        Set<String> idxs = tableDescs.get(tableName).getIndices();
        while (indices.next()) {
            idxs.add(indices.getString("INDEX_NAME"));
        }
    }

    private void addPKs(Map<String, TableDesc> tableDescs, String tableName, ResultSet primaryKeys) throws SQLException {
        Set<String> pk = tableDescs.get(tableName).getPk();
        while (primaryKeys.next()) {
            pk.add(primaryKeys.getString("COLUMN_NAME"));
        }
    }

    private void addColumnDescs(TableDesc tableDesc, ResultSet columns) throws SQLException {
        Set<TableDesc.ColumnDesc> coldescs = tableDesc.getColumns();
        while (columns.next()) {
            String colname = columns.getString("COLUMN_NAME");
            int coltype = columns.getInt("DATA_TYPE");
            coldescs.add(new TableDesc.ColumnDesc(colname, coltype));
        }
    }

    private Map<String, TableDesc> getExpectedTableDescription() {
        Map<String, TableDesc> result = new HashMap<>();
        result.put("RECON_USER", getReconUser());
        result.put("RECON_HISTORY", getReconHistory());
        return result;
    }

    private TableDesc getReconUser() {
        Set<TableDesc.ColumnDesc> columnDescs = new HashSet<>();
        columnDescs.add(new TableDesc.ColumnDesc("USER_ID", Types.BIGINT));
        columnDescs.add(new TableDesc.ColumnDesc("USERNAME", Types.VARCHAR));
        columnDescs.add(new TableDesc.ColumnDesc("PASS_HASH", Types.VARCHAR));
        Set<String> pk = new HashSet<>();
        pk.add("USER_ID");
        Set<String> indices = new HashSet<>();
        indices.add("RECON_USER_USERNAME_IDX");
        return new TableDesc(columnDescs, pk, indices);
    }

    private TableDesc getReconHistory() {
        Set<TableDesc.ColumnDesc> columnDescs = new HashSet<>();
        columnDescs.add(new TableDesc.ColumnDesc("HISTORY_ID", Types.BIGINT));
        columnDescs.add(new TableDesc.ColumnDesc("USER_ID", Types.BIGINT));
        columnDescs.add(new TableDesc.ColumnDesc("SESSION_TIME", Types.BIGINT));
        columnDescs.add(new TableDesc.ColumnDesc("MATCH_TIME", Types.BIGINT));
        columnDescs.add(new TableDesc.ColumnDesc("SOURCE_FILE_NAME", Types.VARCHAR));
        columnDescs.add(new TableDesc.ColumnDesc("TARGET_FILE_NAME", Types.VARCHAR));
        columnDescs.add(new TableDesc.ColumnDesc("MATCHED", Types.INTEGER));
        columnDescs.add(new TableDesc.ColumnDesc("MISMATCHED", Types.INTEGER));
        columnDescs.add(new TableDesc.ColumnDesc("MISSING", Types.INTEGER));
        Set<String> pk = new HashSet<>();
        pk.add("HISTORY_ID");
        Set<String> indices = new HashSet<>();
        indices.add("RECON_HISTORY_USER_ID_IDX");
        return new TableDesc(columnDescs, pk, indices);
    }

    private static class TableDesc {
        private Set<ColumnDesc> columns;
        private Set<String> pk;
        private Set<String> indices;

        public TableDesc() {
            this(new HashSet<>(), new HashSet<>(), new HashSet<>());
        }

        public TableDesc(Set<ColumnDesc> columns, Set<String> pk) {
            this(columns, pk, new HashSet<>());
        }

        public TableDesc(Set<ColumnDesc> columns, Set<String> pk, Set<String> indices) {
            this.columns = columns;
            this.pk = pk;
            this.indices = indices;
        }

        public void setColumns(Set<ColumnDesc> columns) {
            this.columns = columns;
        }

        public void setPk(Set<String> pk) {
            this.pk = pk;
        }

        public Set<ColumnDesc> getColumns() {
            return columns;
        }

        public Set<String> getPk() {
            return pk;
        }

        public Set<String> getIndices() {
            return indices;
        }

        public void setIndices(Set<String> indices) {
            this.indices = indices;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TableDesc tableDesc = (TableDesc) o;
            return columns.equals(tableDesc.columns) &&
                    pk.equals(tableDesc.pk) &&
                    indices.equals(tableDesc.indices);
        }

        @Override
        public int hashCode() {
            return Objects.hash(pk, columns, indices);
        }

        @Override
        public String toString() {
            return "{" +
                    "columns=" + columns +
                    ", pk=" + pk +
                    ", indicies=" + indices +
                    '}';
        }

        private static class ColumnDesc {
            private String name;
            private int sqlType;

            public ColumnDesc(String name, int sqlType) {
                this.name = name;
                this.sqlType = sqlType;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setSqlType(int sqlType) {
                this.sqlType = sqlType;
            }

            public String getName() {
                return name;
            }

            public int getSqlType() {
                return sqlType;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                ColumnDesc that = (ColumnDesc) o;
                return sqlType == that.sqlType &&
                        name.equals(that.name);
            }

            @Override
            public int hashCode() {
                return Objects.hash(name, sqlType);
            }

            @Override
            public String toString() {
                return "{" +
                        "name='" + name + '\'' +
                        ", sqlType=" + sqlType +
                        '}';
            }
        }
    }
}
