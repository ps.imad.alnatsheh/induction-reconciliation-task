package com.progressoft.jip9.reconciliation.db.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class UserDTOTest {
    @Test
    public void givenInput_whenConstruct_thenSuccess() {
        long userID = 50;
        String username = "abcd";
        String passwordHash = "xdfe";

        UserDTO userDTO = new UserDTO().setUserID(userID)
                .setUsername(username)
                .setPasswordHash(passwordHash);

        Assertions.assertEquals(userID, userDTO.getUserID(), "userID was not as expected");
        Assertions.assertEquals(username, userDTO.getUsername(), "username was not as expected");
        Assertions.assertEquals(passwordHash, userDTO.getPasswordHash(), "passwordHash was not as expected");
    }

    @Test
    public void givenTwoUserDTOs_whenEqualAndHashcode_thenPerformAsExpected() {
        UserDTO userDTO1 = getUserDTO();
        UserDTO userDTO2 = getUserDTO();
        UserDTO userDTO3 = getDifferentUserDTO();

        Assertions.assertFalse(userDTO1.equals(null), "userDTO was equal to null");
        Assertions.assertTrue(userDTO1.equals(userDTO1), "userDTO was not equal to itself");
        Assertions.assertFalse(userDTO1.equals(10), "userDTO was equal to a value of another type");
        Assertions.assertTrue(userDTO1.equals(userDTO2), "userDTO was not equal to an equivalent userDTO");
        Assertions.assertTrue(userDTO2.equals(userDTO1), "equals was not commutative");
        Assertions.assertFalse(userDTO1.equals(userDTO3), "userDTO was equal to a different userDTO");
        Assertions.assertEquals(userDTO1.hashCode(), userDTO2.hashCode(), "equivalent userDTOs had different hashcodes");
    }

    @Test
    public void givenUserDTO_whenSerialize_thenDeserializeCorrectly() {
        UserDTO userDTO = getUserDTO();
        UserDTO[] deserialized = new UserDTO[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(userDTO);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (UserDTO) ois.readObject();
        });

        Assertions.assertEquals(userDTO, deserialized[0], "deserialized userDTO was not as expected");
    }

    private UserDTO getUserDTO() {
        return new UserDTO().setUserID(43)
                .setUsername("abcd")
                .setPasswordHash("xdfe");
    }

    private UserDTO getDifferentUserDTO() {
        return new UserDTO().setUserID(11)
                .setUsername("dbda")
                .setPasswordHash("serx");
    }
}
