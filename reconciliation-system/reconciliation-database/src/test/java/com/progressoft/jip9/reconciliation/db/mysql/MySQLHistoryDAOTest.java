package com.progressoft.jip9.reconciliation.db.mysql;

import com.progressoft.jip9.reconciliation.db.DAOExecutionException;
import com.progressoft.jip9.reconciliation.db.TestingUtils;
import com.progressoft.jip9.reconciliation.account.HistoryEntry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

public class MySQLHistoryDAOTest {
    @Test
    public void givenNullDataSource_whenCreate_thenThrowException() {
        DataSource nullDataSource = null;
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new MySQLHistoryDAO(nullDataSource));
        Assertions.assertEquals("null dataSource was given", ex.getMessage());
    }

    @Test
    public void whenSQLExceptionHappens_thenThrowDAOExecutionException() {
        DataSource ds = TestingUtils.getThrowingDataSource();
        MySQLHistoryDAO historyDAO = new MySQLHistoryDAO(ds);

        Exception ex = Assertions.assertThrows(DAOExecutionException.class,
                () -> historyDAO.getAllForUser(2));
        Assertions.assertEquals("an exception was thrown while fetching user's history", ex.getMessage());
        Assertions.assertTrue(ex.getCause() instanceof SQLException);

        ex = Assertions.assertThrows(DAOExecutionException.class,
                () -> historyDAO.getAllForUserGroupedBySession(3));
        Assertions.assertEquals("an exception was thrown while fetching user's history", ex.getMessage());
        Assertions.assertTrue(ex.getCause() instanceof SQLException);

        ex = Assertions.assertThrows(DAOExecutionException.class,
                () -> historyDAO.insertHistoryEntry(1, new HistoryEntry()));
        Assertions.assertEquals("an exception was thrown while inserting history entry", ex.getMessage());
        Assertions.assertTrue(ex.getCause() instanceof SQLException);
    }
}
