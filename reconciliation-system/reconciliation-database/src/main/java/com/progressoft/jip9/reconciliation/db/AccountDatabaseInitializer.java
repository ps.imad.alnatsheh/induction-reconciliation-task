package com.progressoft.jip9.reconciliation.db;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

public class AccountDatabaseInitializer {

    public void initialize(DataSource dataSource) {
        Objects.requireNonNull(dataSource, "null DataSource was given");
        try(Connection connection = dataSource.getConnection()){
            JdbcConnection liquiConn = new JdbcConnection(connection);
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(liquiConn);
            Liquibase liquibase = new Liquibase("account-db.xml",
                    new ClassLoaderResourceAccessor(getClass().getClassLoader()), database);
            liquibase.update(new Contexts(), new LabelExpression());
        }
        catch (SQLException | LiquibaseException e) {
            throw new DatabaseInitializationException("error when initializing account database: " + e.getMessage(),e);
        }
    }
}
