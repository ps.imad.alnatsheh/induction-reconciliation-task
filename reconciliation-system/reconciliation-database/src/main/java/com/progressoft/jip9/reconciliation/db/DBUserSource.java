package com.progressoft.jip9.reconciliation.db;

import com.progressoft.jip9.reconciliation.account.HistoryEntry;
import com.progressoft.jip9.reconciliation.account.User;
import com.progressoft.jip9.reconciliation.account.UserSource;
import com.progressoft.jip9.reconciliation.account.cred.*;
import com.progressoft.jip9.reconciliation.db.dao.DAOFactory;
import com.progressoft.jip9.reconciliation.db.dao.UserDAO;
import com.progressoft.jip9.reconciliation.db.dto.UserDTO;

import java.io.Serializable;
import java.util.*;

public class DBUserSource implements UserSource, Serializable {
    private static final long serialVersionUID = -7550776960101750349L;

    private static final Set<Class<? extends Credentials>> supported = Collections.singleton(UsernamePasswordCredentials.class);
    private final DAOFactory daoFactory;
    private final PasswordManager passwordManager;

    public DBUserSource(DAOFactory daoFactory, PasswordManager passwordManager) {
        this.daoFactory = Objects.requireNonNull(daoFactory, "null daoFactory was given");
        this.passwordManager = Objects.requireNonNull(passwordManager, "null passwordManager was given");
    }

    @Override
    public long login(Credentials credentials) {
        validateCredentials(credentials);
        UsernamePasswordCredentials creds = (UsernamePasswordCredentials) credentials;
        UserDAO userDAO = daoFactory.getUserDAO();
        UserDTO user = userDAO.getByUsername(creds.getUsername());
        validateUserDTO(creds, user);
        return user.getUserID();
    }

    @Override
    public long addUser(Credentials credentials) {
        validateCredentials(credentials);
        UsernamePasswordCredentials creds = (UsernamePasswordCredentials) credentials;
        UserDAO userDAO = daoFactory.getUserDAO();
        String passwordHash = passwordManager.hash(creds.getPassword());
        return userDAO.addUser(creds.getUsername(), passwordHash);
    }

    @Override
    public void insertHistoryEntry(User user, HistoryEntry toInsert) {
        validateUser(user);
        daoFactory.getHistoryDAO().insertHistoryEntry(user.getUserID(), toInsert);
    }

    @Override
    public Map<Long, List<HistoryEntry>> getEntriesByUser(User user) {
        validateUser(user);
        return daoFactory.getHistoryDAO().getAllForUserGroupedBySession(user.getUserID());
    }

    private void validateUserDTO(UsernamePasswordCredentials creds, UserDTO user) {
        throwIfNotAUser(user);
        throwIfPasswordDoesNotMatch(creds, user);
    }

    private void throwIfPasswordDoesNotMatch(UsernamePasswordCredentials creds, UserDTO user) {
        if (!passwordManager.match(user.getPasswordHash(), creds.getPassword()))
            throw new PasswordDoesNotMatchException("password does not match the given username");
    }

    private void throwIfNotAUser(UserDTO user) {
        if (user == null)
            throw new UnknownUsernameException("unknown username");
    }

    private void validateUser(User user) {
        if (!user.isLoggedIn())
            throw new UnauthenticatedUserException("given user is not authenticated");
    }

    private void validateCredentials(Credentials credentials) {
        if (credentials == null)
            throw new NullPointerException("null credentials were given");
        if (!supported.contains(credentials.getClass()))
            throw new UnsupportedCredentialsException(credentials.getClass());
    }
}
