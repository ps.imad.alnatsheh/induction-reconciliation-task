package com.progressoft.jip9.reconciliation.db.mysql;

import com.progressoft.jip9.reconciliation.db.SerializableSupplier;
import com.progressoft.jip9.reconciliation.db.dao.DAOFactory;
import com.progressoft.jip9.reconciliation.db.dao.HistoryDAO;
import com.progressoft.jip9.reconciliation.db.dao.UserDAO;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

public class MySQLDAOFactory implements DAOFactory, Serializable {
    private static final long serialVersionUID = 3918663835090704701L;

    private final SerializableSupplier<? extends DataSource> dataSourceSupplier;
    private transient DataSource dataSource;

    public MySQLDAOFactory(SerializableSupplier<? extends DataSource> dataSourceSupplier) {
        this.dataSourceSupplier = Objects.requireNonNull(dataSourceSupplier, "null dataSourceSupplier was given");
        dataSource = Objects.requireNonNull(dataSourceSupplier.get(), "null dataSource was given");
    }

    @Override
    public UserDAO getUserDAO() {
        return new MySQLUserDAO(dataSource);
    }

    @Override
    public HistoryDAO getHistoryDAO() {
        return new MySQLHistoryDAO(dataSource);
    }

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        dataSource = dataSourceSupplier.get();
    }
}
