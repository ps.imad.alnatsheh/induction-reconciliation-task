package com.progressoft.jip9.reconciliation.db.mysql;

import com.mysql.cj.exceptions.MysqlErrorNumbers;
import com.progressoft.jip9.reconciliation.account.cred.UsernameAlreadyExistsException;
import com.progressoft.jip9.reconciliation.db.DAOExecutionException;
import com.progressoft.jip9.reconciliation.db.dao.UserDAO;
import com.progressoft.jip9.reconciliation.db.dto.UserDTO;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class MySQLUserDAO implements UserDAO {
    public static final String SELECT_BY_USERNAME = "SELECT USER_ID, USERNAME, PASS_HASH FROM RECON_USER WHERE USERNAME = ?";
    public static final String UPDATE_USERNAME = "UPDATE RECON_USER SET USERNAME = ? WHERE USER_ID = ?";
    public static final String UPDATE_PASS_HASH = "UPDATE RECON_USER SET PASS_HASH = ? WHERE USER_ID = ?";
    public static final String INSERT_USER = "INSERT INTO RECON_USER(USERNAME, PASS_HASH) VALUES (?,?)";

    private final DataSource dataSource;

    public MySQLUserDAO(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource, "null dataSource was given");
        String s = "SELECT *";
    }

    @Override
    public UserDTO getByUsername(String username) {
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECT_BY_USERNAME)) {
                statement.setString(1, username);
                try (ResultSet resultSet = statement.executeQuery()) {
                    return getUserDTOOrNull(resultSet);
                }
            }
        } catch (SQLException exception) {
            throw new DAOExecutionException("an exception was thrown while fetching user with username", exception);
        }
    }

    @Override
    public boolean updateUsername(long userID, String newUsername) {
        return updateValue(userID, newUsername, UPDATE_USERNAME);
    }

    @Override
    public boolean updatePasswordHash(long userID, String newPasswordHash) {
        return updateValue(userID, newPasswordHash, UPDATE_PASS_HASH);
    }

    private boolean updateValue(long userID, String newValue, String updateStatement) {
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(updateStatement)) {
                statement.setString(1, newValue);
                statement.setLong(2, userID);
                return statement.executeUpdate() != 0;
            }
        } catch (SQLException exception) {
            if (exception.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY)
                throw new UsernameAlreadyExistsException("given username already exists in the database", exception);
            throw new DAOExecutionException("an exception was thrown while updating user value", exception);
        }
    }

    @Override
    public long addUser(String username, String passwordHash) {
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERT_USER, new String[]{"USER_ID"})) {
                statement.setString(1, username);
                statement.setString(2, passwordHash);
                statement.executeUpdate();
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next())
                        return generatedKeys.getLong(1);
                }
            }
        } catch (SQLException exception) {
            if (exception.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY)
                throw new UsernameAlreadyExistsException("given username already exists in the database", exception);
            throw new DAOExecutionException("an exception was thrown while inserting new user", exception);
        }
        return 0;
    }

    private UserDTO getUserDTOOrNull(ResultSet resultSet) throws SQLException {
        if (resultSet.next())
            return new UserDTO().setUserID(resultSet.getLong("USER_ID"))
                    .setUsername(resultSet.getString("USERNAME"))
                    .setPasswordHash(resultSet.getString("PASS_HASH"));
        return null;
    }
}
