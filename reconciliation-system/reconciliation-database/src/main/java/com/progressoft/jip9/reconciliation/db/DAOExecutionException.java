package com.progressoft.jip9.reconciliation.db;

public class DAOExecutionException extends RuntimeException {
    public DAOExecutionException() {
    }

    public DAOExecutionException(String message) {
        super(message);
    }

    public DAOExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOExecutionException(Throwable cause) {
        super(cause);
    }

    public DAOExecutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
