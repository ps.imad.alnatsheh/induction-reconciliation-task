package com.progressoft.jip9.reconciliation.db.dao;

import com.progressoft.jip9.reconciliation.db.dto.UserDTO;

public interface UserDAO {
    UserDTO getByUsername(String username);

    boolean updateUsername(long userID, String newUsername);

    boolean updatePasswordHash(long userID, String newPasswordHash);

    long addUser(String username, String passwordHash);
}
