package com.progressoft.jip9.reconciliation.db.mysql;

import com.progressoft.jip9.reconciliation.db.DAOExecutionException;
import com.progressoft.jip9.reconciliation.db.dao.HistoryDAO;
import com.progressoft.jip9.reconciliation.account.HistoryEntry;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MySQLHistoryDAO implements HistoryDAO {
    private static final String INSERT_HISTORY_ENTRY = "INSERT INTO RECON_HISTORY(USER_ID, SESSION_TIME, MATCH_TIME, SOURCE_FILE_NAME, TARGET_FILE_NAME, MATCHED, MISMATCHED, MISSING) VALUES (?,?,?,?,?,?,?,?)";
    private static final String SELECT_BY_USER_ID = "SELECT SESSION_TIME, MATCH_TIME, SOURCE_FILE_NAME, TARGET_FILE_NAME, MATCHED, MISMATCHED, MISSING FROM RECON_HISTORY WHERE USER_ID = ?";
    private final DataSource dataSource;

    public MySQLHistoryDAO(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource, "null dataSource was given");
    }

    @Override
    public List<HistoryEntry> getAllForUser(long userID) {
        return getHistoryEntriesByID(userID, Collectors.toList());
    }

    @Override
    public Map<Long, List<HistoryEntry>> getAllForUserGroupedBySession(long userID) {
        return getHistoryEntriesByID(userID, Collectors.groupingBy(HistoryEntry::getSessionCreationTime));
    }

    @Override
    public boolean insertHistoryEntry(long userID, HistoryEntry toInsert) {
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(INSERT_HISTORY_ENTRY)){
                int i = 0;
                statement.setLong(++i, userID);
                statement.setLong(++i, toInsert.getSessionCreationTime());
                statement.setLong(++i, toInsert.getMatchingTime());
                statement.setString(++i, toInsert.getSourceFileName());
                statement.setString(++i, toInsert.getTargetFileName());
                statement.setInt(++i, toInsert.getMatched());
                statement.setInt(++i, toInsert.getMismatched());
                statement.setInt(++i, toInsert.getMissing());
                return statement.executeUpdate() != 0;
            }
        }
        catch (SQLException ex) {
            throw new DAOExecutionException("an exception was thrown while inserting history entry", ex);
        }
    }

    private <A,R> R getHistoryEntriesByID(long userID, Collector<HistoryEntry,A,R> collector) {
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement statement = conn.prepareStatement(SELECT_BY_USER_ID)){
                statement.setLong(1, userID);
                return executeAndApplyCollector(statement, collector);
            }
        }
        catch (SQLException ex) {
            throw new DAOExecutionException("an exception was thrown while fetching user's history", ex);
        }
    }

    private <A, R> R executeAndApplyCollector(PreparedStatement statement, Collector<HistoryEntry, A, R> collector) throws SQLException {
        A intermediateColl = collector.supplier().get();
        BiConsumer<A, HistoryEntry> accumulator = collector.accumulator();
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                HistoryEntry dto = getEntryFromResultSet(resultSet);
                accumulator.accept(intermediateColl, dto);
            }
        }
        return collector.finisher().apply(intermediateColl);
    }

    private HistoryEntry getEntryFromResultSet(ResultSet resultSet) throws SQLException {
        return new HistoryEntry()
                .setSessionCreationTime(resultSet.getLong("SESSION_TIME"))
                .setMatchingTime(resultSet.getLong("MATCH_TIME"))
                .setSourceFileName(resultSet.getString("SOURCE_FILE_NAME"))
                .setTargetFileName(resultSet.getString("TARGET_FILE_NAME"))
                .setMatched(resultSet.getInt("MATCHED"))
                .setMismatched(resultSet.getInt("MISMATCHED"))
                .setMissing(resultSet.getInt("MISSING"));
    }
}
