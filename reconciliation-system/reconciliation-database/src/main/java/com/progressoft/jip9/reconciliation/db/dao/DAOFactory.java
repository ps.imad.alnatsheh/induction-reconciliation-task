package com.progressoft.jip9.reconciliation.db.dao;

public interface DAOFactory {
    UserDAO getUserDAO();

    HistoryDAO getHistoryDAO();
}
