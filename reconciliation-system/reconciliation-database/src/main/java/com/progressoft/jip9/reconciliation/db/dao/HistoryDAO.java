package com.progressoft.jip9.reconciliation.db.dao;

import com.progressoft.jip9.reconciliation.account.HistoryEntry;

import java.util.List;
import java.util.Map;

public interface HistoryDAO {
    List<HistoryEntry> getAllForUser(long userID);

    Map<Long, List<HistoryEntry>> getAllForUserGroupedBySession(long userID);

    boolean insertHistoryEntry(long userID, HistoryEntry toInsert);
}
