package com.progressoft.jip9.reconciliation.db.dto;

import java.io.Serializable;
import java.util.Objects;

public class UserDTO implements Serializable {
    private static final long serialVersionUID = -309109339609352243L;
    private long userID;
    private String username;
    private String passwordHash;

    public UserDTO setUserID(long userID) {
        this.userID = userID;
        return this;
    }

    public long getUserID() {
        return userID;
    }

    public UserDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserDTO setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return userID == userDTO.userID &&
                Objects.equals(username, userDTO.username) &&
                Objects.equals(passwordHash, userDTO.passwordHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userID, username, passwordHash);
    }
}
