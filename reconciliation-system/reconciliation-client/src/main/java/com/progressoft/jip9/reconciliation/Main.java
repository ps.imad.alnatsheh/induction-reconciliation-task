package com.progressoft.jip9.reconciliation;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        Path resultDirectory = getResultDirectory();

        ResultWriters writers = getResultWriters(resultDirectory);
        MatchResultHandler handler = new CSVMatchResultHandler(writers);
        RecordMatcher matcher = new RecordMatcher(handler);
        ConsoleReconciliation app = new ConsoleReconciliation(matcher);
        app.run();
    }

    private static Path getResultDirectory() {
        String dir;
        if((dir = System.getProperty("resultDirectory.path")) != null)
            return Paths.get(dir);
        return Paths.get(".");
    }

    private static ResultWriters getResultWriters(Path directory) throws IOException {
        Files.createDirectories(directory);
        return new ResultWriters() {
            @Override
            public Writer getMatchedStream() throws IOException {
                return Files.newBufferedWriter(directory.resolve("matched.csv"));
            }

            @Override
            public Writer getMismatchedStream() throws IOException {
                return Files.newBufferedWriter(directory.resolve("mismatched.csv"));
            }

            @Override
            public Writer getMissingStream() throws IOException {
                return Files.newBufferedWriter(directory.resolve("missing.csv"));
            }
        };
    }
}
