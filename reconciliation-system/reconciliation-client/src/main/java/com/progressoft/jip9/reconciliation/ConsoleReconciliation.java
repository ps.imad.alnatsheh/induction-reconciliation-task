package com.progressoft.jip9.reconciliation;

import com.progressoft.jip9.reconciliation.channel.FileRecordsChannel;
import com.progressoft.jip9.reconciliation.channel.RecordsChannel;
import com.progressoft.jip9.reconciliation.format.CSVRecordsFormat;
import com.progressoft.jip9.reconciliation.format.JSONRecordsFormat;
import com.progressoft.jip9.reconciliation.format.RecordsFormat;

import java.nio.file.Paths;
import java.util.Scanner;
import java.util.function.Supplier;

public class ConsoleReconciliation {
    private final RecordMatcher recordMatcher;

    public ConsoleReconciliation(RecordMatcher recordMatcher) {
        this.recordMatcher = recordMatcher;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter source file location:");
        RecordsChannel sourceChannel = getFileChannel(scanner.nextLine());
        System.out.println("Enter source file format:");
        RecordsFormat sourceFormat = getRecordsFormat(scanner.next());
        scanner.nextLine();

        System.out.println("Enter target file location:");
        RecordsChannel targetChannel = getFileChannel(scanner.nextLine());
        System.out.println("Enter target file format:");
        RecordsFormat targetFormat = getRecordsFormat(scanner.next());
        scanner.nextLine();

        RecordStore source = sourceFormat.readRecords(sourceChannel);
        RecordStore target = targetFormat.readRecords(targetChannel);

        recordMatcher.match(source, target);
    }

    private RecordsChannel getFileChannel(String file) {
        return new FileRecordsChannel(Paths.get(file));
    }

    private RecordsFormat getRecordsFormat(String format) {
        return Formats.valueOf(format.toUpperCase()).getFormat();
    }

    private enum Formats {
        CSV(CSVRecordsFormat::new), JSON(JSONRecordsFormat::new);

        private final Supplier<RecordsFormat> formatSupplier;

        Formats(Supplier<RecordsFormat> formatSupplier) {
            this.formatSupplier = formatSupplier;
        }

        RecordsFormat getFormat() {
            return formatSupplier.get();
        }
    }
}
