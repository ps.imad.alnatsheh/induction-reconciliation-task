package com.progressoft.jip9.reconciliation.format;

public class RecordParsingException extends RuntimeException {
    public RecordParsingException() {
    }

    public RecordParsingException(String message) {
        super(message);
    }

    public RecordParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
