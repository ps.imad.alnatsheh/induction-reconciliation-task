package com.progressoft.jip9.reconciliation.format;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.ValueInstantiationException;
import com.progressoft.jip9.reconciliation.Record;
import com.progressoft.jip9.reconciliation.RecordStore;
import com.progressoft.jip9.reconciliation.channel.RecordsChannel;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Currency;

public class JSONRecordsFormat implements RecordsFormat {
    @Override
    public RecordStore readRecords(RecordsChannel channel) {
        RecordStore result = new RecordStore();
        try (Reader reader = channel.getReader()) {
            try {
                parseJSON(result, reader);
            } catch (ValueInstantiationException e) {
                handleValueInstantiationError(e);
            }
        } catch (IOException e) {
            throw new RecordParsingException("an error occurred while parsing JSON: " + e.getMessage(), e);
        }
        return result;
    }

    private void parseJSON(RecordStore recordStore, Reader reader) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        try(JsonParser parser = mapper.createParser(reader)) {
            JsonToken token = parser.nextToken();
            throwIfNotArrayStart(token);
            while ((token = parser.nextToken()) == JsonToken.START_OBJECT) {
                JSONRecord jsonRecord = readJSONRecord(mapper, parser);
                recordStore.add(jsonRecord.convertToRecord());
            }
            throwIfNotArrayEnd(token, parser);
        }
    }

    private JSONRecord readJSONRecord(ObjectMapper mapper, JsonParser parser) throws IOException {
        return mapper.readValue(parser, JSONRecord.class);
    }

    private void throwIfNotArrayEnd(JsonToken token, JsonParser parser) {
        if(token != JsonToken.END_ARRAY)
            throw new RecordParsingException("error parsing JSON: error at line: " + parser.getTokenLocation().getLineNr());
    }

    private void throwIfNotArrayStart(JsonToken token) {
        if(token != JsonToken.START_ARRAY)
            throw new RecordParsingException("error parsing JSON: could not find expected array start");
    }

    private void handleValueInstantiationError(ValueInstantiationException e) {
        String appendedMessage = e.getMessage();
        if (isCausedByRecordParsingException(e))
            appendedMessage = e.getCause().getMessage();
        throw new RecordParsingException("error parsing at line " + e.getLocation().getLineNr() + ": " + appendedMessage, e);
    }

    private boolean isCausedByRecordParsingException(ValueInstantiationException e) {
        return e.getCause().getClass() == RecordParsingException.class;
    }

    public static class JSONRecord {
        private static final String AMOUNT_FIELD = "amount";
        private static final String REFERENCE_FIELD = "reference";
        private static final String CURRENCY_CODE_FIELD = "currencyCode";
        private static final String DATE_FIELD = "date";
        private static final String PURPOSE_FIELD = "purpose";
        private static final DateTimeFormatter INPUT_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/uuuu");

        LocalDate valuedate;
        String reference;
        BigDecimal amount;
        Currency currency;
        String purpose;

        @JsonCreator
        public JSONRecord(@JsonProperty(REFERENCE_FIELD) String reference,
                          @JsonProperty(AMOUNT_FIELD) String amount,
                          @JsonProperty(CURRENCY_CODE_FIELD) String currencyCode,
                          @JsonProperty(DATE_FIELD) String date,
                          @JsonProperty(PURPOSE_FIELD) String purpose) {
            this.reference = getAndValidateReference(reference);
            this.amount = getAndValidateAmount(amount);
            this.currency = getAndValidateCurrency(currencyCode);
            this.valuedate = getAndValidateValueDate(date);
            this.purpose = purpose;
        }


        private static String getAndValidateReference(String field) {
            return getOrThrowIfBlank(field, REFERENCE_FIELD);
        }

        private static LocalDate getAndValidateValueDate(String field) {
            LocalDate date;
            field = getOrThrowIfBlank(field, DATE_FIELD);
            try {
                date = LocalDate.parse(field, INPUT_FORMATTER);
            }
            catch (DateTimeParseException ex) {
                throw new RecordParsingException("record contained an invalid value for the field '" + DATE_FIELD + "'",ex);
            }
            return date;
        }

        private static Currency getAndValidateCurrency(String field) {
            Currency currency;
            field = getOrThrowIfBlank(field, CURRENCY_CODE_FIELD);
            try {
                currency = Currency.getInstance(field);
            }
            catch (IllegalArgumentException ex) {
                throw new RecordParsingException("record contained an invalid value for the field '" + CURRENCY_CODE_FIELD + "'",ex);
            }
            return currency;
        }

        private static BigDecimal getAndValidateAmount(String field) {
            BigDecimal amount;
            field = getOrThrowIfBlank(field, AMOUNT_FIELD);
            try {
                amount = new BigDecimal(field);
                if(amount.signum() < 0)
                    throw new RecordParsingException("record contained an invalid value for the field '" + AMOUNT_FIELD + "'");
            }
            catch (NumberFormatException ex) {
                throw new RecordParsingException("record contained an invalid value for the field '" + AMOUNT_FIELD + "'",ex);
            }
            return amount;
        }

        private static String getOrThrowIfBlank(String field, String fieldName) {
            if(StringUtils.isBlank(field))
                throw new RecordParsingException("record did not contain the field '" + fieldName + "'");
            return field;
        }

        public Record convertToRecord() {
            return new Record.Builder().setDate(valuedate).setCurrency(currency).setAmount(amount).setID(reference).build();
        }
    }
}
