package com.progressoft.jip9.reconciliation.format;

import com.progressoft.jip9.reconciliation.Record;
import com.progressoft.jip9.reconciliation.RecordStore;
import com.progressoft.jip9.reconciliation.channel.RecordsChannel;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Currency;
import java.util.Map;

public class CSVRecordsFormat implements RecordsFormat {
    private static final CSVFormat INPUT_FORMAT = CSVFormat.DEFAULT.withQuote(null).withHeader();
    private static final String ID_FIELD = "trans unique id";
    private static final String AMOUNT_FIELD = "amount";
    private static final String CURRENCY_FIELD = "currency";
    private static final String VALUE_DATE_FIELD = "value date";

    @Override
    public RecordStore readRecords(RecordsChannel channel) {
        RecordStore store = new RecordStore();
        try (CSVParser parser = CSVParser.parse(channel.getReader(), INPUT_FORMAT)) {
            ensureExistenceOfHeaders(parser.getHeaderMap(),
                    ID_FIELD, AMOUNT_FIELD, CURRENCY_FIELD, VALUE_DATE_FIELD);
            for (CSVRecord csvRecord : parser) {
                throwIfInconsistent(csvRecord);
                Record record = parseRecord(csvRecord);
                store.add(record);
            }
        } catch (IOException e) {
            throw new RecordParsingException("an error occurred while parsing CSV: " + e.getMessage(), e);
        }
        return store;
    }

    private Record parseRecord(CSVRecord csvRecord) {
        String id = getAndValidateID(csvRecord);
        BigDecimal amount = getAndValidateAmount(csvRecord);
        Currency currency = getAndValidateCurrency(csvRecord);
        LocalDate valueDate = getAndValidateValueDate(csvRecord);
        return new Record.Builder().setID(id)
                .setAmount(amount).setCurrency(currency)
                .setDate(valueDate).build();
    }

    private static void throwIfInconsistent(CSVRecord csvRecord) {
        if(!csvRecord.isConsistent())
            throw new RecordParsingException("record " + csvRecord.getRecordNumber() + " contained too many or too few fields");
    }

    private static String getAndValidateID(CSVRecord csvRecord) {
        return getOrThrowIfBlank(csvRecord, ID_FIELD);
    }

    private static LocalDate getAndValidateValueDate(CSVRecord csvRecord) {
        LocalDate date;
        String dateStr = getOrThrowIfBlank(csvRecord, VALUE_DATE_FIELD);
        try {
            date = LocalDate.parse(dateStr, DateTimeFormatter.ISO_LOCAL_DATE);
        }
        catch (DateTimeParseException exception) {
            throw getRecordParsingExceptionCausedBy(csvRecord, exception, VALUE_DATE_FIELD);
        }
        return date;
    }

    private static Currency getAndValidateCurrency(CSVRecord csvRecord) {
        Currency currency;
        String currencyStr = getOrThrowIfBlank(csvRecord, CURRENCY_FIELD);
        try {
            currency = Currency.getInstance(currencyStr);
        }
        catch (IllegalArgumentException ex) {
            throw getRecordParsingExceptionCausedBy(csvRecord, ex, CURRENCY_FIELD);
        }
        return currency;
    }

    private static BigDecimal getAndValidateAmount(CSVRecord csvRecord) {
        BigDecimal amount;
        String amountStr = getOrThrowIfBlank(csvRecord, AMOUNT_FIELD);
        try {
            amount = new BigDecimal(amountStr);
            if(amount.signum() < 0)
                throw new RecordParsingException("record " + csvRecord.getRecordNumber() + " contained an invalid value for the field '" + AMOUNT_FIELD + "'");
        }
        catch (NumberFormatException exception) {
            throw getRecordParsingExceptionCausedBy(csvRecord, exception, AMOUNT_FIELD);
        }
        return amount;
    }

    private static String getOrThrowIfBlank(CSVRecord csvRecord, String field) {
        String result = csvRecord.get(field);
        if(StringUtils.isBlank(result))
            throw new RecordParsingException("record " + csvRecord.getRecordNumber() + " did not contain the field '" + field + "'");
        return result;
    }

    private static RecordParsingException getRecordParsingExceptionCausedBy(CSVRecord csvRecord, Exception causedBy, String fieldName) {
        return new RecordParsingException("record " + csvRecord.getRecordNumber() + " contained an invalid value for the field '" + fieldName + "'", causedBy);
    }

    private static void ensureExistenceOfHeaders(Map<String, Integer> headerMap, String... fieldNames) {
        for (String fieldName : fieldNames) {
            if(!headerMap.containsKey(fieldName))
                throw new RecordParsingException("header does not contain a required field: '" + fieldName + "'");
        }
    }
}
