package com.progressoft.jip9.reconciliation.format;

import com.progressoft.jip9.reconciliation.RecordStore;
import com.progressoft.jip9.reconciliation.channel.RecordsChannel;

public interface RecordsFormat {
    RecordStore readRecords(RecordsChannel channel);
}
