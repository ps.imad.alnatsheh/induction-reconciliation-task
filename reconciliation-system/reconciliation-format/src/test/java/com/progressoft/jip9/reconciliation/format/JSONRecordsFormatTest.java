package com.progressoft.jip9.reconciliation.format;

import com.progressoft.jip9.reconciliation.Record;
import com.progressoft.jip9.reconciliation.RecordStore;
import com.progressoft.jip9.reconciliation.channel.RecordsChannel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

public class JSONRecordsFormatTest {
    @Test
    public void whenIOExceptionOccurs_thenThrowRecordParsingException() {
        JSONRecordsFormat format = new JSONRecordsFormat();
        IOException toThrow = new IOException("this is a testing exception");
        RecordsChannel channelWithThrowingReader = () -> getThrowingReader(toThrow);

        RecordParsingException thrown = Assertions.assertThrows(RecordParsingException.class,
                () -> format.readRecords(channelWithThrowingReader));
        Assertions.assertEquals("an error occurred while parsing JSON: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());
    }

    @Test
    public void givenChannelWithJSONWithInvalidStructure_whenReadRecords_thenThrowException() {
        StringRecordsChannel channel = new StringRecordsChannel();
        JSONRecordsFormat format = new JSONRecordsFormat();
        Executable readRecords = () -> format.readRecords(channel);

        channel.setStringToRead(getJsonWithoutArray());
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing JSON: could not find expected array start", ex.getMessage());

        channel.setStringToRead(getFaultyJson());
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing JSON: error at line: 9", ex.getMessage());
    }

    private String getFaultyJson() {
        return "[\n" +
                "  {\n" +
                "    \"date\": \"10/02/2020\",\n" +
                "    \"reference\": \"TR-47884222206\",\n" +
                "    \"amount\": \"500.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"general\"\n" +
                "  },\n" +
                "  \"string\"\n" +
                "]";
    }

    private String getJsonWithoutArray() {
        return "{\n" +
                "\"date\": \"10/02/2020\",\n" +
                "\"reference\": \"TR-47884222206\",\n" +
                "\"amount\": \"500.00\",\n" +
                "\"currencyCode\": \"USD\",\n" +
                "\"purpose\": \"general\"\n" +
                "}";
    }

    @Test
    public void givenChannelWithJSONWithInvalidFieldsInRecords_whenReadRecords_thenThrowException() {
        StringRecordsChannel channel = new StringRecordsChannel();
        JSONRecordsFormat format = new JSONRecordsFormat();
        Executable readRecords = () -> format.readRecords(channel);

        assertValidID(channel, readRecords);
        assertValidAmount(channel, readRecords);
        assertValidCurrency(channel, readRecords);
        assertValidValueDate(channel, readRecords);
    }

    @Test
    public void givenChannel_whenReadRecords_thenExpectedResultIsReturned() {
        StringRecordsChannel channel = new StringRecordsChannel(getValidJson());
        JSONRecordsFormat format = new JSONRecordsFormat();

        RecordStore result = format.readRecords(channel);
        RecordStore expected = getExpectedRecordStore();

        Assertions.assertNotNull(result, "returned RecordStore was null");
        Assertions.assertEquals(expected, result, "returned RecordStore was not as expected");

        List<StringRecordsChannel.MonitoringStringReader> requestedReaders = channel.getRequestedReaders();
        Assertions.assertEquals(1, requestedReaders.size(), "number of requested readers was not as expected");
        Assertions.assertTrue(requestedReaders.get(0).isClosed(), "reader was not closed");
    }

    private void assertValidValueDate(StringRecordsChannel channel, Executable readRecords) {
        String withoutValueDate = "[\n  {\n    \"reference\": \"TR-47884222206\",\n    \"amount\": \"500.00\",\n    \"currencyCode\": \"USD\",\n    \"purpose\": \"general\"\n  }\n]\n";
        String withUnparseableValueDate = "[\n  {\n    \"date\": \"unparsablegibberish\",\n    \"reference\": \"TR-47884222206\",\n    \"amount\": \"500.00\",\n    \"currencyCode\": \"USD\",\n    \"purpose\": \"general\"\n  }\n]\n";

        channel.setStringToRead(withoutValueDate);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 7: record did not contain the field 'date'", ex.getMessage());

        channel.setStringToRead(withUnparseableValueDate);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 8: record contained an invalid value for the field 'date'", ex.getMessage());
    }

    private void assertValidCurrency(StringRecordsChannel channel, Executable readRecords) {
        String withoutCurrency = "[\n  {\n    \"date\": \"10/02/2020\",\n    \"reference\": \"TR-47884222206\",\n    \"amount\": \"500.00\",\n    \"purpose\": \"general\"\n  }\n]\n";
        String withInvalidCurrency = "[\n  {\n    \"date\": \"10/02/2020\",\n    \"reference\": \"TR-47884222206\",\n    \"amount\": \"500.00\",\n    \"currencyCode\": \"gibberish\",\n    \"purpose\": \"general\"\n  }\n]\n";

        channel.setStringToRead(withoutCurrency);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 7: record did not contain the field 'currencyCode'", ex.getMessage());

        channel.setStringToRead(withInvalidCurrency);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 8: record contained an invalid value for the field 'currencyCode'", ex.getMessage());
    }

    private void assertValidAmount(StringRecordsChannel channel, Executable readRecords) {
        String withoutAmount = "[\n{\n\"date\": \"03/02/2020\",\n\"reference\": \"TR-47884222205\",\n\"currencyCode\": \"JOD\",\n\"purpose\": \"\"\n}\n]\n";
        String withNegativeAmount = "[\n  {\n    \"date\": \"03/02/2020\",\n    \"reference\": \"TR-47884222205\",\n    \"amount\": \"-10.0\",\n    \"currencyCode\": \"JOD\",\n    \"purpose\": \"\"\n  }\n]\n";
        String withUnparseableAmount = "[\n  {\n    \"date\": \"03/02/2020\",\n    \"reference\": \"TR-47884222205\",\n    \"amount\": \"unparsablegibberish\",\n    \"currencyCode\": \"JOD\",\n    \"purpose\": \"\"\n  }\n]\n";

        channel.setStringToRead(withoutAmount);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 7: record did not contain the field 'amount'", ex.getMessage());

        channel.setStringToRead(withNegativeAmount);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 8: record contained an invalid value for the field 'amount'", ex.getMessage());

        channel.setStringToRead(withUnparseableAmount);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 8: record contained an invalid value for the field 'amount'", ex.getMessage());
    }

    private void assertValidID(StringRecordsChannel channel, Executable readRecords) {
        String withoutID = "[\n  {\n    \"date\": \"10/02/2020\",\n    \"reference\": \"TR-47884222206\",\n    \"amount\": \"500.00\",\n    \"currencyCode\": \"USD\",\n    \"purpose\": \"general\"\n  },\n    {\n    \"date\": \"10/02/2020\",\n    \"amount\": \"500.00\",\n    \"currencyCode\": \"USD\",\n    \"purpose\": \"general\"\n  }\n]";
        channel.setStringToRead(withoutID);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("error parsing at line 14: record did not contain the field 'reference'", ex.getMessage());
    }

    private RecordStore getExpectedRecordStore() {
        RecordStore records = new RecordStore();
        records.add(new Record.Builder().setID("TR-47884222201")
                .setAmount(new BigDecimal("140"))
                .setDate(LocalDate.parse("2020-01-20"))
                .setCurrency(Currency.getInstance("USD")).build());
        records.add(new Record.Builder().setID("TR-47884222205")
                .setAmount(new BigDecimal("60.0000"))
                .setDate(LocalDate.parse("2020-02-03"))
                .setCurrency(Currency.getInstance("JOD")).build());
        records.add(new Record.Builder().setID("TR-47884222202")
                .setAmount(new BigDecimal("30.000"))
                .setDate(LocalDate.parse("2020-01-22"))
                .setCurrency(Currency.getInstance("JOD")).build());
        records.add(new Record.Builder().setID("TR-47884222206")
                .setAmount(new BigDecimal("500.0"))
                .setDate(LocalDate.parse("2020-02-10"))
                .setCurrency(Currency.getInstance("USD")).build());
        return records;
    }

    private String getValidJson() {
        return "[\n" +
                "  {\n" +
                "    \"date\": \"20/01/2020\",\n" +
                "    \"reference\": \"TR-47884222201\",\n" +
                "    \"amount\": \"140.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"03/02/2020\",\n" +
                "    \"reference\": \"TR-47884222205\",\n" +
                "    \"amount\": \"60.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"10/02/2020\",\n" +
                "    \"reference\": \"TR-47884222206\",\n" +
                "    \"amount\": \"500.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"general\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"22/01/2020\",\n" +
                "    \"reference\": \"TR-47884222202\",\n" +
                "    \"amount\": \"30.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  }\n" +
                "]\n";
    }

    private static Reader getThrowingReader(IOException toThrow) {
        return new Reader() {
            @Override
            public int read(char[] cbuf, int off, int len) throws IOException {
                throw toThrow;
            }

            @Override
            public void close() throws IOException {

            }
        };
    }
}
