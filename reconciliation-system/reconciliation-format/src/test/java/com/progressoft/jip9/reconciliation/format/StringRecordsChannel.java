package com.progressoft.jip9.reconciliation.format;

import com.progressoft.jip9.reconciliation.channel.RecordsChannel;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class StringRecordsChannel implements RecordsChannel {
    private String stringToRead;
    private List<MonitoringStringReader> requested = new ArrayList<>();

    public StringRecordsChannel() {}

    public StringRecordsChannel(String stringToRead) {
        this.stringToRead = stringToRead;
    }

    @Override
    public Reader getReader() {
        MonitoringStringReader reader = new MonitoringStringReader(stringToRead);
        requested.add(reader);
        return reader;
    }

    public void setStringToRead(String stringToRead) {
        this.stringToRead = stringToRead;
    }

    public List<MonitoringStringReader> getRequestedReaders() {
        return requested;
    }

    public static class MonitoringStringReader extends StringReader {
        private boolean isClosed = false;

        public MonitoringStringReader(String s) {
            super(s);
        }

        public boolean isClosed() {
            return isClosed;
        }

        @Override
        public void close() {
            isClosed = true;
            super.close();
        }
    }
}
