package com.progressoft.jip9.reconciliation.format;

import com.progressoft.jip9.reconciliation.Record;
import com.progressoft.jip9.reconciliation.RecordStore;
import com.progressoft.jip9.reconciliation.channel.RecordsChannel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

public class CSVRecordsFormatTest {

    @Test
    public void whenIOExceptionOccurs_thenThrowRecordParsingException() {
        CSVRecordsFormat format = new CSVRecordsFormat();
        IOException toThrow = new IOException("this is a testing exception");
        RecordsChannel channelWithThrowingReader = () -> getThrowingReader(toThrow);

        RecordParsingException thrown = Assertions.assertThrows(RecordParsingException.class,
                () -> format.readRecords(channelWithThrowingReader));
        Assertions.assertEquals("an error occurred while parsing CSV: this is a testing exception", thrown.getMessage());
        Assertions.assertSame(toThrow, thrown.getCause());
    }

    @Test
    public void givenChannelWithCSVWithMissingFieldsInHeader_whenReadRecords_thenThrowException() {
        CSVRecordsFormat format = new CSVRecordsFormat();
        StringRecordsChannel channel = new StringRecordsChannel("trans description,amount,currency,purpose,value date,trans type\n");
        Executable readRecords = () -> format.readRecords(channel);

        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("header does not contain a required field: 'trans unique id'", ex.getMessage());

        channel.setStringToRead("trans unique id,trans description,currency,purpose,value date,trans type\n");
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("header does not contain a required field: 'amount'", ex.getMessage());

        channel.setStringToRead("trans unique id,amount,trans description,purpose,value date,trans type\n");
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("header does not contain a required field: 'currency'", ex.getMessage());

        channel.setStringToRead("trans unique id,amount,trans description,currency,purpose,trans type\n");
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("header does not contain a required field: 'value date'", ex.getMessage());
    }

    @Test
    public void givenChannelWithCSVWithInvalidFieldsInRecords_whenReadRecords_thenThrowException() {
        String header = "trans unique id,trans description,amount,currency,purpose,value date,trans type\n";
        StringRecordsChannel channel = new StringRecordsChannel();
        CSVRecordsFormat format = new CSVRecordsFormat();
        Executable readRecords = () -> format.readRecords(channel);

        assertNoMissingField(header, channel, readRecords);
        assertValidID(header, channel, readRecords);
        assertValidAmount(header, channel, readRecords);
        assertValidCurrency(header, channel, readRecords);
        assertValidValueDate(header, channel, readRecords);
    }

    @Test
    public void givenChannel_whenReadRecords_thenExpectedResultIsReturned() {
        String csv = getValidCSV();
        StringRecordsChannel channel = new StringRecordsChannel(csv);
        CSVRecordsFormat format = new CSVRecordsFormat();
        RecordStore result = format.readRecords(channel);
        RecordStore expected = getExpectedRecordStore();

        Assertions.assertEquals(expected, result, "result was not as expected");

        List<StringRecordsChannel.MonitoringStringReader> requestedReaders = channel.getRequestedReaders();
        Assertions.assertEquals(1, requestedReaders.size(), "number of requested readers was not as expected");
        Assertions.assertTrue(requestedReaders.get(0).isClosed(), "reader was not closed");
    }

    private void assertValidValueDate(String header, StringRecordsChannel channel, Executable readRecords) {
        String withoutValueDate = "TR-47884222201,online transfer,140,USD,donation,,D\n";
        String withUnparseableValueDate = "TR-47884222201,online transfer,140,USD,donation,invalid date,D\n";

        channel.setStringToRead(header + withoutValueDate);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 did not contain the field 'value date'", ex.getMessage());

        channel.setStringToRead(header + withUnparseableValueDate);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 contained an invalid value for the field 'value date'", ex.getMessage());
    }

    private void assertValidCurrency(String header, StringRecordsChannel channel, Executable readRecords) {
        String withoutCurrency = "TR-47884222201,online transfer,140,,donation,2020-01-20,D\n";
        String withInvalidCurrency = "TR-47884222201,online transfer,140,UUU,donation,2020-01-20,D\n";

        channel.setStringToRead(header + withoutCurrency);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 did not contain the field 'currency'", ex.getMessage());

        channel.setStringToRead(header + withInvalidCurrency);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 contained an invalid value for the field 'currency'", ex.getMessage());
    }

    private void assertValidAmount(String header, StringRecordsChannel channel, Executable readRecords) {
        String withoutAmount = "TR-47884222201,online transfer,,USD,donation,2020-01-20,D\n";
        String withNegativeAmount = "TR-47884222201,online transfer,-10,USD,donation,2020-01-20,D\n";
        String withUnparseableAmount = "TR-47884222201,online transfer,invalid number,USD,donation,2020-01-20,D\n";

        channel.setStringToRead(header + withoutAmount);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 did not contain the field 'amount'", ex.getMessage());

        channel.setStringToRead(header + withNegativeAmount);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 contained an invalid value for the field 'amount'", ex.getMessage());

        channel.setStringToRead(header + withUnparseableAmount);
        ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 contained an invalid value for the field 'amount'", ex.getMessage());
    }

    private void assertValidID(String header, StringRecordsChannel channel, Executable readRecords) {
        String withoutID = "TR-47884222201,online transfer,140,USD,donation,2020-01-20,D\n" +
                "TR-47884222202,atm withdrawal,20.0000,JOD,,2020-01-22,D\n" +
                ",online transfer,140,USD,donation,2020-01-20,D\n";
        channel.setStringToRead(header + withoutID);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 3 did not contain the field 'trans unique id'", ex.getMessage());
    }

    private void assertNoMissingField(String header, StringRecordsChannel channel, Executable readRecords) {
        String withAMissingField = "TR-47884222201,140,USD,donation,2020-01-20,D\n";
        channel.setStringToRead(header + withAMissingField);
        Exception ex = Assertions.assertThrows(RecordParsingException.class, readRecords);
        Assertions.assertEquals("record 1 contained too many or too few fields", ex.getMessage());
    }

    private static RecordStore getExpectedRecordStore() {
        RecordStore records = new RecordStore();
        records.add(new Record.Builder().setID("TR-47884222201")
                .setAmount(new BigDecimal("140"))
                .setDate(LocalDate.parse("2020-01-20"))
                .setCurrency(Currency.getInstance("USD")).build());
        records.add(new Record.Builder().setID("TR-47884222202")
                .setAmount(new BigDecimal("20.0000"))
                .setDate(LocalDate.parse("2020-01-22"))
                .setCurrency(Currency.getInstance("JOD")).build());
        records.add(new Record.Builder().setID("TR-47884222203")
                .setAmount(new BigDecimal("5000"))
                .setDate(LocalDate.parse("2020-01-25"))
                .setCurrency(Currency.getInstance("JOD")).build());
        records.add(new Record.Builder().setID("TR-47884222206")
                .setAmount(new BigDecimal("500.0"))
                .setDate(LocalDate.parse("2020-02-10"))
                .setCurrency(Currency.getInstance("USD")).build());
        return records;
    }

    private static String getValidCSV() {
        return "trans unique id,trans description,amount,currency,purpose,value date,trans type\n" +
                "TR-47884222201,online transfer,140,USD,donation,2020-01-20,D\n" +
                "TR-47884222202,atm withdrawal,20.0000,JOD,,2020-01-22,D\n" +
                "TR-47884222203,counter withdrawal,5000,JOD,,2020-01-25,D\n" +
                "TR-47884222206,atm withdrawal,500.0,USD,,2020-02-10,D\n";
    }

    private static Reader getThrowingReader(IOException toThrow) {
        return new Reader() {
            @Override
            public int read(char[] cbuf, int off, int len) throws IOException {
                throw toThrow;
            }

            @Override
            public void close() throws IOException {

            }
        };
    }

}
