package com.progressoft.jip9.reconciliation.channel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public class FileRecordsChannelTest {
    @Test
    public void givenInvalidFilePaths_whenConstruct_thenThrowException() {
        Path dirPath = Paths.get(".");
        Path notExists = Paths.get(".", "test" + new Random().nextInt(10000));
        Path nullPath = null;

        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new FileRecordsChannel(nullPath));
        Assertions.assertEquals("provided path is null", ex.getMessage());

        ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new FileRecordsChannel(notExists));
        Assertions.assertEquals("provided path does not exist", ex.getMessage());

        ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new FileRecordsChannel(dirPath));
        Assertions.assertEquals("provided path is a directory", ex.getMessage());
    }

    @Test
    public void givenValidPath_whenGetReader_thenReturnExpectedResult() throws IOException {
        Path path = Files.createTempFile("channeltest", null);
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path)) ){
            writer.println("this is a temporary file");
        }
        FileRecordsChannel channel = new FileRecordsChannel(path);
        Reader reader = channel.getReader();
        Assertions.assertNotNull(reader);
        try (BufferedReader bufferedReader = new BufferedReader(reader)) {

            Assertions.assertEquals("this is a temporary file", bufferedReader.readLine());
            Assertions.assertNull(bufferedReader.readLine());
        }
    }
}
