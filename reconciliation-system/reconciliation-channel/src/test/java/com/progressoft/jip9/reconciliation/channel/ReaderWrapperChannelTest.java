package com.progressoft.jip9.reconciliation.channel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ReaderWrapperChannelTest {
    @Test
    public void givenInvalidReader_whenCreate_thenThrowException() throws IOException {
        Reader nullReader = null;

        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new ReaderWrapperChannel(nullReader));
        Assertions.assertEquals("null reader was given", ex.getMessage());
    }

    @Test
    public void givenValidInputStream_whenGetReader_thenSuccess () {
        Reader reader = new StringReader("test");
        ReaderWrapperChannel channel = new ReaderWrapperChannel(reader);

        Assertions.assertSame(reader,channel.getReader());
    }
}
