package com.progressoft.jip9.reconciliation.channel;

import java.io.Reader;
import java.util.Objects;

public class ReaderWrapperChannel implements RecordsChannel {
    private final Reader reader;

    public ReaderWrapperChannel(Reader reader) {
        this.reader = Objects.requireNonNull(reader, "null reader was given");
    }

    @Override
    public Reader getReader() {
        return reader;
    }
}
