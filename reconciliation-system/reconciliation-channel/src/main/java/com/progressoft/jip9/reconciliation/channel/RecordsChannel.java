package com.progressoft.jip9.reconciliation.channel;

import java.io.IOException;
import java.io.Reader;

public interface RecordsChannel {
    Reader getReader() throws IOException;
}
