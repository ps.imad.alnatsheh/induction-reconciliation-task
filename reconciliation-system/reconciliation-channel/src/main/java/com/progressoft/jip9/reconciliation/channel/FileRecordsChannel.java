package com.progressoft.jip9.reconciliation.channel;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class FileRecordsChannel implements RecordsChannel {
    private final Path path;

    public FileRecordsChannel(Path path) {
        validatePath(path);
        this.path = path;
    }

    @Override
    public Reader getReader() throws IOException {
        return new InputStreamReader(Files.newInputStream(path), StandardCharsets.UTF_8);
    }

    private static void validatePath(Path path) {
        Objects.requireNonNull(path, "provided path is null");
        throwIfNotExists(path);
        throwIfNotFile(path);
    }

    private static void throwIfNotFile(Path path) {
        if(Files.isDirectory(path))
            throw new IllegalArgumentException("provided path is a directory");
    }

    private static void throwIfNotExists(Path path) {
        if(!Files.exists(path))
            throw new IllegalArgumentException("provided path does not exist");
    }
}
