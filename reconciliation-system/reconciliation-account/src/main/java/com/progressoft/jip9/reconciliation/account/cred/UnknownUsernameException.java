package com.progressoft.jip9.reconciliation.account.cred;

public class UnknownUsernameException extends InvalidCredentialsException {
    public UnknownUsernameException() {
    }

    public UnknownUsernameException(String message) {
        super(message);
    }

    public UnknownUsernameException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownUsernameException(Throwable cause) {
        super(cause);
    }
}
