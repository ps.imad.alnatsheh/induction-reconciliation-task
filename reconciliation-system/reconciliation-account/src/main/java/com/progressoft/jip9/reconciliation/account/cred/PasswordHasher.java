package com.progressoft.jip9.reconciliation.account.cred;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

public interface PasswordHasher {
    void hash(char[] password, Random random, ByteArrayOutputStream output) throws IOException;

    boolean match(ByteArrayInputStream stored, char[] providedPass) throws IOException;

    byte getID();
}
