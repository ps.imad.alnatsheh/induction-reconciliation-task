package com.progressoft.jip9.reconciliation.account;

import com.progressoft.jip9.reconciliation.account.cred.Credentials;

import java.io.Serializable;

public class SingleSourceAccountSecurityManager implements AccountSecurityManager {
    private final UserSource userSource;

    public SingleSourceAccountSecurityManager(UserSource userSource) {
        this.userSource = userSource;
    }

    @Override
    public User getCurrentUser(SessionData session) {
        Object o = session.getAttribute("user");
        if(o instanceof User)
            return (User) o;
        User user = new UserImpl(userSource);
        session.setAttribute("user", user);
        return user;
    }

    public static class UserImpl implements User, Serializable {
        private static final long serialVersionUID = 4132507752149079792L;

        private final UserSource userSource;
        private boolean loggedIn;
        private long userId;

        public UserImpl(UserSource userSource) {
            this.userSource = userSource;
            this.loggedIn = false;
            this.userId = 0;
        }

        @Override
        public boolean isLoggedIn() {
            return loggedIn;
        }

        @Override
        public long getUserID() {
            return userId;
        }

        @Override
        public void login(Credentials creds) {
            userId = userSource.login(creds);
            loggedIn = true;
        }

        @Override
        public void logout() {
            loggedIn = false;
        }
    }
}
