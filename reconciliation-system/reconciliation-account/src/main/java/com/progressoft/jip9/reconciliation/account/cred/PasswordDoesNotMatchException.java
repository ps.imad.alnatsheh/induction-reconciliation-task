package com.progressoft.jip9.reconciliation.account.cred;

public class PasswordDoesNotMatchException extends InvalidCredentialsException {
    public PasswordDoesNotMatchException() {
    }

    public PasswordDoesNotMatchException(String message) {
        super(message);
    }

    public PasswordDoesNotMatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public PasswordDoesNotMatchException(Throwable cause) {
        super(cause);
    }
}
