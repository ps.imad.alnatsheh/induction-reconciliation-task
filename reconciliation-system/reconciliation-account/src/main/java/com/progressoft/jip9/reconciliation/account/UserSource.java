package com.progressoft.jip9.reconciliation.account;

import com.progressoft.jip9.reconciliation.account.cred.Credentials;

import java.util.List;
import java.util.Map;

public interface UserSource {
    long login(Credentials credentials);

    long addUser(Credentials credentials);

    void insertHistoryEntry(User user, HistoryEntry toInsert);

    Map<Long, List<HistoryEntry>> getEntriesByUser(User user);
}
