package com.progressoft.jip9.reconciliation.account.cred;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.*;

public class PasswordManager implements Serializable {

    public static final Base64.Encoder ENCODER = Base64.getUrlEncoder().withoutPadding();
    public static final Base64.Decoder DECODER = Base64.getUrlDecoder();
    private static final long serialVersionUID = -6798160677293164121L;
    private final Map<Byte, PasswordHasher> hashers;
    private final PasswordHasher defaultHasher;
    private transient SecureRandom random;

    public PasswordManager(Set<PasswordHasher> hasherSet, byte defaultHasherID) {
        Objects.requireNonNull(hasherSet, "null hashers set was given");
        hashers = new HashMap<>();
        fillHashersMap(hasherSet);
        defaultHasher = hashers.get(defaultHasherID);
        failIfNoDefaultExists();
        random = new SecureRandom();
    }

    public String hash(char[] password) {
        try {
            return hash(password, defaultHasher);
        }
        finally {
            deletePass(password);
        }
    }

    private void deletePass(char[] password) {
        if(password == null)
            return;
        Arrays.fill(password, ' ');
    }

    public boolean match(String stored, char[] providedPass) {
        try {
            byte[] decoded = DECODER.decode(stored);
            ByteArrayInputStream bais = new ByteArrayInputStream(decoded);
            byte id = (byte) bais.read();
            PasswordHasher hasher = hashers.get(id);
            throwIfUnknownHasher(id, hasher);
            return hasher.match(bais, providedPass);
        } catch (IOException e) {
            throw new PasswordHashingException(e);
        } finally {
            deletePass(providedPass);
        }
    }

    private void throwIfUnknownHasher(byte id, PasswordHasher hasher) {
        if (hasher == null) {
            throw new IllegalStateException("unknown PasswordHasher with id: " + id);
        }
    }

    private String hash(char[] password, PasswordHasher hasher) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(100);
        baos.write(hasher.getID());
        try {
            hasher.hash(password, random, baos);
        } catch (IOException e) {
            throw new PasswordHashingException(e);
        }
        return ENCODER.encodeToString(baos.toByteArray());
    }

    private void fillHashersMap(Set<PasswordHasher> hasherSet) {
        for (PasswordHasher hasher : hasherSet) {
            PasswordHasher existing = hashers.put(hasher.getID(), hasher);
            failIfAlreadyExists(existing);
        }
    }

    private void failIfNoDefaultExists() {
        if (defaultHasher == null) {
            throw new IllegalArgumentException("the hasher with the defaultHasherID did not exist in the given set");
        }
    }

    private void failIfAlreadyExists(PasswordHasher existing) {
        if (existing != null) {
            throw new IllegalArgumentException("duplicate hashers in the set were found");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasswordManager that = (PasswordManager) o;
        return hashers.equals(that.hashers) &&
                defaultHasher.equals(that.defaultHasher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hashers, defaultHasher);
    }

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        random = new SecureRandom();
    }
}
