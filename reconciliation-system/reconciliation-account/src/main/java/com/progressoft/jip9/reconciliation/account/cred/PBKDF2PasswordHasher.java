package com.progressoft.jip9.reconciliation.account.cred;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class PBKDF2PasswordHasher implements PasswordHasher, Serializable {

    private static final long serialVersionUID = 2113466022230206356L;

    private final static String ALGORITHM = "PBKDF2WithHmacSHA256";
    private static SecretKeyFactory factory = null;
    private final int cost;
    private final int size;

    public PBKDF2PasswordHasher(int cost, int size) {
        this.cost = clamp(cost, 10, 31);
        this.size = clamp(size, 16, 64);
    }

    private int clamp(int cost, int min, int max) {
        return Math.min(max, Math.max(min, cost));
    }

    @Override
    public void hash(char[] password, Random random, ByteArrayOutputStream output) throws IOException {
        hash(password, random, output, cost, size);
    }

    @Override
    public boolean match(ByteArrayInputStream stored, char[] providedPass) throws IOException {
        int cost = stored.read();
        int size = stored.read();
        byte[] salt = new byte[size];
        stored.read(salt);
        byte[] storedKey = new byte[stored.available()];
        stored.read(storedKey);
        byte[] providedKey = pbkdf2(providedPass, salt, cost, size);
        return Arrays.equals(storedKey, providedKey);
    }

    @Override
    public byte getID() {
        return 1;
    }

    public int getCost() {
        return cost;
    }

    public int getSize() {
        return size;
    }

    private void hash(char[] password, Random random, ByteArrayOutputStream output, int cost, int size) throws IOException {
        output.write(cost);
        output.write(size);
        byte[] salt = new byte[size];
        random.nextBytes(salt);
        output.write(salt);
        byte[] key = pbkdf2(password, salt, cost, size);
        output.write(key);
    }

    private byte[] pbkdf2(char[] password, byte[] salt, int cost, int size) {
        int iters = 1 << cost;
        int keyLength = size * 8;
        PBEKeySpec spec = new PBEKeySpec(password, salt, iters, keyLength);
        try {
            SecretKey secretKey = getSecretKeyFactory().generateSecret(spec);
            return secretKey.getEncoded();
        } catch (InvalidKeySpecException e) {
            throw new PasswordHashingException("an error occured while hashing password", e);
        }
    }

    private static SecretKeyFactory getSecretKeyFactory() {
        if (factory == null) {
            try {
                return factory = SecretKeyFactory.getInstance(ALGORITHM);
            } catch (NoSuchAlgorithmException e) {
                throw new PasswordHashingException("hashing algorithm was not found", e);
            }
        }
        return factory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PBKDF2PasswordHasher hasher = (PBKDF2PasswordHasher) o;
        return cost == hasher.cost &&
                size == hasher.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cost, size);
    }
}
