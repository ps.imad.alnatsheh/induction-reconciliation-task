package com.progressoft.jip9.reconciliation.account.cred;

public class UserSourceException extends RuntimeException {
    public UserSourceException() {
    }

    public UserSourceException(String message) {
        super(message);
    }

    public UserSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserSourceException(Throwable cause) {
        super(cause);
    }
}
