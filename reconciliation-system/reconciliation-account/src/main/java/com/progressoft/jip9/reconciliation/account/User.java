package com.progressoft.jip9.reconciliation.account;

import com.progressoft.jip9.reconciliation.account.cred.Credentials;

public interface User {
    boolean isLoggedIn();

    long getUserID();

    void login(Credentials creds);

    void logout();
}
