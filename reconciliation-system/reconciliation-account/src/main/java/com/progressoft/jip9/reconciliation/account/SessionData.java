package com.progressoft.jip9.reconciliation.account;

public interface SessionData {
    Object getAttribute(String key);

    void setAttribute(String key, Object data);
}
