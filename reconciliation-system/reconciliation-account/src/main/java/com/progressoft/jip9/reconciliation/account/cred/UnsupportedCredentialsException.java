package com.progressoft.jip9.reconciliation.account.cred;

public class UnsupportedCredentialsException extends InvalidCredentialsException {
    private final Class<? extends Credentials> causingClass;

    public UnsupportedCredentialsException(Class<? extends Credentials> causingClass) {
        this(causingClass.getSimpleName() + " is not supported as Credentials", causingClass);
    }

    public UnsupportedCredentialsException(String message, Class<? extends Credentials> causingClass) {
        super(message);
        this.causingClass = causingClass;
    }

    public UnsupportedCredentialsException(String message, Throwable cause, Class<? extends Credentials> causingClass) {
        super(message, cause);
        this.causingClass = causingClass;
    }

    public Class<? extends Credentials> getCausingClass() {
        return causingClass;
    }
}
