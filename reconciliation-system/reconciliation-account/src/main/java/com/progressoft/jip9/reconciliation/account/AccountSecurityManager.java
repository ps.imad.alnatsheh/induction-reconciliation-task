package com.progressoft.jip9.reconciliation.account;

public interface AccountSecurityManager {
    User getCurrentUser(SessionData session);
}
