package com.progressoft.jip9.reconciliation.account.cred;

public class UnauthenticatedUserException extends RuntimeException {
    public UnauthenticatedUserException() {
    }

    public UnauthenticatedUserException(String message) {
        super(message);
    }

    public UnauthenticatedUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthenticatedUserException(Throwable cause) {
        super(cause);
    }
}
