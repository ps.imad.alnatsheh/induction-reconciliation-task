package com.progressoft.jip9.reconciliation.account.cred;

public interface Credentials extends AutoCloseable{
    default void close(){ }
}
