package com.progressoft.jip9.reconciliation.account.cred;

import java.util.Arrays;

public class UsernamePasswordCredentials implements Credentials {
    private final String username;
    private char[] password;

    public UsernamePasswordCredentials(String username, char[] password) {
        this.username = username;
        this.password = password.clone();
    }

    public String getUsername() {
        return username;
    }

    public char[] getPassword() {
        return password == null ? null : password.clone();
    }

    @Override
    public void close() {
        Arrays.fill(password, ' ');
        password = null;
    }
}
