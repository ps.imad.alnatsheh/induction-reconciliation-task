package com.progressoft.jip9.reconciliation.account;

import java.io.Serializable;
import java.util.Objects;

public class HistoryEntry implements Serializable {
    private static final long serialVersionUID = -8398503960489014822L;

    private long sessionCreationTime;
    private long matchingTime;
    private String sourceFileName;
    private String targetFileName;
    private int matched;
    private int mismatched;
    private int missing;


    public long getSessionCreationTime() {
        return sessionCreationTime;
    }

    public HistoryEntry setSessionCreationTime(long sessionCreationTime) {
        this.sessionCreationTime = sessionCreationTime;
        return this;
    }

    public long getMatchingTime() {
        return matchingTime;
    }

    public HistoryEntry setMatchingTime(long matchingTime) {
        this.matchingTime = matchingTime;
        return this;
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public HistoryEntry setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
        return this;
    }

    public String getTargetFileName() {
        return targetFileName;
    }

    public HistoryEntry setTargetFileName(String targetFileName) {
        this.targetFileName = targetFileName;
        return this;
    }

    public int getMatched() {
        return matched;
    }

    public HistoryEntry setMatched(int matched) {
        this.matched = matched;
        return this;
    }

    public int getMismatched() {
        return mismatched;
    }

    public HistoryEntry setMismatched(int mismatched) {
        this.mismatched = mismatched;
        return this;
    }

    public int getMissing() {
        return missing;
    }

    public HistoryEntry setMissing(int missing) {
        this.missing = missing;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryEntry entry = (HistoryEntry) o;
        return sessionCreationTime == entry.sessionCreationTime &&
                matchingTime == entry.matchingTime &&
                matched == entry.matched &&
                mismatched == entry.mismatched &&
                missing == entry.missing &&
                Objects.equals(sourceFileName, entry.sourceFileName) &&
                Objects.equals(targetFileName, entry.targetFileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionCreationTime, matchingTime, sourceFileName, targetFileName, matched, mismatched, missing);
    }
}
