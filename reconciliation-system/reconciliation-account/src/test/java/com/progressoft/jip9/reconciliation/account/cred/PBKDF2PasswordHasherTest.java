package com.progressoft.jip9.reconciliation.account.cred;

import com.progressoft.jip9.reconciliation.account.HistoryEntry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Random;


public class PBKDF2PasswordHasherTest {
    @Test
    public void givenCost_whenCreate_thenClamp10To31() {
        int largeCost = 255;
        int smallCost = -6;
        int validCost = 12;
        int size = 32;
        PBKDF2PasswordHasher hasher = new PBKDF2PasswordHasher(largeCost, size);
        Assertions.assertEquals(31, hasher.getCost(), "result was not as expected");

        hasher = new PBKDF2PasswordHasher(smallCost, size);
        Assertions.assertEquals(10, hasher.getCost(), "result was not as expected");

        hasher = new PBKDF2PasswordHasher(validCost, size);
        Assertions.assertEquals(validCost, hasher.getCost(), "result was not as expected");
    }

    @Test
    public void givenSize_whenCreate_thenClamp16To64() {
        int largeSize = 128;
        int smallSize = -20;
        int validSize = 32;
        int cost = 16;
        PBKDF2PasswordHasher hasher = new PBKDF2PasswordHasher(cost, largeSize);
        Assertions.assertEquals(64, hasher.getSize(), "result was not as expected");

        hasher = new PBKDF2PasswordHasher(cost, smallSize);
        Assertions.assertEquals(16, hasher.getSize(), "result was not as expected");

        hasher = new PBKDF2PasswordHasher(cost, validSize);
        Assertions.assertEquals(validSize, hasher.getSize(), "result was not as expected");

    }

    @Test
    public void whenGetID_thenReturn1() {
        PBKDF2PasswordHasher hasher = new PBKDF2PasswordHasher(16, 32);
        Assertions.assertEquals(1, hasher.getID());
    }

    @Test
    public void givenPassword_whenHashThenMatch_thenReturnExpectedResult() throws IOException {
        int size = 32;
        int cost = 16;
        PBKDF2PasswordHasher hasher = new PBKDF2PasswordHasher(cost, size);

        Random random = new Random(1234);
        byte[] salt = new byte[size];
        random.nextBytes(salt);
        random = new Random(1234);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(200);
        char[] pass = "123456".toCharArray();
        hasher.hash(pass, random, baos);

        byte[] resultHash = baos.toByteArray();
        Assertions.assertEquals(66, resultHash.length, "result length was not as expected");
        Assertions.assertEquals(cost, resultHash[0], "cost was not as expected");
        Assertions.assertEquals(size, resultHash[1], "size was not as expected");
        for (int i = 0; i < salt.length; i++) {
            Assertions.assertEquals(salt[i], resultHash[i + 2], "unexpected value at index " + i);
        }

        ByteArrayInputStream bais = new ByteArrayInputStream(resultHash);
        boolean match = hasher.match(bais, pass);
        Assertions.assertTrue(match, "the result did not match the password");
    }

    @Test
    public void whenSerialized_thenSerializeCorrectly() {
        int size = 32;
        int cost = 16;
        PBKDF2PasswordHasher hasher = new PBKDF2PasswordHasher(cost, size);
        PBKDF2PasswordHasher[] deserialized = new PBKDF2PasswordHasher[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(hasher);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (PBKDF2PasswordHasher) ois.readObject();
        });

        Assertions.assertEquals(hasher, deserialized[0], "deserialized entry was not as expected");

    }
}
