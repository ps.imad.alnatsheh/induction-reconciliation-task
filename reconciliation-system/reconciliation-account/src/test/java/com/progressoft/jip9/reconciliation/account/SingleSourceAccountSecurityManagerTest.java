package com.progressoft.jip9.reconciliation.account;

import com.progressoft.jip9.reconciliation.account.cred.Credentials;
import com.progressoft.jip9.reconciliation.account.cred.UsernamePasswordCredentials;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.List;
import java.util.Map;

public class SingleSourceAccountSecurityManagerTest {
    @Test
    public void givenSessionData_whenGetCurrentUser_thenReturnExpectedResult() {
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("test", "abcd".toCharArray());
        UserSource source = new SimpleUserSource(credentials);
        SingleSourceAccountSecurityManager manager = new SingleSourceAccountSecurityManager(source);
        SimpleSessionData sessionData = new SimpleSessionData();

        User currentUser = manager.getCurrentUser(sessionData);
        Assertions.assertNotNull(currentUser, "returned user was null");
        Assertions.assertEquals(0, currentUser.getUserID(), "returned user had an id");
        Assertions.assertFalse(currentUser.isLoggedIn(), "returned user is logged in");
        Assertions.assertSame(sessionData.storedUser, currentUser, "user was not stored in sessionData");

        currentUser.login(credentials);
        Assertions.assertEquals(1, currentUser.getUserID(), "user had the wrong id");
        Assertions.assertTrue(currentUser.isLoggedIn(), "user is not logged in");

        Assertions.assertSame(currentUser, manager.getCurrentUser(sessionData), "refetching user did not give the same one");

        assertSerializable(sessionData.storedUser);

        currentUser.logout();
        Assertions.assertFalse(currentUser.isLoggedIn(), "user is still logged in");
    }

    private void assertSerializable(User user) {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream(1000);
        Object[] result = new Object[1];
        Assertions.assertDoesNotThrow(()->{
            ObjectOutputStream outstream = new ObjectOutputStream(byteOut);
            outstream.writeObject(user);
            ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
            ObjectInputStream instream = new ObjectInputStream(byteIn);
            result[0] = instream.readObject();
        });
        User deserialized = (User) result[0];
        Assertions.assertEquals(user.getUserID(), deserialized.getUserID());
        Assertions.assertEquals(user.isLoggedIn(), deserialized.isLoggedIn());
    }

    private static class SimpleSessionData implements SessionData {
        User storedUser;

        @Override
        public Object getAttribute(String key) {
            if(key.equals("user"))
                return storedUser;
            Assertions.fail("unnecessary method call: getAttribute");
            return null;
        }

        @Override
        public void setAttribute(String key, Object data) {
            if(key.equals("user")) {
                if(data instanceof User) {
                    storedUser = (User) data;
                    return;
                }
                if(data == null)
                    Assertions.fail("given data is null");
                Assertions.fail("given data is not a user");
            }
            Assertions.fail("unnecessary method call: setAttribute");
        }
    }

    private static class SimpleUserSource implements UserSource, Serializable {
        private static final long serialVersionUID = -8876276234957356892L;
        private transient final Credentials creds;
        boolean called;

        public SimpleUserSource(Credentials creds) {
            this.creds = creds;
            called = false;
        }

        @Override
        public long login(Credentials credentials) {
            if(credentials != creds)
                Assertions.fail("unexpected credentials were given");
            if(called)
                Assertions.fail("login called multiple times");
            called = true;
            return 1;
        }

        @Override
        public long addUser(Credentials credentials) {
            Assertions.fail("unnecessary method call: must not add a new user");
            return 0;
        }

        @Override
        public void insertHistoryEntry(User user, HistoryEntry toInsert) {
            Assertions.fail("unnecessary method call: must not update user data");
        }

        @Override
        public Map<Long, List<HistoryEntry>> getEntriesByUser(User user) {
            Assertions.fail("unnecessary method call: getEntriesByUser");
            return null;
        }
    }
}
