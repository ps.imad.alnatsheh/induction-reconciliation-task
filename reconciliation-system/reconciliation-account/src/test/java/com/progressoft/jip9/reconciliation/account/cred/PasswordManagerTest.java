package com.progressoft.jip9.reconciliation.account.cred;

import com.progressoft.jip9.reconciliation.account.HistoryEntry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.*;

public class PasswordManagerTest {
    @Test
    public void givenNullSetOrDuplicatePasswordHasherOrNonexistentDefault_whenConstruct_thenThrowException() {
        Set<PasswordHasher> nullSet = null;
        Set<PasswordHasher> duplicateHashersSet = getDuplicatePasswordHasherSet();
        Set<PasswordHasher> validSet = getValidPasswordHasherSet();
        byte validID = 1, invalidID = 5;
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new PasswordManager(nullSet, validID));
        Assertions.assertEquals("null hashers set was given", ex.getMessage());

        ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PasswordManager(duplicateHashersSet, validID));
        Assertions.assertEquals("duplicate hashers in the set were found", ex.getMessage());

        ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PasswordManager(validSet, invalidID));
        Assertions.assertEquals("the hasher with the defaultHasherID did not exist in the given set", ex.getMessage());
    }

    @Test
    public void givenStoredPasswordWithUnknownHasher_whenMatch_thenThrowException() {
        char[] password = "abcd".toCharArray();
        char[] wrongPassword = "dbca".toCharArray();
        Set<PasswordHasher> set1 = getHashingPasswordHasherSet(password, wrongPassword);
        Set<PasswordHasher> set2 = getValidPasswordHasherSet();
        byte defaultId1 = 2;
        byte defaultId2 = 1;
        PasswordManager manager1 = new PasswordManager(set1, defaultId1);
        PasswordManager manager2 = new PasswordManager(set2, defaultId2);

        String hashed = manager1.hash(password);

        Exception ex = Assertions.assertThrows(IllegalStateException.class,
                () -> manager2.match(hashed, password));
        Assertions.assertEquals("unknown PasswordHasher with id: 2", ex.getMessage());
    }

    @Test
    public void givenPasswordString_whenHash_thenReturnExpectedResult() {
        char[] password = "abcd".toCharArray();
        char[]  wrongPassword = "dbca".toCharArray();
        Set<PasswordHasher> hasherSet = getHashingPasswordHasherSet(password, wrongPassword);
        byte id = 2;
        PasswordManager manager = new PasswordManager(hasherSet, id);

        char[] clone = password.clone();
        String hashed = manager.hash(clone);
        char[] deletedPassword = "    ".toCharArray();
        Assertions.assertArrayEquals(deletedPassword, clone, "password was not deleted");

        Assertions.assertNotNull(hashed, "hashed password was null");

        Base64.Decoder base64 = Base64.getDecoder();
        byte[] decoded = base64.decode(hashed);

        assertDecodedAsExpected(id, decoded, password);

        clone = password.clone();
        char[] wrongClone = wrongPassword.clone();

        Assertions.assertTrue(manager.match(hashed, clone), "password was not matched");
        Assertions.assertFalse(manager.match(hashed, wrongClone), "wrongPassword was matched");

        Assertions.assertArrayEquals(deletedPassword, clone, "password was not deleted");
        Assertions.assertArrayEquals(deletedPassword, wrongClone, "password was not deleted");
    }

    @Test
    public void whenSerialized_thenSerializeCorrectly() {
        Set<PasswordHasher> hasherSet = Collections.singleton(new PBKDF2PasswordHasher(16, 32));
        byte id = 1;
        PasswordManager manager = new PasswordManager(hasherSet, id);
        PasswordManager[] deserialized = new PasswordManager[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(manager);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (PasswordManager) ois.readObject();
            deserialized[0].hash("abcd".toCharArray());
        });

        Assertions.assertEquals(manager, deserialized[0], "deserialized entry was not as expected");
    }

    private void assertDecodedAsExpected(byte id, byte[] decoded, char[] password) {
        byte[] output = getOutputBytes(password);
        Assertions.assertEquals(output.length + 1, decoded.length, "decoded length was not as expected");
        Assertions.assertEquals(id, decoded[0], "hasher ID was not added correctly");
        for (int i = 0; i < output.length; i++) {
            Assertions.assertEquals(output[i], decoded[i+1], "wrong value at: " + i);
        }
    }

    private Set<PasswordHasher> getHashingPasswordHasherSet(char[] password, char[] wrongPassword) {
        Set<PasswordHasher> hashers = new HashSet<>();
        hashers.add(getHashingPasswordHasher(password, wrongPassword));
        hashers.add(getFailingPasswordHasher());
        return hashers;
    }

    private PasswordHasher getHashingPasswordHasher(char[] _password, char[] _wrongPassword) {
        byte[] outputBytes = getOutputBytes(_password);
        return new PasswordHasher() {
            @Override
            public void hash(char[] password, Random random, ByteArrayOutputStream output) throws IOException {
                Assertions.assertTrue(random instanceof SecureRandom, "given random was not secure");
                Assertions.assertArrayEquals(_password, password, "given password array was not as expected");
                output.write(outputBytes);
            }

            @Override
            public boolean match(ByteArrayInputStream stored, char[] providedPass) throws IOException {
                byte[] storedData = new byte[stored.available()];
                stored.read(storedData);
                Assertions.assertArrayEquals(outputBytes,storedData, "stored data was not as previously given");
                if(Arrays.equals(_password, providedPass))
                    return true;
                if(Arrays.equals(_wrongPassword, providedPass))
                    return false;
                Assertions.fail("providedPass array was not as expected");
                return false;
            }

            @Override
            public byte getID() {
                return 2;
            }
        };
    }

    private byte[] getOutputBytes(char[] password) {
        byte[] inputBytes = new String(password).getBytes(StandardCharsets.UTF_8);
        byte[] outputBytes = Arrays.copyOf(inputBytes, inputBytes.length + 1);
        outputBytes[outputBytes.length - 1] = 9;
        return outputBytes;
    }

    private Set<PasswordHasher> getValidPasswordHasherSet() {
        Set<PasswordHasher> hashers = new HashSet<>();
        hashers.add(getFailingPasswordHasher());
        return hashers;
    }

    private Set<PasswordHasher> getDuplicatePasswordHasherSet() {
        Set<PasswordHasher> duplicateHashers = new HashSet<>();
        duplicateHashers.add(getFailingPasswordHasher());
        duplicateHashers.add(getFailingPasswordHasher());
        return duplicateHashers;
    }

    private PasswordHasher getFailingPasswordHasher() {
        return new PasswordHasher() {
            @Override
            public void hash(char[] password, Random random, ByteArrayOutputStream output) {
                Assertions.fail("unnecessary method call: hash");
            }

            @Override
            public boolean match(ByteArrayInputStream stored, char[] providedPass) {
                Assertions.fail("unnecessary method call: match");
                return false;
            }

            @Override
            public byte getID() {
                return 1;
            }
        };
    }
}
