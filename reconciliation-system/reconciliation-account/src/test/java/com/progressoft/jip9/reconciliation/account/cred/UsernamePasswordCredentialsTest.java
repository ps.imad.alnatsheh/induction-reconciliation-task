package com.progressoft.jip9.reconciliation.account.cred;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UsernamePasswordCredentialsTest {
    @Test
    public void givenUsernameAndPassword_whenCreate_thenReturnExpectedResult(){
        String user = "abcd";
        char[] pass = "1234".toCharArray();
        UsernamePasswordCredentials creds = new UsernamePasswordCredentials(user, pass);

        Assertions.assertEquals(user, creds.getUsername(), "username was not as expected");
        Assertions.assertArrayEquals(pass, creds.getPassword(), "password was not as expected");
        Assertions.assertNotSame(pass, creds.getPassword(), "password was not cloned");
    }

    @Test
    public void whenClose_thenDeletePasswordCorrectly() {
        String user = "abcd";
        char[] pass = "1234".toCharArray();
        UsernamePasswordCredentials creds = new UsernamePasswordCredentials(user, pass);

        creds.close();

        Assertions.assertArrayEquals(null, creds.getPassword());
    }
}
