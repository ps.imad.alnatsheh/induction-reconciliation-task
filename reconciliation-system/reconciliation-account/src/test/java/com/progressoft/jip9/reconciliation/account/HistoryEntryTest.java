package com.progressoft.jip9.reconciliation.account;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.time.Instant;

public class HistoryEntryTest {
    @Test
    public void givenInput_whenConstruct_thenSuccess() {
        long sessionTime = Instant.now().toEpochMilli();
        long matchingTime = sessionTime + 100;
        String sourceFileName = "source.csv";
        String targetFileName = "target.json";
        int matched = 10;
        int mismatched = 2;
        int missing = 1;

        HistoryEntry entry = new HistoryEntry()
                .setSessionCreationTime(sessionTime)
                .setMatchingTime(matchingTime)
                .setSourceFileName(sourceFileName)
                .setTargetFileName(targetFileName)
                .setMatched(matched)
                .setMismatched(mismatched)
                .setMissing(missing);

        Assertions.assertEquals(sessionTime, entry.getSessionCreationTime(), "sessionCreationTime was not as expected");
        Assertions.assertEquals(matchingTime, entry.getMatchingTime(), "matchingTime was not as expected");
        Assertions.assertEquals(sourceFileName, entry.getSourceFileName(), "sourceFileName was not as expected");
        Assertions.assertEquals(targetFileName, entry.getTargetFileName(), "targetFileName was not as expected");
        Assertions.assertEquals(matched, entry.getMatched(), "matched was not as expected");
        Assertions.assertEquals(mismatched, entry.getMismatched(), "mismatched was not as expected");
        Assertions.assertEquals(missing, entry.getMissing(), "missing was not as expected");
    }

    @Test
    public void givenTwoEntries_whenEqualsAndHashcode_thenPerformAsExpected() {
        HistoryEntry entry1 = getEntry();
        HistoryEntry entry2 = getEntry();
        HistoryEntry entry3 = getDifferentEntry();

        Assertions.assertFalse(entry1.equals(null), "entry was equal to null");
        Assertions.assertTrue(entry1.equals(entry1), "entry was not equal to itself");
        Assertions.assertFalse(entry1.equals(10), "entry was equal to a value of another type");
        Assertions.assertTrue(entry1.equals(entry2), "entry was not equal to an equivalent entry");
        Assertions.assertTrue(entry2.equals(entry1), "equals was not commutative");
        Assertions.assertFalse(entry1.equals(entry3), "entry was equal to a different entry");
        Assertions.assertEquals(entry1.hashCode(), entry2.hashCode(), "equivalent entries had different hashcodes");
    }

    @Test
    public void givenEntry_whenSerialize_thenDeserializeCorrectly() {
        HistoryEntry entry = getEntry();
        HistoryEntry[] deserialized = new HistoryEntry[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(entry);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (HistoryEntry) ois.readObject();
        });

        Assertions.assertEquals(entry, deserialized[0], "deserialized entry was not as expected");
    }

    private HistoryEntry getEntry() {
        return new HistoryEntry()
                .setSessionCreationTime(Instant.now().toEpochMilli())
                .setMatchingTime(Instant.now().toEpochMilli() + 100)
                .setSourceFileName("source.csv")
                .setTargetFileName("target.json")
                .setMatched(10)
                .setMismatched(2)
                .setMissing(1);
    }

    private HistoryEntry getDifferentEntry() {
        return new HistoryEntry()
                .setSessionCreationTime(Instant.now().toEpochMilli())
                .setMatchingTime(Instant.now().toEpochMilli() + 100)
                .setSourceFileName("source.csv")
                .setTargetFileName("targét.json")
                .setMatched(7)
                .setMismatched(2)
                .setMissing(1);
    }
}
