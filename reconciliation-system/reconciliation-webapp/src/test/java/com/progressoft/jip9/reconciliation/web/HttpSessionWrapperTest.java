package com.progressoft.jip9.reconciliation.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class HttpSessionWrapperTest {
    @Test
    public void givenHttpSession_whenOperate_thenDelegateMethods() {
        SimpleInvocationHandler ih = new SimpleInvocationHandler();
        HttpSession session = getProxySession(ih);
        HttpSessionWrapper wrapper = new HttpSessionWrapper(session);

        Assertions.assertSame(ih.value1, wrapper.getAttribute(ih.key1), "returned value was not as expected");
        wrapper.setAttribute(ih.key2, ih.value2);

        int[] expected = {1, 1};

        Assertions.assertArrayEquals(expected, ih.called, "not all method were called only once");
    }

    private HttpSession getProxySession(InvocationHandler invocationHandler) {
        return (HttpSession) Proxy.newProxyInstance(getClass().getClassLoader(),
                new Class<?>[]{HttpSession.class}, invocationHandler);
    }

    private static class SimpleInvocationHandler implements InvocationHandler {
        int[] called = new int[2];
        Object value1 = new Object();
        Object value2 = new Object();
        String key1 = "key1";
        String key2 = "key2";
        long time = 1234;

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if(method.getName().equals("getAttribute")) {
                Assertions.assertEquals(key1, args[0], "provided argument was not as expected");
                called[0]++;
                return value1;
            }
            if(method.getName().equals("setAttribute")) {
                Assertions.assertEquals(key2, args[0], "provided argument was not as expected");
                Assertions.assertEquals(value2, args[1], "provided argument was not as expected");
                called[1]++;
                return null;
            }
            Assertions.fail("unnecessary method call");
            return null;
        }
    }
}
