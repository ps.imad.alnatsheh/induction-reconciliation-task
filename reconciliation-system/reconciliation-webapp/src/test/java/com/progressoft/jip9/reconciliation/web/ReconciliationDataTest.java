package com.progressoft.jip9.reconciliation.web;

import com.progressoft.jip9.reconciliation.MatchResultHandler;
import com.progressoft.jip9.reconciliation.Record;
import com.progressoft.jip9.reconciliation.RecordStore;
import com.progressoft.jip9.reconciliation.format.RecordsFormat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;
import java.util.stream.Collectors;

public class ReconciliationDataTest {

    boolean format1Called = false;
    boolean format2Called = false;

    @Test
    public void whenCreate_thenReturnExpectedResult() {
        ReconciliationData progress = new ReconciliationData();
        Assertions.assertFalse(progress.getSourceRead(), "isRead returned true unexpectedly");
        Assertions.assertFalse(progress.getTargetRead(), "isRead returned true unexpectedly");
        Assertions.assertFalse(progress.getMatchingComplete(), "matchingComplete returned true unexpectedly");
    }

    @Test
    public void givenIncompleteProgress_whenMatch_thenThrowException() {
        ReconciliationData progress = new ReconciliationData();
        progress.readSource(getFormat1(), getStream1(), "source", "json");
        Exception ex = Assertions.assertThrows(IllegalStateException.class,
                progress::match);
        Assertions.assertEquals("cannot match: either source or target RecordStore is missing", ex.getMessage());

        progress = new ReconciliationData();
        progress.readTarget(getFormat2(), getStream2(), "target", "json");
        ex = Assertions.assertThrows(IllegalStateException.class,
                progress::match);
        Assertions.assertEquals("cannot match: either source or target RecordStore is missing", ex.getMessage());
    }

    @Test
    public void givenFormatAndStream_whenReadSourceOrTarget_thenSuccess() {
        ReconciliationData progress = new ReconciliationData();
        progress.readSource(getFormat1(), getStream1(), "source", "csv");
        Assertions.assertTrue(progress.getSourceRead(), "source was not read");
        Assertions.assertTrue(format1Called, "format1 readRecords was not called");

        progress.readTarget(getFormat2(), getStream2(), "target", "json");
        Assertions.assertTrue(format2Called, "format2 readRecords was not called");
        Assertions.assertTrue(progress.getTargetRead(), "target was not read");
    }

    boolean handleMatchedCalled = false;
    boolean handleMismatchedCalled = false;
    boolean handleMissingCalled = false;
    @Test
    public void givenCompleteProgress_whenMatchAndDelegate_thenSuccess() {
        ReconciliationData progress = prepareProgress();

        progress.match();
        Assertions.assertTrue(progress.getMatchingComplete(), "matching was not complete");
        assertDelegation(progress);
    }

    private ReconciliationData prepareProgress() {
        ReconciliationData progress = new ReconciliationData();
        progress.readSource(getFormat1(), getStream1(), "source", "json");
        progress.readTarget(getFormat2(), getStream2(), "target", "csv");
        return progress;
    }

    @Test
    public void whenSerialize_thenDeserializeCorrectly() {
        ReconciliationData progress = prepareProgress();
        progress.match();

        ReconciliationData[] deserialized = new ReconciliationData[1];

        Assertions.assertDoesNotThrow(() -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(progress);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            deserialized[0] = (ReconciliationData) ois.readObject();
        });
        assertDelegation(deserialized[0]);
    }

    private void assertDelegation(ReconciliationData progress) {
        MatchResultHandler handler = getHandler();
        progress.delegate(handler);
        Assertions.assertTrue(handleMatchedCalled, "method was not called as expected");
        Assertions.assertTrue(handleMismatchedCalled, "method was not called as expected");
        Assertions.assertTrue(handleMissingCalled, "method was not called as expected");
    }

    private MatchResultHandler getHandler() {
        return new MatchResultHandler() {
            @Override
            public void handleMatched(RecordStore recordStore) {
                Assertions.assertFalse(handleMatchedCalled, "method was called more than once");
                Assertions.assertEquals(getExpectedMatched(), recordStore, "given RecordStore was not as expected");
                handleMatchedCalled = true;
            }

            @Override
            public void handleMismatched(RecordStore source, RecordStore target) {
                Assertions.assertFalse(handleMismatchedCalled, "method was called more than once");
                Assertions.assertEquals(getExpectedMismatchedSource(), source, "given RecordStore was not as expected");
                Assertions.assertEquals(getExpectedMismatchedTarget(), target, "given RecordStore was not as expected");
                handleMismatchedCalled = true;
            }

            @Override
            public void handleMissing(RecordStore inSourceOnly, RecordStore inTargetOnly) {
                Assertions.assertFalse(handleMissingCalled, "method was called more than once");
                Assertions.assertEquals(getExpectedInSourceOnly(), inSourceOnly, "given RecordStore was not as expected");
                Assertions.assertEquals(getExpectedInTargetOnly(), inTargetOnly, "given RecordStore was not as expected");
                handleMissingCalled = true;
            }
        };
    }

    private InputStream getStream1() {
        return new ByteArrayInputStream("testing data\ntest 1 test 1".getBytes(StandardCharsets.UTF_8));
    }

    private InputStream getStream2() {
        return new ByteArrayInputStream("testing data\ntest 2 test 2".getBytes(StandardCharsets.UTF_8));
    }

    private RecordsFormat getFormat1() {
        return channel -> {
            Assertions.assertFalse(format1Called, "format 1 was called more than once");
            format1Called = true;
            try (BufferedReader reader = new BufferedReader(channel.getReader())) {
                Assertions.assertEquals("testing data\ntest 1 test 1", reader.lines().collect(Collectors.joining("\n")));
            } catch (IOException e) {
                Assertions.fail("unexpected Exception was thrown", e);
            }
            return getSourceStore();
        };
    }

    private RecordsFormat getFormat2() {
        return channel -> {
            Assertions.assertFalse(format2Called, "format 2 was called more than once");
            format2Called = true;
            try (BufferedReader reader = new BufferedReader(channel.getReader())) {
                Assertions.assertEquals("testing data\ntest 2 test 2", reader.lines().collect(Collectors.joining("\n")));
            } catch (IOException e) {
                Assertions.fail("unexpected Exception was thrown", e);
            }
            return getTargetStore();
        };
    }

    private RecordStore getExpectedInTargetOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222199")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("25.90"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedInSourceOnly() {
        Record record = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.00"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedMismatchedTarget() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("2.600"))
                .setDate(LocalDate.of(2019, Month.OCTOBER, 12))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedMismatchedSource() {
        Record record = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        RecordStore store = new RecordStore();
        store.add(record);
        return store;
    }

    private RecordStore getExpectedMatched() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.350"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        return store;
    }

    private RecordStore getSourceStore() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("USD"))
                .setAmount(new BigDecimal("2.60"))
                .setDate(LocalDate.of(2019, Month.SEPTEMBER, 23))
                .build();
        Record record3 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.350"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        Record record4 = new Record.Builder()
                .setID("TR-47884222201")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("15.00"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        store.add(record3);
        store.add(record4);
        return store;
    }

    private RecordStore getTargetStore() {
        Record record1 = new Record.Builder()
                .setID("TR-47884222202")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("10.250"))
                .setDate(LocalDate.of(2019, Month.MAY, 19))
                .build();
        Record record2 = new Record.Builder()
                .setID("TR-47884222205")
                .setCurrency(Currency.getInstance("JOD"))
                .setAmount(new BigDecimal("2.600"))
                .setDate(LocalDate.of(2019, Month.OCTOBER, 12))
                .build();
        Record record3 = new Record.Builder()
                .setID("TR-47884222206")
                .setCurrency(Currency.getInstance("OMR"))
                .setAmount(new BigDecimal("19.350"))
                .setDate(LocalDate.of(2020, Month.JANUARY, 5))
                .build();
        Record record4 = new Record.Builder()
                .setID("TR-47884222199")
                .setCurrency(Currency.getInstance("EUR"))
                .setAmount(new BigDecimal("25.90"))
                .setDate(LocalDate.of(2019, Month.APRIL, 4))
                .build();
        RecordStore store = new RecordStore();
        store.add(record1);
        store.add(record2);
        store.add(record3);
        store.add(record4);
        return store;
    }
}
