package com.progressoft.jip9.reconciliation.web.filter;

import com.progressoft.jip9.reconciliation.account.AccountSecurityManager;
import com.progressoft.jip9.reconciliation.account.User;
import com.progressoft.jip9.reconciliation.web.HttpSessionWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {
    private final AccountSecurityManager manager;

    public LoginFilter(AccountSecurityManager manager) {
        this.manager = manager;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        User currentUser = manager.getCurrentUser(new HttpSessionWrapper(session));
        boolean urlIsLogin = urlIsLogin(req);
        if(!currentUser.isLoggedIn()) {
            if(urlIsLogin) {
                chain.doFilter(req,resp);
                return;
            }
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }
        chain.doFilter(req, resp);
    }

    private boolean urlIsLogin(HttpServletRequest req) {
        String requestURL = req.getRequestURL().toString();
        return requestURL.endsWith("/login") || requestURL.endsWith("/login.jsp");
    }

    @Override
    public void destroy() {

    }
}
