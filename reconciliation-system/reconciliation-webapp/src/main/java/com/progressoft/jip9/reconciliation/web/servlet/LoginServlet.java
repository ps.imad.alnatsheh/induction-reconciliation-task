package com.progressoft.jip9.reconciliation.web.servlet;

import com.progressoft.jip9.reconciliation.account.AccountSecurityManager;
import com.progressoft.jip9.reconciliation.account.SessionData;
import com.progressoft.jip9.reconciliation.account.User;
import com.progressoft.jip9.reconciliation.account.cred.PasswordDoesNotMatchException;
import com.progressoft.jip9.reconciliation.account.cred.UnknownUsernameException;
import com.progressoft.jip9.reconciliation.account.cred.UsernamePasswordCredentials;
import com.progressoft.jip9.reconciliation.web.HttpSessionWrapper;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;

public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 5243670256278598608L;

    private final AccountSecurityManager securityManager;

    public LoginServlet(AccountSecurityManager securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        SessionData sessionData = new HttpSessionWrapper(session);
        User currentUser = securityManager.getCurrentUser(sessionData);
        if(currentUser.isLoggedIn()) {
            fakeRedirectToUpload(req, resp);
            return;
        }
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if(StringUtils.isAnyBlank(username,password)) {
            errorWithoutErrorPage(resp, "Please provide a username and a password.");
            return;
        }
        char[] pass = password.toCharArray();
        try (UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, pass)) {
            currentUser.login(credentials);
            session.setMaxInactiveInterval(60 * 10);
            fakeRedirectToUpload(req, resp);
        }
        catch (PasswordDoesNotMatchException | UnknownUsernameException e) {
            errorWithoutErrorPage(resp, "The provided username and password combination did not match any record in our database");
        }
        finally {
            Arrays.fill(pass, ' ');
        }
    }

    private void errorWithoutErrorPage(HttpServletResponse resp, String errorText) throws IOException {
        resp.setContentType("text/plain");
        resp.setStatus(400);
        resp.getWriter().append(errorText).flush();
    }

    private void fakeRedirectToUpload(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/plain");
        resp.setStatus(201);
        resp.setHeader("Location", req.getContextPath() + "/upload");
    }
}
