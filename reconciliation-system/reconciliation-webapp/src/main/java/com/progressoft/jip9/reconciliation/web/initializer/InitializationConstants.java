package com.progressoft.jip9.reconciliation.web.initializer;

class InitializationConstants {
    static final String ACCOUNTDB_PREFIX = "accountdb.";
    static final String ENV_VAR_RECON_PROPERTIES = "recon.properties";
    static final String CLASSPATH_RECON_PROPERTIES = "/recon.properties";
    static final String PBKDF2_COST_PROPERTY = "pbkdf2.cost";
    static final String PBKDF2_COST_DEFAULT = "16";
    static final String PBKDF2_SIZE_PROPERTY = "pbkdf2.size";
    static final String PBKDF2_SIZE_DEFAULT = "32";
    static final String ENSURE_USER_EXISTS_PROPERTY = "ensure.user.exists";
    static final String ENSURE_USER_EXISTS_DEFAULT = "false";
    static final String TEMP_DIR_PREFIX_PROPERTY = "temp.dir.prefix";
    static final String TEMP_DIR_PREFIX_DEFAULT = "recon";
    static final String MULTIPART_MAX_FILE_SIZE_PROPERTY = "multipart.max.file.size";
    static final String MULTIPART_MAX_FILE_SIZE_DEFAULT = "20848820";
    static final String MULTIPART_MAX_REQUEST_SIZE_PROPERTY = "multipart.max.request.size";
    static final String MULTIPART_MAX_REQUEST_SIZE_DEFAULT = "418018841";
    static final String MULTIPART_FILE_SIZE_THRESHOLD_PROPERTY = "multipart.file.size.threshold";
    static final String MULTIPART_FILE_SIZE_THRESHOLD_DEFAULT = "1048576";
}
