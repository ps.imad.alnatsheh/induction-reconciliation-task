package com.progressoft.jip9.reconciliation.web.servlet;

import com.progressoft.jip9.reconciliation.web.ReconciliationData;
import com.progressoft.jip9.reconciliation.web.ReconciliationStage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ReconciliationResultServlet extends HttpServlet {
    private static final long serialVersionUID = 5120309322492660370L;
    private static final String RECON_DATA = "reconData";
    private static final String RECON_STAGE = "reconStage";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        ReconciliationData reconData = getReconData(session);
        ReconciliationStage stage = getReconStage(session);
        if(stage != ReconciliationStage.Result) {
            resp.sendRedirect(req.getContextPath() + "/upload");
            return;
        }
        reconData.match();
        // TODO: delegate stuff
        req.getRequestDispatcher("/WEB-INF/views/matchResult.jsp").forward(req, resp);
    }

    private ReconciliationStage getReconStage(HttpSession session) {
        ReconciliationStage reconStage = (ReconciliationStage) session.getAttribute(RECON_STAGE);
        if (reconStage == null) {
            reconStage = ReconciliationStage.Source;
            session.setAttribute(RECON_STAGE, reconStage);
        }
        return reconStage;
    }

    private ReconciliationData getReconData(HttpSession session) {
        ReconciliationData reconData = (ReconciliationData) session.getAttribute(RECON_DATA);
        if (reconData == null) {
            reconData = new ReconciliationData();
            session.setAttribute(RECON_DATA, reconData);
        }
        return reconData;
    }
}
