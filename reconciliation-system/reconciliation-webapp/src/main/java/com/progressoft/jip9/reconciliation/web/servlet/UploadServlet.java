package com.progressoft.jip9.reconciliation.web.servlet;

import com.progressoft.jip9.reconciliation.db.SerializableSupplier;
import com.progressoft.jip9.reconciliation.format.RecordParsingException;
import com.progressoft.jip9.reconciliation.format.RecordsFormat;
import com.progressoft.jip9.reconciliation.web.ReconciliationData;
import com.progressoft.jip9.reconciliation.web.ReconciliationStage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Map;

public class UploadServlet extends HttpServlet {
    private static final long serialVersionUID = -3178209106961531890L;

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadServlet.class);
    private static final String UPLOAD_JSP = "/WEB-INF/views/upload.jsp";
    private static final String COMPARE_JSP = "/WEB-INF/views/compare.jsp";
    private static final String RECON_DATA = "reconData";
    private static final String RECON_STAGE = "reconStage";
    private Map<String, SerializableSupplier<? extends RecordsFormat>> supportedFormats;

    public UploadServlet(Map<String, SerializableSupplier<? extends RecordsFormat>> supportedFormats) {
        this.supportedFormats = supportedFormats;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        ReconciliationData reconData = getReconData(session);
        ReconciliationStage stage = getReconStage(session);
        req.setAttribute("supportedFormats", supportedFormats.keySet());
        switch (stage) {
            case Source:
            case Target:
                dispatch(req, resp, UPLOAD_JSP);
                break;
            case Compare:
                checkFilesOrDispatchCompare(req, resp, session, reconData);
                break;
            case Result:
                if (reconData.getMatchingComplete())
                    resp.sendRedirect(req.getContextPath() + "/result");
                checkFilesOrDispatchCompare(req, resp, session, reconData);
                break;
        }
    }

    private void dispatch(HttpServletRequest req, HttpServletResponse resp, String loc) throws ServletException, IOException {
        req.getRequestDispatcher(loc).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TODO this method needs refactoring so we understand what inside of it
        HttpSession session = req.getSession();
        ReconciliationData reconData = getReconData(session);
        ReconciliationStage stage = getReconStage(session);

        if (handlePrevious(req, resp, session, stage))
            return;
        if (handleCancel(req, resp, session))
            return;
        if (handleCompare(req, resp, session, reconData))
            return;

        redirectIfNotSourceOrTarget(req, resp, stage);

        String format = req.getParameter("format");
        if (!supportedFormats.containsKey(format)) {
            errorWithoutErrorPage(resp, "Unknown format");
            return;
        }
        RecordsFormat recordsFormat = supportedFormats.get(format).get();
        try {
            Part file = req.getPart("file");
            if (file == null) {
                errorWithoutErrorPage(resp, "No file was uploaded");
                return;
            }
            switch (stage) {
                case Source:
                    reconData.readSource(recordsFormat, file.getInputStream(), file.getSubmittedFileName(), format);
                    setStage(session, ReconciliationStage.Target);
                    break;
                case Target:
                    reconData.readTarget(recordsFormat, file.getInputStream(), file.getSubmittedFileName(), format);
                    setStage(session, ReconciliationStage.Compare);
                    break;
                default:
                    break;
            }
            fakeRedirectTo(req, resp, "/upload");
        } catch (RecordParsingException e) {
            errorWithoutErrorPage(resp, "Provided file was invalid, please re-upload");
        } catch (ServletException e) {
            errorWithoutErrorPage(resp, "Invalid request");
        } catch (IllegalStateException e) {
            errorWithoutErrorPage(resp, "File size is too large");
        } catch (IOException e) {
            errorWithoutErrorPage(resp, "An unexpected error happened while reading the file");
            LOGGER.error("IOException occurred on getPart", e);
        }
    }

    private boolean handleCompare(HttpServletRequest req, HttpServletResponse resp, HttpSession session, ReconciliationData reconData) throws IOException, ServletException {
        String compare = req.getParameter("compare");
        if ("true".equals(compare)) {
            if (!checkFiles(req, resp, session, reconData))
                return true;
            setStage(session, ReconciliationStage.Result);
            fakeRedirectTo(req, resp, "/result");
            return true;
        }
        return false;
    }

    private boolean handleCancel(HttpServletRequest req, HttpServletResponse resp, HttpSession session) throws IOException {
        String cancel = req.getParameter("cancel");
        if ("true".equals(cancel)) {
            setStage(session, ReconciliationStage.Source);
            session.setAttribute(RECON_DATA, new ReconciliationData());
            fakeRedirectTo(req, resp, "/upload");
            return true;
        }
        return false;
    }

    private boolean handlePrevious(HttpServletRequest req, HttpServletResponse resp, HttpSession session, ReconciliationStage stage) throws IOException {
        String previous = req.getParameter("previous");
        if ("true".equals(previous)) {
            setStage(session, stage.getPrevious());
            fakeRedirectTo(req, resp, "/upload");
            return true;
        }
        return false;
    }

    private void setStage(HttpSession session, ReconciliationStage target) {
        session.setAttribute(RECON_STAGE, target);
    }

    private void checkFilesOrDispatchCompare(HttpServletRequest req, HttpServletResponse resp, HttpSession session, ReconciliationData reconData) throws ServletException, IOException {
        if (!reconData.getSourceRead()) {
            setStage(session, ReconciliationStage.Source);
            dispatch(req, resp, UPLOAD_JSP);
        }
        if (!reconData.getTargetRead()) {
            setStage(session, ReconciliationStage.Target);
            dispatch(req, resp, UPLOAD_JSP);
        }
        dispatch(req, resp, COMPARE_JSP);
    }

    private boolean checkFiles(HttpServletRequest req, HttpServletResponse resp, HttpSession session, ReconciliationData reconData) throws ServletException, IOException {
        if (!reconData.getSourceRead()) {
            setStage(session, ReconciliationStage.Source);
            return false;
        }
        if (!reconData.getTargetRead()) {
            setStage(session, ReconciliationStage.Target);
            return false;
        }
        return true;
    }

    private void redirectIfNotSourceOrTarget(HttpServletRequest req, HttpServletResponse resp, ReconciliationStage stage) throws IOException {
        switch (stage) {
            case Compare:
                fakeRedirectTo(req, resp, "/upload");
            case Result:
                fakeRedirectTo(req, resp, "/result");
            default:
                break;
        }
    }

    private ReconciliationStage getReconStage(HttpSession session) {
        ReconciliationStage reconStage = (ReconciliationStage) session.getAttribute(RECON_STAGE);
        if (reconStage == null) {
            reconStage = ReconciliationStage.Source;
            setStage(session, reconStage);
        }
        return reconStage;
    }

    private ReconciliationData getReconData(HttpSession session) {
        ReconciliationData reconData = (ReconciliationData) session.getAttribute(RECON_DATA);
        if (reconData == null) {
            reconData = new ReconciliationData();
            session.setAttribute(RECON_DATA, reconData);
        }
        return reconData;
    }

    private void errorWithoutErrorPage(HttpServletResponse resp, String errorText) throws IOException {
        resp.setContentType("text/plain");
        resp.setStatus(400);
        resp.getWriter().append(errorText).flush();
    }

    private void fakeRedirectTo(HttpServletRequest req, HttpServletResponse resp, String loc) throws IOException {
        // TODO it has no difference than resp.sendRedirect !!
        resp.setContentType("text/plain");
        resp.setStatus(201);
        resp.setHeader("Location", req.getContextPath() + loc);
    }
}
