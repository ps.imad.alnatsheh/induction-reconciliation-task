package com.progressoft.jip9.reconciliation.web;

public enum ReconciliationStage {
    Source,Target,Compare, Result;

    private ReconciliationStage previous;
    private ReconciliationStage next;

    static {
        Source.previous = Source;
        Source.next = Target;
        Target.previous = Source;
        Target.next = Compare;
        Compare.previous = Target;
        Compare.next = Result;
        Result.previous = Compare;
        Result.next = Source;
    }

    public ReconciliationStage getPrevious() {
        return previous;
    }

    public ReconciliationStage getNext() {
        return next;
    }
}
