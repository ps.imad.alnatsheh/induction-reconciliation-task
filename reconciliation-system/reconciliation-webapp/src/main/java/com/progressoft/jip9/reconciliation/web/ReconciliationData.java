package com.progressoft.jip9.reconciliation.web;

import com.progressoft.jip9.reconciliation.DelegatingMatchResultHandler;
import com.progressoft.jip9.reconciliation.MatchResultHandler;
import com.progressoft.jip9.reconciliation.RecordMatcher;
import com.progressoft.jip9.reconciliation.RecordStore;
import com.progressoft.jip9.reconciliation.channel.ReaderWrapperChannel;
import com.progressoft.jip9.reconciliation.channel.RecordsChannel;
import com.progressoft.jip9.reconciliation.format.RecordParsingException;
import com.progressoft.jip9.reconciliation.format.RecordsFormat;
import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

public class ReconciliationData implements Serializable {

    private static final long serialVersionUID = -385757037185255094L;
    private RecordStore source;
    private RecordStore target;
    private boolean sourceRead;
    private boolean targetRead;
    private boolean matchingComplete;
    private DelegatingMatchResultHandler delegatingHandler;
    private String sourceFileName;
    private String targetFileName;
    private String sourceFormatType;
    private String targetFormatType;

    public ReconciliationData() {
        this.source = null;
        this.target = null;
        this.sourceRead = false;
        this.targetRead = false;
        sourceFileName = "";
        targetFileName = "";
        sourceFormatType = "";
        targetFormatType = "";
        matchingComplete = false;
        delegatingHandler = new DelegatingMatchResultHandler();
    }

    private void resetHandler() {
        if(matchingComplete) {
            matchingComplete = false;
            delegatingHandler = new DelegatingMatchResultHandler();
        }
    }

    public void readSource(RecordsFormat format, InputStream stream, String filename, String formatType) {
        resetHandler();
        try {
            InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
            RecordsChannel channel = new ReaderWrapperChannel(reader);
            this.source = format.readRecords(channel);
            this.sourceRead = true;
            this.sourceFileName = StringUtils.defaultString(filename);
            this.sourceFormatType = StringUtils.defaultString(formatType);
        }
        catch (RecordParsingException e) {
            this.source = null;
            this.sourceRead = false;
            this.sourceFileName = "";
            this.sourceFormatType = "";
            throw e;
        }
    }

    public void readTarget(RecordsFormat format, InputStream stream, String filename, String formatType) {
        resetHandler();
        try {
            InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
            RecordsChannel channel = new ReaderWrapperChannel(reader);
            this.target = format.readRecords(channel);
            this.targetRead = true;
            this.targetFileName = StringUtils.defaultString(filename);
            this.targetFormatType = StringUtils.defaultString(formatType);
        }
        catch (RecordParsingException e) {
            this.target = null;
            this.targetRead = false;
            this.targetFileName = "";
            this.targetFormatType = "";
            throw e;
        }
    }

    public boolean getSourceRead() {
        return sourceRead;
    }

    public boolean getTargetRead() {
        return targetRead;
    }

    public boolean getMatchingComplete() {
        return matchingComplete;
    }

    public void match() {
        throwIfNotFullyRead();
        resetHandler();
        RecordMatcher matcher = new RecordMatcher(delegatingHandler);
        matcher.match(source, target);
        matchingComplete = true;
    }

    private void throwIfNotFullyRead() {
        if(!targetRead || !sourceRead) {
            throw new IllegalStateException("cannot match: either source or target RecordStore is missing");
        }
    }

    public void delegate(MatchResultHandler handler) {
        this.delegatingHandler.delegate(handler);
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public String getTargetFileName() {
        return targetFileName;
    }

    public String getSourceFormatType() {
        return sourceFormatType;
    }

    public String getTargetFormatType() {
        return targetFormatType;
    }

    public DelegatingMatchResultHandler getHandler() {
        return delegatingHandler;
    }
}
