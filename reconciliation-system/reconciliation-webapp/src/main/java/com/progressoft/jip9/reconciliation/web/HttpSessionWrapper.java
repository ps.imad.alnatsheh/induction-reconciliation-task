package com.progressoft.jip9.reconciliation.web;

import com.progressoft.jip9.reconciliation.account.SessionData;

import javax.servlet.http.HttpSession;

public class HttpSessionWrapper implements SessionData {
    private final HttpSession session;

    public HttpSessionWrapper(HttpSession session) {
        this.session = session;
    }

    @Override
    public Object getAttribute(String key) {
        return session.getAttribute(key);
    }

    @Override
    public void setAttribute(String key, Object data) {
        session.setAttribute(key, data);
    }
}
