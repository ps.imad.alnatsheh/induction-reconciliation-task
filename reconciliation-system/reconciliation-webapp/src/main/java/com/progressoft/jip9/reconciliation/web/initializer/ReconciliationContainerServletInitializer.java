package com.progressoft.jip9.reconciliation.web.initializer;

import com.progressoft.jip9.reconciliation.account.AccountSecurityManager;
import com.progressoft.jip9.reconciliation.account.SingleSourceAccountSecurityManager;
import com.progressoft.jip9.reconciliation.account.UserSource;
import com.progressoft.jip9.reconciliation.account.cred.PBKDF2PasswordHasher;
import com.progressoft.jip9.reconciliation.account.cred.PasswordHasher;
import com.progressoft.jip9.reconciliation.account.cred.PasswordManager;
import com.progressoft.jip9.reconciliation.account.cred.UsernamePasswordCredentials;
import com.progressoft.jip9.reconciliation.db.AccountDatabaseInitializer;
import com.progressoft.jip9.reconciliation.db.DBUserSource;
import com.progressoft.jip9.reconciliation.db.SerializableSupplier;
import com.progressoft.jip9.reconciliation.db.dao.DAOFactory;
import com.progressoft.jip9.reconciliation.db.mysql.MySQLDAOFactory;
import com.progressoft.jip9.reconciliation.format.CSVRecordsFormat;
import com.progressoft.jip9.reconciliation.format.JSONRecordsFormat;
import com.progressoft.jip9.reconciliation.format.RecordsFormat;
import com.progressoft.jip9.reconciliation.web.filter.LoginFilter;
import com.progressoft.jip9.reconciliation.web.servlet.LoginServlet;
import com.progressoft.jip9.reconciliation.web.servlet.LogoutServlet;
import com.progressoft.jip9.reconciliation.web.servlet.ReconciliationResultServlet;
import com.progressoft.jip9.reconciliation.web.servlet.UploadServlet;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.servlet.*;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static com.progressoft.jip9.reconciliation.web.initializer.InitializationConstants.*;

public class ReconciliationContainerServletInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        Properties appProperties = loadProperties();
        Properties dbProperties = getDBProperties(appProperties);

        StaticDataSourceSupplier dsSupplier = new StaticDataSourceSupplier(dbProperties);

        initAccountDatabase(dsSupplier);

        AccountSecurityManager securityManager = initAccountSecurityManager(appProperties, dsSupplier);

        initLoginFilter(ctx, securityManager);
        initLoginServlet(ctx, securityManager);
        initLogoutServlet(ctx);
        initUploadServlet(ctx, appProperties);
        initResultServlet(ctx);
    }

    private void initLoginFilter(ServletContext ctx, AccountSecurityManager securityManager) {
        LoginFilter loginFilter = new LoginFilter(securityManager);
        FilterRegistration.Dynamic reg = ctx.addFilter("loginFilter", loginFilter);
        reg.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
    }

    private void initResultServlet(ServletContext ctx) {
        ReconciliationResultServlet servlet = new ReconciliationResultServlet();
        ServletRegistration.Dynamic reg = ctx.addServlet("reconciliationResult", servlet);
        reg.addMapping("/result");
    }

    private void initLoginServlet(ServletContext ctx, AccountSecurityManager securityManager) {
        LoginServlet login = new LoginServlet(securityManager);
        ServletRegistration.Dynamic reg = ctx.addServlet("loginServlet", login);
        reg.addMapping("/login");
    }

    private void initLogoutServlet(ServletContext ctx) {
        LogoutServlet servlet = new LogoutServlet();
        ServletRegistration.Dynamic reg = ctx.addServlet("logoutServlet", servlet);
        reg.addMapping("/logout");
    }

    private void initUploadServlet(ServletContext ctx, Properties appProperties) {
        String tempDirPrefix = appProperties.getProperty(TEMP_DIR_PREFIX_PROPERTY, TEMP_DIR_PREFIX_DEFAULT);
        long multipartMaxFileSize = Long.parseLong(appProperties.getProperty(MULTIPART_MAX_FILE_SIZE_PROPERTY, MULTIPART_MAX_FILE_SIZE_DEFAULT));
        long multipartMaxRequestSize = Long.parseLong(appProperties.getProperty(MULTIPART_MAX_REQUEST_SIZE_PROPERTY, MULTIPART_MAX_REQUEST_SIZE_DEFAULT));
        int multipartFileSizeThreshold = Integer.parseInt(appProperties.getProperty(MULTIPART_FILE_SIZE_THRESHOLD_PROPERTY, MULTIPART_FILE_SIZE_THRESHOLD_DEFAULT));
        String tempDir = getTempDirLocation(tempDirPrefix);
        MultipartConfigElement config = new MultipartConfigElement(
                tempDir, multipartMaxFileSize,
                multipartMaxRequestSize, multipartFileSizeThreshold
        );

        UploadServlet servlet = new UploadServlet(getSupportedRecordsFormats());
        ServletRegistration.Dynamic reg = ctx.addServlet("uploadServlet", servlet);
        reg.setMultipartConfig(config);
        reg.addMapping("/upload");
    }

    private String getTempDirLocation(String tempDirPrefix) {
        try {
            return Files.createTempDirectory(tempDirPrefix).toString();
        } catch (IOException e) {
            throw new InitializationException(e);
        }
    }

    private void initAccountDatabase(StaticDataSourceSupplier dsSupplier) {
        AccountDatabaseInitializer databaseInitializer = new AccountDatabaseInitializer();
        databaseInitializer.initialize(dsSupplier.get());
    }

    private AccountSecurityManager initAccountSecurityManager(Properties appProperties, StaticDataSourceSupplier dsSupplier) {
        int pbkdf2Cost = Integer.parseInt(appProperties.getProperty(PBKDF2_COST_PROPERTY, PBKDF2_COST_DEFAULT));
        int pbkdf2Size = Integer.parseInt(appProperties.getProperty(PBKDF2_SIZE_PROPERTY, PBKDF2_SIZE_DEFAULT));
        boolean ensureAUserExists = Boolean.parseBoolean(appProperties.getProperty(ENSURE_USER_EXISTS_PROPERTY, ENSURE_USER_EXISTS_DEFAULT));
        DAOFactory daoFactory = new MySQLDAOFactory(dsSupplier);

        PasswordHasher hasher = new PBKDF2PasswordHasher(pbkdf2Cost, pbkdf2Size);
        PasswordManager passwordManager = new PasswordManager(Collections.singleton(hasher), hasher.getID());

        UserSource userSource = new DBUserSource(daoFactory, passwordManager);
        if (ensureAUserExists)
            ensureAUserExists(dsSupplier.get(), userSource);
        return new SingleSourceAccountSecurityManager(userSource);
    }

    private void ensureAUserExists(DataSource ds, UserSource userSource) {
        try (Connection conn = ds.getConnection()) {
            try (Statement statement = conn.createStatement()) {
                statement.execute("SELECT COUNT(*) FROM RECON_USER");
                try (ResultSet resultSet = statement.getResultSet()) {
                    if (resultSet.next())
                        if (resultSet.getInt(1) > 0)
                            return;
                }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        char[] password = "admin".toCharArray();
        try (UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("admin", password)) {
            userSource.addUser(credentials);
        }
    }

    private Properties getDBProperties(Properties appProperties) {
        Properties properties = new Properties();
        for (String property : appProperties.stringPropertyNames()) {
            if (property.startsWith(ACCOUNTDB_PREFIX)) {
                String newPropertyName = property.substring(ACCOUNTDB_PREFIX.length());
                String propertyValue = appProperties.getProperty(property);
                properties.setProperty(newPropertyName, propertyValue);
            }
        }
        return properties;
    }

    private Properties loadProperties() {
        String propertiesPath = System.getProperty(ENV_VAR_RECON_PROPERTIES);
        if (propertiesPath == null) {
            return loadDefaultProperties();
        }
        try (Reader reader = Files.newBufferedReader(Paths.get(propertiesPath))) {
            Properties properties = new Properties();
            properties.load(reader);
            return properties;
        } catch (IOException ioEx) {
            try {
                return loadDefaultProperties();
            } catch (InitializationException initEx) {
                throw new InitializationException(ioEx);
            }
        }
    }

    private Properties loadDefaultProperties() {
        Properties props = new Properties();
        try (InputStream in = getClass().getClassLoader().getResourceAsStream(CLASSPATH_RECON_PROPERTIES)) {
            props.load(in);
        } catch (IOException e) {
            throw new InitializationException(e);
        }
        return props;
    }

    private Map<String, SerializableSupplier<? extends RecordsFormat>> getSupportedRecordsFormats() {
        Map<String, SerializableSupplier<? extends RecordsFormat>> result = new HashMap<>();
        result.put("JSON", new JSONRecordsFormatSupplier());
        result.put("CSV", new CSVRecordsFormatSupplier());
        return result;
    }

    /*
    defined as static nested classes because the serialization
    of inner classes (both local and anonymous) and lambda expressions is discouraged
    */

    private static class JSONRecordsFormatSupplier implements SerializableSupplier<JSONRecordsFormat> {
        private static final long serialVersionUID = 1779438392780508174L;

        @Override
        public JSONRecordsFormat get() {
            return new JSONRecordsFormat();
        }
    }

    private static class CSVRecordsFormatSupplier implements SerializableSupplier<CSVRecordsFormat> {
        private static final long serialVersionUID = 6610913753303317380L;

        @Override
        public CSVRecordsFormat get() {
            return new CSVRecordsFormat();
        }
    }

    private static class StaticDataSourceSupplier implements SerializableSupplier<DataSource> {
        private static final long serialVersionUID = 995134882542911624L;
        private static HikariDataSource dataSource;
        private final Properties dbProperties;

        private StaticDataSourceSupplier(Properties dbProperties) {
            this.dbProperties = dbProperties;
        }

        @Override
        public DataSource get() {
            if (dataSource == null) {// TODO you need a synchronization here
                HikariConfig hikariConfig = new HikariConfig(dbProperties);
                dataSource = new HikariDataSource(hikariConfig);
            }
            return dataSource;
        }
    }
}
