<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="bg-dark">
<form id="login-form" style="width: 250pt; background-color: #EEEEEE" class="rounded rounded-lg p-3 mx-auto mt-5 needs-validation" novalidate>
    <div class="form-group">
        <label for="username">Username:</label>
        <input id="username" type="text" name="username"
               class="form-control border border-secondary" required>
        <div class="invalid-feedback">Please provide this value!</div>
    </div>
    <div class="form-group">
        <label for="password">Password:</label>
        <input id="password" type="password" name="password"
               class="form-control border border-secondary" required>
        <div class="invalid-feedback">Please provide this value!</div>
    </div>
    <div class="form-group">
        <div class="text-right pt-4">
            <button type="submit" name="login" class="btn btn-primary">Login</button>
        </div>
    </div>
</form>
<script>
    $.fn.isValid = function(){
        return this[0].checkValidity()
    }

    $(()=>{
        const form = $('#login-form')
        form.on({
            submit: (e)=>{
                e.preventDefault();
                if(form.isValid()) {
                    $('#login-alert').alert('close');
                    $.ajax({
                        url:'${pageContext.request.contextPath}/login',
                        method: 'POST',
                        data: form.serialize(),
                        error: (error) => showAlert(error.getResponseHeader("Content-Header") === "text/plain" ? error.responseText : 'Unknown error!'),
                        success: (data,status,xhr) => {
                            if(xhr.status === 201) {
                                const loc = xhr.getResponseHeader("Location")
                                if(loc !== null)
                                    location.href = loc
                            }
                        }
                    })
                }
                form.addClass('was-validated');
            }
        })
    })

    function showAlert(message) {
        const alertTag = '<div id="login-alert" class="alert alert-danger alert-dismissible fade show">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Login failed!</strong> ' + message + '</div>'
        $('#login-form').append(alertTag);
    }
</script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
</body>
</html>
