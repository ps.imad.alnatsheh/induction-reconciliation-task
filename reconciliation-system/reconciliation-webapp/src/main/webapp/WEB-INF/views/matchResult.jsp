<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.progressoft.jip9.reconciliation.Record" %>
<%@ page import="com.progressoft.jip9.reconciliation.web.ReconciliationData" %>
<html>
<head>
    <title>Reconciliation Result</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="bg-dark">
<nav class="navbar navbar-expand-sm bg-light">
    <span class="navbar-brand"><strong>Reconciliation</strong></span>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a href="${pageContext.request.contextPath}/logout" class="nav-link">Logout</a>
        </li>
    </ul>
</nav>
<div class="p-3 mx-auto mt-5">
    <div class="container bg-white rounded rounded-lg pt-3">
        <div class="row">
            <div class="col">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#matched">Matched</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#mismatched">Mismatched</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#missing">Missing</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="tab-content">
                    <div class="tab-pane active" id="matched">
                        <table class="table table-striped table-bordered border-primary">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Transaction ID</th>
                                <th>Amount</th>
                                <th>Currency</th>
                                <th>Value Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${sessionScope.reconData.handler.matched.iterator()}" varStatus="loop"
                                       var="record">
                                <tr>
                                    <td>${loop.count}</td>
                                    <td>${record.ID}</td>
                                    <td>${record.amount}</td>
                                    <td>${record.currency}</td>
                                    <td>${record.date}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="mismatched">
                        <table class="table table-striped table-bordered border-primary">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Found In</th>
                                <th>Transaction ID</th>
                                <th>Amount</th>
                                <th>Currency</th>
                                <th>Value Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${sessionScope.reconData.handler.mismatchedSource.iterator()}"
                                       varStatus="loop" var="srecord">
                                <tr>
                                    <td>${loop.count}</td>
                                    <td>SOURCE</td>
                                    <td>${srecord.ID}</td>
                                    <td>${srecord.amount}</td>
                                    <td>${srecord.currency}</td>
                                    <td>${srecord.date}</td>
                                </tr>
                                <tr>
                                    <td>${loop.count}</td>
                                    <td>TARGET</td>
                                    <td>${sessionScope.reconData.handler.mismatchedTarget.get(srecord.ID).ID}</td>
                                    <td>${sessionScope.reconData.handler.mismatchedTarget.get(srecord.ID).amount}</td>
                                    <td>${sessionScope.reconData.handler.mismatchedTarget.get(srecord.ID).currency}</td>
                                    <td>${sessionScope.reconData.handler.mismatchedTarget.get(srecord.ID).date}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="missing">
                        <table class="table table-striped table-bordered border-primary">
                            <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Found In</th>
                                <th>Transaction ID</th>
                                <th>Amount</th>
                                <th>Currency</th>
                                <th>Value Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${sessionScope.reconData.handler.inSourceOnly.iterator()}"
                                       varStatus="loop" var="srecord">
                                <tr>
                                    <td>${loop.count}</td>
                                    <td>SOURCE</td>
                                    <td>${srecord.ID}</td>
                                    <td>${srecord.amount}</td>
                                    <td>${srecord.currency}</td>
                                    <td>${srecord.date}</td>
                                </tr>
                            </c:forEach>
                            <c:forEach items="${sessionScope.reconData.handler.inTargetOnly.iterator()}"
                                       varStatus="loop" var="trecord">
                                <tr>
                                    <td>${loop.count}</td>
                                    <td>TARGET</td>
                                    <td>${trecord.ID}</td>
                                    <td>${trecord.amount}</td>
                                    <td>${trecord.currency}</td>
                                    <td>${trecord.date}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.fn.isValid = function () {
        return this[0].checkValidity()
    }

    function checkFakeRedirect(data, status, xhr) {
        if (xhr.status === 201) {
            const loc = xhr.getResponseHeader("Location")
            if (loc !== null)
                location.href = loc
        }
    }

    function showAlert(message) {
        const alertTag = '<div id="upload-alert" class="alert alert-danger alert-dismissible fade show">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Upload failed!</strong> ' + message + '</div>'
        $('#upload-form').append(alertTag);
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
</body>
</html>
