<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Reconciliation</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="bg-dark">
<nav class="navbar navbar-expand-sm bg-light">
    <span class="navbar-brand"><strong>Reconciliation</strong></span>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a href="${pageContext.request.contextPath}/logout" class="nav-link">Logout</a>
        </li>
    </ul>
</nav>

<div class="p-3 mx-auto mt-5">
    <div class="container bg-white rounded rounded-lg pt-3">
        <div class="row">
            <div class="col-6">
                <div class="card border-primary">
                    <h5 class="card-header text-center font-weight-bold bg-primary text-light">Source File</h5>
                    <div class="card-body">
                        <p class="card-text"><strong>File name:</strong> ${sessionScope.reconData.sourceFileName}
                        </p>
                        <p class="card-text"><strong>File
                            format:</strong> ${sessionScope.reconData.sourceFormatType}</p>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card border-primary">
                    <h5 class="card-header text-center font-weight-bold bg-primary text-light">Target File</h5>
                    <div class="card-body">
                        <p class="card-text"><strong>File name:</strong> ${sessionScope.reconData.targetFileName}
                        </p>
                        <p class="card-text"><strong>File
                            format:</strong> ${sessionScope.reconData.targetFormatType}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-6 py-3">
                <button id="cancel" onclick="sendCancel()" class="btn btn-outline-primary">Cancel</button>
            </div>
            <div class="col-6 text-right py-3 form-group">
                    <button id="previous" onclick="sendPrevious()" class="btn btn-outline-primary">Previous</button>
                    <button id="compare" onclick="sendCompare()" class="btn btn-primary">Compare</button>
            </div>
        </div>
    </div>
</div>

<script>
    $.fn.isValid = function () {
        return this[0].checkValidity()
    }

    function sendPrevious() {
        postToUpload({
            previous: true
        })
    }

    function sendCancel() {
        postToUpload({
            cancel: true
        })
    }

    function sendCompare() {
        postToUpload({
            compare: true
        })
    }

    function postToUpload(postData) {
        $.post({
            url: '${pageContext.request.contextPath}/upload',
            method: 'POST',
            data: postData,
            success: checkFakeRedirect
        })
    }

    function checkFakeRedirect(data, status, xhr) {
        if (xhr.status === 201) {
            const loc = xhr.getResponseHeader("Location")
            if (loc !== null)
                location.href = loc
        }
    }

    function showAlert(message) {
        const alertTag = '<div id="upload-alert" class="alert alert-danger alert-dismissible fade show">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Upload failed!</strong> ' + message + '</div>'
        $('#upload-form').append(alertTag);
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
</body>
</html>
