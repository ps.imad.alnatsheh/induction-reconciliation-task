<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.progressoft.jip9.reconciliation.web.ReconciliationStage" %>

<html>
<head>
    <title>Reconciliation</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="bg-dark">
<nav class="navbar navbar-expand-sm bg-light">
    <span class="navbar-brand"><strong>Reconciliation</strong></span>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a href="${pageContext.request.contextPath}/logout" class="nav-link">Logout</a>
        </li>
    </ul>
</nav>

<form id="upload-form" style="width: 250pt; background-color: #EEEEEE"
      class="rounded rounded-lg p-3 mx-auto mt-5 needs-validation" novalidate>
    <div class="form-group">
        <label for="file">${sessionScope.reconStage} file:</label>
        <input id="file" type="file" name="file"
               class="form-control border border-secondary" required>
        <div class="invalid-feedback">Please provide this value!</div>
    </div>
    <div class="form-group">
        <label for="format-select">Format:</label>
        <select id="format-select" name="format"
                class="form-control border border-secondary" required>
            <c:forEach items="${requestScope.supportedFormats}" var="supportedFormat">
                <option value="${supportedFormat}">${supportedFormat}</option>
            </c:forEach>
        </select>
        <div class="invalid-feedback">Please provide this value!</div>
    </div>
    <div class="form-group">
        <div class="text-right pt-4 pl-1">
            <c:if test="${sessionScope.reconStage eq ReconciliationStage.Target}">
                <button id="previous" onclick="sendPrevious()" class="btn btn-outline-primary">Previous</button>
            </c:if>
            <button type="submit" name="next" class="btn btn-primary">Next</button>
        </div>
    </div>
</form>
<script>
    $.fn.isValid = function () {
        return this[0].checkValidity()
    }

    $(() => {
        const form = $('#upload-form')
        form.on({
            submit: (e) => {
                e.preventDefault();
                const formData = new FormData(form[0]);
                if (form.isValid()) {
                    $('#upload-alert').alert('close');
                    $.ajax({
                        url: '${pageContext.request.contextPath}/upload',
                        method: 'POST',
                        contentType: false,
                        processData: false,
                        data: formData,
                        error: (error) => showAlert(error.getResponseHeader("Content-Type").startsWith("text/plain") ? error.responseText : 'Unknown error!'),
                        success: checkFakeRedirect
                    })
                }
                form.addClass('was-validated');
            }
        })
    })

    function sendPrevious() {
        $.post({
            url: '${pageContext.request.contextPath}/upload',
            method: 'POST',
            data: {
                previous: true
            },
            success: checkFakeRedirect
        })
    }

    function checkFakeRedirect(data, status, xhr) {
        if (xhr.status === 201) {
            const loc = xhr.getResponseHeader("Location")
            if (loc !== null)
                location.href = loc
        }
    }

    function showAlert(message) {
        const alertTag = '<div id="upload-alert" class="alert alert-danger alert-dismissible fade show">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Upload failed!</strong> ' + message + '</div>'
        $('#upload-form').append(alertTag);
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
</body>
</html>